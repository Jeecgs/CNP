package com.seed.conf;

import com.seed.codegenerator.generator.entity.TemplateConfig;
import org.jeecgframework.AbstractUnitTest;
import org.junit.Test;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

/**
 * @author gchiaway
 *         日期: 2020-03-23
 *         时间: 16:38
 */
public class TestConf extends AbstractUnitTest {

    @Test
    public void testTypeDesc() {
        Yaml yaml = new Yaml(new Constructor(TemplateConfig.class));
        TemplateConfig templateConfig = (TemplateConfig) yaml.load(this.getClass().getClassLoader()
                .getResourceAsStream("generator/template/universal/template.yml"));
        System.out.println(templateConfig);
    }

}
