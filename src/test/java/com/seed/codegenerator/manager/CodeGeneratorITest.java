package com.seed.codegenerator.manager;

import com.seed.codegenerator.generator.service.table.entity.DataBaseTableDescribe;
import org.jeecgframework.AbstractUnitTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author gchiaway
 *         日期: 2020-02-14
 *         时间: 21:10
 */
public class CodeGeneratorITest extends AbstractUnitTest {

    @Autowired
    private CodeGeneratorI codeGenerator;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void generateCode() throws Exception {
        DataBaseTableDescribe dataBaseTableDescribe = new DataBaseTableDescribe("cnp", "t");
        codeGenerator.generateUniversalCode(dataBaseTableDescribe);
    }

    @Test
    public void generateJeecgCode() throws Exception {
    }

    @Test
    public void generateSSMCode() throws Exception {
    }

    @Test
    public void generateUniversalCode() throws Exception {
    }

}