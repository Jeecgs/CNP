package com.seed.codegenerator.generator.service.base;

import com.seed.codegenerator.generator.type.GenTypeEnum;
import org.jeecgframework.AbstractUnitTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * @author gchiaway
 *         日期: 2020-02-14
 *         时间: 21:13
 */
public class BaseGeneratorITest extends AbstractUnitTest {

    @Autowired()
    @Qualifier("baseGeneratorService")
    private BaseGeneratorI baseGenerator;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getConfiguration() throws Exception {
        freemarker.template.Configuration cfg;
        try {
            cfg = baseGenerator.getConfiguration(GenTypeEnum.UNIVERSAL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}