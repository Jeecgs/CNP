package com.seed.codegenerator.generator.service.table;

import com.seed.codegenerator.generator.entity.DataBaseTable;
import com.seed.codegenerator.generator.service.table.entity.DataBaseTableDescribe;
import org.jeecgframework.AbstractUnitTest;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author gchiaway
 *         日期: 2020-02-14
 *         时间: 18:13
 */
public class DataBaseTableServiceITest extends AbstractUnitTest {

    @Autowired
    private DataBaseTableServiceI tester;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getDataBaseName() throws Exception {
    }

    @Test
    public void setDataBaseName() throws Exception {
    }

    @Test
    public void listDataBaseTablesByTableNamesPrefix() throws Exception {
        //TODO: Test goes here...
        DataBaseTableDescribe dataBaseTableDescribe = new DataBaseTableDescribe("test_", "table");
        List<DataBaseTable> all = tester.listDataBaseTablesByTableNamesPrefix(dataBaseTableDescribe);
        Assert.assertTrue("全匹配失败", all.size() > 0);
        for (DataBaseTable dataBaseTable : all) {
            System.out.println(dataBaseTable.toString());
        }

        List<DataBaseTable> some = tester.listDataBaseTablesByTableNamesPrefix(dataBaseTableDescribe, "test_table_simple");
        Assert.assertTrue("部分匹配失败", some.size() > 0);
        for (DataBaseTable dataBaseTable : some) {
            System.out.println(dataBaseTable.toString());
        }
    }

}