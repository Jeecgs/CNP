package com.business.app.cnp.note.service;

import com.business.app.cnp.note.query.NotePageQuery;
import com.business.app.cnp.note.vo.NoteDetailVO;
import com.business.app.cnp.note.vo.NotePage;
import org.jeecgframework.AbstractUnitTest;
import org.junit.Test;

import javax.annotation.Resource;

import static org.junit.Assert.*;

public class NoteServiceITest extends AbstractUnitTest {

    @Resource
    private NoteServiceI noteServiceI;
    @Test
    public void getNoteDetailVO() {
        try {
            NoteDetailVO noteDetailVO=noteServiceI.getNoteDetailVO("1");
            System.out.println(noteDetailVO.toString()+"=====================");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void getNotePage() {
        NotePageQuery notePageQuery = new NotePageQuery();
        notePageQuery.setPage(1);
        notePageQuery.setSize(5);
        notePageQuery.setNoteName("2");
        try {
            NotePage dbOwnerPage=noteServiceI.getNotePage(notePageQuery);
            System.out.println(dbOwnerPage.getList()+"====================="+dbOwnerPage.getTotalPage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}