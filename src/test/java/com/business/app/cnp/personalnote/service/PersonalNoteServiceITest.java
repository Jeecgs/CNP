package com.business.app.cnp.personalnote.service;

import com.business.app.cnp.personalnote.vo.PersonalNoteDetailVO;
import org.jeecgframework.AbstractUnitTest;
import org.junit.Test;
import javax.annotation.Resource;


public class PersonalNoteServiceITest extends AbstractUnitTest {

    @Resource
    private PersonalNoteServiceI personalNoteServiceI;
    @Test
    public void listPersonalNoteDetailVOs() {
        try {
            PersonalNoteDetailVO personalNoteDetailVO = personalNoteServiceI.getPersonalNoteDetailVO("1");
            System.out.println(personalNoteDetailVO.toString()+"==============");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}