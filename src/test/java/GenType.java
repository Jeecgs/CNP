/**
 * 生成策略的枚举类
 *
 * @author gchiaway
 *         日期: 2019-11-06
 *         时间: 20:15
 */
public enum GenType {
    /**
     * 生成ssm风格的代码
     */
    SSM("ssm", "ssm"),
    /**
     * 生成jeecg风格的代码
     */
    JEECG("jeecg", "jeecg"),
    /**
     * 生成通用型模板
     */
    UNIVERSAL("universal", "universal"),;

    /**
     * 类型名称
     */
    private String type;

    /**
     * 类型路径
     */
    private String path;


    GenType(String type, String path) {
        this.type = type;
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
