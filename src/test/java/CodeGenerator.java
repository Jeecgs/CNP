import com.google.common.base.CaseFormat;
import com.seed.codegenerator.generator.entity.DataBaseTable;
import freemarker.template.TemplateExceptionHandler;
import org.apache.commons.lang3.StringUtils;
import org.jeecgframework.AbstractUnitTest;
import org.jeecgframework.core.common.service.CommonService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.*;
import org.mybatis.generator.internal.DefaultShellCallback;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

/**
 * 代码生成器，根据数据表名称生成对应的Model、Mapper、Service、Controller简化开发。
 */
public class CodeGenerator extends AbstractUnitTest {

    @Autowired
    private CommonService commonService;

    //数据库名称
    @Value("${project.database.name}")
    private String DATABASE_NAME;

    //项目生成策略
    private static final GenType GENERATION_STRATEGY = GenType.SSM;
    //项目在硬盘上的基础路径
    private static final String PROJECT_PATH = System.getProperty("user.dir");
    //模板位置
    private static final String TEMPLATE_FILE_PATH = PROJECT_PATH + "/src/main/resources/manager/template/" + GENERATION_STRATEGY.getType();

    /**
     * Mapper插件基础接口的完全限定名
     */
    private static final String MAPPER_INTERFACE_REFERENCE =  "com.seed.core.mapper.Mapper";

    //业务包路径
    @Value("${project.api.package}")
    private String API_BASE_PACKAGE;

    @Value("${project.business.package}")
    private String BUSINESS_BASE_PACKAGE;

    //JDBC配置，请修改为你项目的实际配置
    @Value("${jdbc.url.jeecg}")
    private String JDBC_URL;
    @Value("${jdbc.username.jeecg}")
    private String JDBC_USERNAME;
    @Value("${jdbc.password.jeecg}")
    private String JDBC_PASSWORD;

    private static final String JDBC_DIVER_CLASS_NAME = "com.mysql.jdbc.Driver";

    //java文件路径
    private static final String JAVA_PATH = "/src/main/java";
    //资源文件路径
    private static final String RESOURCES_PATH = "/src/main/resources";

    //@author
    private static final String AUTHOR = "CodeGenerator";
    //@date
    private static final String DATE = new SimpleDateFormat("yyyy年MM月dd日").format(new Date());

    @Test
    public void CodeGenerator() throws Exception {
        //前缀匹配
//        genCodeByTableNamesPrefix("orchard_db");
          //genCodeByTableNamesPrefix("test_table");

        //指定表名
        List<String> tableNames = new ArrayList<>();
        tableNames.add("sys_dictionaries");

        genCodeByTableNames("sys",tableNames);
    }

    /**
     * 通过数据表名称生成代码，Model 名称通过解析数据表名称获得，下划线转大驼峰的形式。
     * 如输入表名称 "t_user_detail" 将生成 TUserDetail、TUserDetailMapper、TUserDetailService ...
     *
     * @param tableNames 数据表名称...
     */
    private void genCodeByTableNames(String tableNamesPrefix,List<String> tableNames) {
        for (String tableName : tableNames) {
            //TODO：指定表名生成
            DataBaseTable dbTable = new DataBaseTable();
            dbTable.setTableName(tableName);

            genCodeByCodeGeneratorDBTable(dbTable, tableNamesPrefix);
        }
    }

    private void genCodeByCodeGeneratorDBTable(DataBaseTable dbTable) {

//        String entityNameWithUnderline = dbTable.getTableName();
//        dbTable.setEntityNameWithUnderline(entityNameWithUnderline);
//
//        String entityName = entityNameWithUnderline.replaceAll("_", "");
//        dbTable.setEntityName(entityName);
//
//        String entityNameHump = tableNameConvertUpperCamel(entityNameWithUnderline);
//        dbTable.setEntityNameHump(entityNameHump);
//
//        String entityNameHumpFirstLow = tableNameConvertLowerCamel(entityNameWithUnderline);
//        dbTable.setEntityNameHumpFirstLow(entityNameHumpFirstLow);
//
//        String entityNameWithPre = dbTable.getTableName().replaceAll("_", "");
//        dbTable.setEntityNameWithPre(entityNameWithPre);

        genCodeByCustomModelName(dbTable);

    }

    /**
     * 根据数据库前缀生成代码
     * 例如：ipd-table1，ipd-table2 可以用ipd进行匹配
     *
     * @param tableNamesPrefix 数据表名称...
     */
    private void genCodeByTableNamesPrefix(String tableNamesPrefix) {
        //判断前缀末端是否添加了下划线
        if (!tableNamesPrefix.substring(tableNamesPrefix.length() - 1, tableNamesPrefix.length()).equals("_")) {
            //末尾没有有下划线
            StringBuilder sb = new StringBuilder(tableNamesPrefix);
            sb.insert(tableNamesPrefix.length(), "_");
            tableNamesPrefix = sb.toString();
        }

        String sql = "SELECT TABLE_NAME, TABLE_COMMENT FROM information_schema.TABLES  WHERE table_schema = '" + DATABASE_NAME + "' AND TABLE_NAME LIKE CONCAT(\"" + tableNamesPrefix + "\", \"%\");";
        List dbTableList = commonService.findListbySql(sql);

        for (int i = 0; i < dbTableList.size(); i++) {
            Object[] dbo = (Object[]) dbTableList.get(i);

            DataBaseTable dbTable = new DataBaseTable();
            dbTable.setTableName(dbo[0].toString());
            dbTable.setTableComment(dbo[1].toString());

            genCodeByCodeGeneratorDBTable(dbTable, tableNamesPrefix);
        }
    }

    private void genCodeByCodeGeneratorDBTable(DataBaseTable dbTable, String tableNamesPrefix) {
//
//        String entityNameWithUnderline = dbTable.getTableName().split(tableNamesPrefix)[1];
//        dbTable.setEntityNameWithUnderline(entityNameWithUnderline);
//
//        String entityName = entityNameWithUnderline.replaceAll("_", "");
//        dbTable.setEntityName(entityName);
//
//        String entityNameHump = tableNameConvertUpperCamel(entityNameWithUnderline);
//        dbTable.setEntityNameHump(entityNameHump);
//
//        String entityNameHumpFirstLow = tableNameConvertLowerCamel(entityNameWithUnderline);
//        dbTable.setEntityNameHumpFirstLow(entityNameHumpFirstLow);
//
//        String entityNameWithPre = dbTable.getTableName().replaceAll("_", "");
//        dbTable.setEntityNameWithPre(entityNameWithPre);

        genCodeByCustomModelName(dbTable);
    }

    private void genCodeByCustomModelName(DataBaseTable dbTable) {
        Map<String, Object> data = new HashMap<>();
        String tableName = dbTable.getTableName();
        String tableComment = dbTable.getTableComment();
        String entityName = dbTable.getEntityName();
        String entityNameHump = dbTable.getEntityNameHump();
        String entityNameHumpFirstLow = dbTable.getEntityNameHumpFirstLow();
        //String entityNameWithPre = dbTable.getEntityNameWithPre();

        data.put("date", DATE);
        data.put("author", AUTHOR);

        tableComment = StringUtils.isEmpty(tableComment) ? tableNameConvertUpperCamel(tableName) : tableComment;
        data.put("tableComment", tableComment);
        data.put("tableName", tableName);
        data.put("entityName", entityName);
        data.put("entityNameHump", entityNameHump);
        data.put("entityNameHumpFirstLow", entityNameHumpFirstLow);
        //data.put("entityNameWithPre", entityNameWithPre);

        String modelNameUpperCamel = tableNameConvertUpperCamel(tableName);
        data.put("baseRequestMapping", modelNameConvertMappingPath(modelNameUpperCamel));
        data.put("modelNameUpperCamel", modelNameUpperCamel);
        data.put("modelNameLowerCamel", CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, modelNameUpperCamel));
        data.put("modelNameAllLow", CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, modelNameUpperCamel));

        data.put("basePackage", API_BASE_PACKAGE);
        data.put("businessBasePackage", BUSINESS_BASE_PACKAGE);


        genFrom(data, dbTable);
        genVO(data, dbTable);

        //genController(data, dbTable);
        genService(data, dbTable);
        genDao(data, dbTable);
    }

    private void genFrom(Map<String, Object> data, DataBaseTable dbTable) {
        try {
            String entityName = dbTable.getEntityName();
            String entityNameHump = dbTable.getEntityNameHump();

            freemarker.template.Configuration cfg = this.getConfiguration();

            //生成searchFrom
            File searchFrom = new File(PROJECT_PATH + JAVA_PATH + packageConvertPath(API_BASE_PACKAGE) + entityName + "/from/" + entityNameHump + "SearchFrom.java");
            if (!searchFrom.getParentFile().exists()) {
                searchFrom.getParentFile().mkdirs();
            }
            cfg.getTemplate("from/search-from.ftl").process(data, new FileWriter(searchFrom));
            //生成simpleFrom
            File simpleFrom = new File(PROJECT_PATH + JAVA_PATH + packageConvertPath(API_BASE_PACKAGE) + entityName + "/from/" + entityNameHump + "From.java");
            if (!simpleFrom.getParentFile().exists()) {
                simpleFrom.getParentFile().mkdirs();
            }
            cfg.getTemplate("from/simple-from.ftl").process(data, new FileWriter(simpleFrom));


            System.out.println(dbTable.getEntityName() + "from生成成功");
        } catch (Exception e) {
            throw new RuntimeException("生成from失败", e);
        }
    }

    private void genVO(Map<String, Object> data, DataBaseTable dbTable) {
        try {
            String entityName = dbTable.getEntityName();
            String entityNameHump = dbTable.getEntityNameHump();

            freemarker.template.Configuration cfg = this.getConfiguration();

            //生成VO
            File VO = new File(PROJECT_PATH + JAVA_PATH + packageConvertPath(API_BASE_PACKAGE) + entityName + "/VO/" + entityNameHump + "VO.java");
            if (!VO.getParentFile().exists()) {
                VO.getParentFile().mkdirs();
            }
            cfg.getTemplate("VO/simple-VO.ftl").process(data, new FileWriter(VO));
            //生成listVO
            File listVO = new File(PROJECT_PATH + JAVA_PATH + packageConvertPath(API_BASE_PACKAGE) + entityName + "/VO/" + entityNameHump + "ListVO.java");
            if (!listVO.getParentFile().exists()) {
                listVO.getParentFile().mkdirs();
            }
            cfg.getTemplate("VO/app-base-brief-VO.ftl").process(data, new FileWriter(listVO));
            //生成detailVO
            File detailVO = new File(PROJECT_PATH + JAVA_PATH + packageConvertPath(API_BASE_PACKAGE) + entityName + "/VO/" + entityNameHump + "DetailVO.java");
            if (!detailVO.getParentFile().exists()) {
                detailVO.getParentFile().mkdirs();
            }
            cfg.getTemplate("VO/app-base-detail-VO.ftl").process(data, new FileWriter(detailVO));
            //生成page
            File page = new File(PROJECT_PATH + JAVA_PATH + packageConvertPath(API_BASE_PACKAGE) + entityName + "/VO/" + entityNameHump + "Page.java");
            if (!page.getParentFile().exists()) {
                page.getParentFile().mkdirs();
            }
            cfg.getTemplate("VO/app-base-page.ftl").process(data, new FileWriter(page));

            System.out.println(dbTable.getEntityName() + "VO生成成功");
        } catch (Exception e) {
            throw new RuntimeException("生成VO失败", e);
        }
    }

    private void genController(Map<String, Object> data, DataBaseTable dbTable) {
        try {
            String entityName = dbTable.getEntityName();
            String entityNameHump = dbTable.getEntityNameHump();
            freemarker.template.Configuration cfg = getConfiguration();

            File file = new File(PROJECT_PATH + JAVA_PATH + packageConvertPath(API_BASE_PACKAGE) + entityName + "/controller/" + entityNameHump + "Controller.java");
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            //TODO:选择post风格或者restful风格
            cfg.getTemplate("controller/controller-restful.ftl").process(data, new FileWriter(file));
            //cfg.getTemplate("controller/controller.ftl").process(data, new FileWriter(file));

            System.out.println(dbTable.getEntityName() + "Controller.java 生成成功");
        } catch (Exception e) {
            throw new RuntimeException("生成Controller失败", e);
        }
    }

    private void genService(Map<String, Object> data, DataBaseTable dbTable) {
        try {
            String entityName = dbTable.getEntityName();
            String entityNameHump = dbTable.getEntityNameHump();

            freemarker.template.Configuration cfg = getConfiguration();

            File file = new File(PROJECT_PATH + JAVA_PATH + packageConvertPath(API_BASE_PACKAGE) + entityName + "/service/" + entityNameHump + "Service.java");
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            cfg.getTemplate("service/service.ftl").process(data, new FileWriter(file));
            System.out.println(entityNameHump + "Service.java 生成成功");

            File file1 = new File(PROJECT_PATH + JAVA_PATH + packageConvertPath(API_BASE_PACKAGE) + entityName + "/service/impl/" + entityNameHump + "ServiceImpl.java");
            if (!file1.getParentFile().exists()) {
                file1.getParentFile().mkdirs();
            }
            cfg.getTemplate("service/impl/service-impl.ftl").process(data, new FileWriter(file1));
            System.out.println(entityNameHump + "ServiceImpl.java 生成成功");
        } catch (Exception e) {
            throw new RuntimeException("生成Service失败", e);
        }
    }

    private void genDao(Map<String, Object> data, DataBaseTable dbTable) {
        try {
            switch (GENERATION_STRATEGY){
                case SSM:{
                    this.genSsmDao(data, dbTable);
                    break;
                }
                case JEECG:{
                    this.genJeecgDao(data, dbTable);
                    break;
                }
            }
            System.out.println(dbTable.getEntityName() + "dao生成成功");
        } catch (Exception e) {
            throw new RuntimeException("生成dao失败", e);
        }
    }

    private void genSsmDao(Map<String, Object> data, DataBaseTable dbTable) throws Exception{
        Context context = new Context(ModelType.FLAT);
        context.setId("Potato");
        context.setTargetRuntime("MyBatis3Simple");
        context.addProperty(PropertyRegistry.CONTEXT_BEGINNING_DELIMITER, "`");
        context.addProperty(PropertyRegistry.CONTEXT_ENDING_DELIMITER, "`");

        JDBCConnectionConfiguration jdbcConnectionConfiguration = new JDBCConnectionConfiguration();
        jdbcConnectionConfiguration.setConnectionURL(JDBC_URL);
        jdbcConnectionConfiguration.setUserId(JDBC_USERNAME);
        jdbcConnectionConfiguration.setPassword(JDBC_PASSWORD);
        jdbcConnectionConfiguration.setDriverClass(JDBC_DIVER_CLASS_NAME);
        context.setJdbcConnectionConfiguration(jdbcConnectionConfiguration);

        PluginConfiguration pluginConfiguration = new PluginConfiguration();
        pluginConfiguration.setConfigurationType("tk.mybatis.mapper.manager.MapperPlugin");
        pluginConfiguration.addProperty("mappers", MAPPER_INTERFACE_REFERENCE);
        context.addPluginConfiguration(pluginConfiguration);

        JavaModelGeneratorConfiguration javaModelGeneratorConfiguration = new JavaModelGeneratorConfiguration();
        javaModelGeneratorConfiguration.setTargetProject(PROJECT_PATH + JAVA_PATH);
        javaModelGeneratorConfiguration.setTargetPackage(API_BASE_PACKAGE + "." + dbTable.getEntityName() + ".model");
        context.setJavaModelGeneratorConfiguration(javaModelGeneratorConfiguration);

        SqlMapGeneratorConfiguration sqlMapGeneratorConfiguration = new SqlMapGeneratorConfiguration();
        sqlMapGeneratorConfiguration.setTargetProject(PROJECT_PATH + RESOURCES_PATH);
        sqlMapGeneratorConfiguration.setTargetPackage("mapper");
        context.setSqlMapGeneratorConfiguration(sqlMapGeneratorConfiguration);

        JavaClientGeneratorConfiguration javaClientGeneratorConfiguration = new JavaClientGeneratorConfiguration();
        javaClientGeneratorConfiguration.setTargetProject(PROJECT_PATH + JAVA_PATH);
        javaClientGeneratorConfiguration.setTargetPackage(API_BASE_PACKAGE + "." + dbTable.getEntityName() + ".dao");
        javaClientGeneratorConfiguration.setConfigurationType("XMLMAPPER");
        context.setJavaClientGeneratorConfiguration(javaClientGeneratorConfiguration);

        TableConfiguration tableConfiguration = new TableConfiguration(context);
        tableConfiguration.setTableName(dbTable.getTableName());
        tableConfiguration.setDomainObjectName(dbTable.getEntityNameHump());
        tableConfiguration.setGeneratedKey(new GeneratedKey("id", "Mysql", true, null));
        context.addTableConfiguration(tableConfiguration);

        List<String> warnings;
        MyBatisGenerator generator;
        try {
            Configuration config = new Configuration();
            config.addContext(context);
            config.validate();
            boolean overwrite = true;
            DefaultShellCallback callback = new DefaultShellCallback(overwrite);
            warnings = new ArrayList<String>();
            generator = new MyBatisGenerator(config, callback, warnings);
            generator.generate(null);
        } catch (Exception e) {
            throw new RuntimeException("生成Model和Mapper失败", e);
        }
    }

    private void genJeecgDao(Map<String, Object> data, DataBaseTable dbTable) throws Exception{
        String entityName = dbTable.getEntityName();
        String entityNameHump = dbTable.getEntityNameHump();
        freemarker.template.Configuration cfg = this.getConfiguration();

        //生成miniDao
        File miniDao = new File(PROJECT_PATH + JAVA_PATH + packageConvertPath(API_BASE_PACKAGE) + entityName + "/dao/" + entityNameHump + "MiniDao.java");
        if (!miniDao.getParentFile().exists()) {
            miniDao.getParentFile().mkdirs();
        }
        cfg.getTemplate("dao/mini-dao.ftl").process(data, new FileWriter(miniDao));
        //生成simpleMiniDao
        File simpleMiniDao = new File(PROJECT_PATH + JAVA_PATH + packageConvertPath(API_BASE_PACKAGE) + entityName + "/sql/" + entityNameHump + "MiniDao_get" + entityNameHump + "VOListBy" + entityNameHump + "From.sql");
        if (!simpleMiniDao.getParentFile().exists()) {
            simpleMiniDao.getParentFile().mkdirs();
        }
        cfg.getTemplate("sql/simple-minidao.ftl").process(data, new FileWriter(simpleMiniDao));
        //生成miniDao-page
        File miniDaoPage = new File(PROJECT_PATH + JAVA_PATH + packageConvertPath(API_BASE_PACKAGE) + entityName + "/sql/" + entityNameHump + "MiniDao_get" + entityNameHump + "PageBy" + entityNameHump + "SearchFrom.sql");
        if (!miniDaoPage.getParentFile().exists()) {
            miniDaoPage.getParentFile().mkdirs();
        }
        cfg.getTemplate("sql/minidao-page.ftl").process(data, new FileWriter(miniDaoPage));
        //生成miniDao
        File miniDaoPageTotal = new File(PROJECT_PATH + JAVA_PATH + packageConvertPath(API_BASE_PACKAGE) + entityName + "/sql/" + entityNameHump + "MiniDao_get" + entityNameHump + "PageBy" + entityNameHump + "SearchFromTotal.sql");
        if (!miniDaoPageTotal.getParentFile().exists()) {
            miniDaoPageTotal.getParentFile().mkdirs();
        }
        cfg.getTemplate("sql/minidao-page-total.ftl").process(data, new FileWriter(miniDaoPageTotal));
    }

    private freemarker.template.Configuration getConfiguration() throws IOException {
        freemarker.template.Configuration cfg = new freemarker.template.Configuration();
        cfg.setDirectoryForTemplateLoading(new File(TEMPLATE_FILE_PATH));
        cfg.setDefaultEncoding("UTF-8");
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.IGNORE_HANDLER);
        return cfg;
    }

    private String tableNameConvertLowerCamel(String tableName) {
        return CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, tableName.toLowerCase());
    }

    private String tableNameConvertUpperCamel(String tableName) {
        return CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, tableName.toLowerCase());

    }

    private String tableNameConvertMappingPath(String tableName) {
        tableName = tableName.toLowerCase();//兼容使用大写的表名
        return "/" + (tableName.contains("_") ? tableName.replaceAll("_", "/") : tableName);
    }

    private String modelNameConvertMappingPath(String modelName) {
        String tableName = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, modelName);
        return tableNameConvertMappingPath(tableName);
    }

    private String packageConvertPath(String packageName) {
        return String.format("/%s/", packageName.contains(".") ? packageName.replaceAll("\\.", "/") : packageName);
    }

}
