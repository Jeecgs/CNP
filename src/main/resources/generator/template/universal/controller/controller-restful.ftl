package ${basePackage}.${entityNameWithOutUnderline}.controller;

import com.alibaba.fastjson.JSONObject;
import com.seed.core.exception.ServiceException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecgframework.core.common.model.json.AjaxJson;
import org.jeecgframework.p3.core.logger.Logger;
import org.jeecgframework.p3.core.logger.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import com.seed.core.controller.AppBaseController;

import ${basePackage}.${entityNameWithOutUnderline}.from.*;
import ${basePackage}.${entityNameWithOutUnderline}.vo.*;
import ${basePackage}.${entityNameWithOutUnderline}.model.*;
import ${basePackage}.${entityNameWithOutUnderline}.query.*;
import ${basePackage}.${entityNameWithOutUnderline}.service.*;

import java.util.HashMap;
import java.util.Map;
import java.util.List;

/**
 * @author ${author}
 * @date ${date}
 */
@RestController
@Api(value = "${entityNameHump}Controller", description = "${tableComment}管理", tags = "${entityNameHump}Controller")
@RequestMapping("/app/v1/${entityNameHump}Controller")
public class ${entityNameHump}Controller extends AppBaseController {

    private static final Logger logger = LoggerFactory.getLogger(${entityNameHump}Controller.class);

    private final ${entityNameHump}ServiceI ${entityNameHumpFirstLow}Service;

    @Autowired
    public ${entityNameHump}Controller(${entityNameHump}ServiceI ${entityNameHumpFirstLow}Service) {
        this.${entityNameHumpFirstLow}Service = ${entityNameHumpFirstLow}Service;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "根据id获取${tableComment}详细信息")
    public AjaxJson get${entityNameHump}VO(@PathVariable String id) {
        logger.info("[api-v1]${entityNameHump}Controller.get${entityNameHump}VO接口,根据id获取${tableComment}详细信息");

        AjaxJson result = new AjaxJson();

        if (StringUtils.isEmpty(id)) {
            result.setMsg("id不能为空!");
            result.setSuccess(false);
            return result;
        }

        ${entityNameHump}DetailVO ${entityNameHumpFirstLow}DetailVO  = null;
        try {
            ${entityNameHumpFirstLow}DetailVO  = ${entityNameHumpFirstLow}Service.get${entityNameHump}DetailVO(id);
        } catch (ServiceException e) {
            e.printStackTrace();
            result.setSuccess(false);
            result.setMsg("查询失败");
            return result;
        }

        Map<String, Object> map = new HashMap<>(16);
        map.put("${entityNameHumpFirstLow}DetailVO", ${entityNameHumpFirstLow}DetailVO);
        result.setObj(map);
        return result;
    }

}
