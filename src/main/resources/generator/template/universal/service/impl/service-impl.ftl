package ${basePackage}.${entityNameWithOutUnderline}.service.impl;

import ${basePackage}.${entityNameWithOutUnderline}.vo.*;
import ${basePackage}.${entityNameWithOutUnderline}.query.*;
import ${basePackage}.${entityNameWithOutUnderline}.dao.*;
import ${basePackage}.${entityNameWithOutUnderline}.model.*;
import ${basePackage}.${entityNameWithOutUnderline}.service.*;
import ${basePackage}.${entityNameWithOutUnderline}.manager.*;
import ${basePackage}.${entityNameWithOutUnderline}.from.*;
import com.seed.core.exception.DAOException;
import com.seed.core.exception.ServiceException;
import com.seed.core.service.impl.AbstractService;
import org.jeecgframework.core.util.MyBeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * @author ${author}
 * @date ${date}
 */
@Service("${entityNameHumpFirstLow}Service")
@Transactional(rollbackFor = ServiceException.class)
public class ${entityNameHump}ServiceImpl extends AbstractService<${entityNameHump}> implements ${entityNameHump}ServiceI {

    private final ${entityNameHump}Mapper ${entityNameHumpFirstLow}Mapper;

    private final ${entityNameHump}ManagerI ${entityNameHumpFirstLow}Manager;

    @Autowired
    public ${entityNameHump}ServiceImpl(${entityNameHump}ManagerI ${entityNameHumpFirstLow}Manager, ${entityNameHump}Mapper ${entityNameHumpFirstLow}Mapper) {
        this.${entityNameHumpFirstLow}Manager = ${entityNameHumpFirstLow}Manager;
        this.${entityNameHumpFirstLow}Mapper = ${entityNameHumpFirstLow}Mapper;
    }

    /**
     * 转换为VO对象
     *
     * @param simpleList model列表
     * @return VO对象列表
     */
    private List<${entityNameHump}DetailVO> listDetailVOS(List<${entityNameHump}> ${entityNameHumpFirstLow}List) {
        List<${entityNameHump}DetailVO> ${entityNameHumpFirstLow}DetailVOList = new ArrayList<>();
        for (${entityNameHump} ${entityNameHumpFirstLow} : ${entityNameHumpFirstLow}List) {
            ${entityNameHump}DetailVO ${entityNameHumpFirstLow}DetailVO = new ${entityNameHump}DetailVO(${entityNameHumpFirstLow});
            ${entityNameHumpFirstLow}DetailVOList.add(${entityNameHumpFirstLow}DetailVO);
        }
        return ${entityNameHumpFirstLow}DetailVOList;
    }

    /**
     * 转换为VO对象
     *
     * @param simpleList model列表
     * @return VO对象列表
     */
    private List<${entityNameHump}BriefVO> listBriefVOS(List<${entityNameHump}> ${entityNameHumpFirstLow}List) {
        List<${entityNameHump}BriefVO> ${entityNameHumpFirstLow}BriefVOList = new ArrayList<>();
        for (${entityNameHump} ${entityNameHumpFirstLow} : ${entityNameHumpFirstLow}List) {
            ${entityNameHump}BriefVO ${entityNameHumpFirstLow}BriefVO = new ${entityNameHump}BriefVO(${entityNameHumpFirstLow});
            ${entityNameHumpFirstLow}BriefVOList.add(${entityNameHumpFirstLow}BriefVO);
        }
        return ${entityNameHumpFirstLow}BriefVOList;
    }

    /**
     * 持久化
     *
     * @param ${entityNameHumpFirstLow}From 需要保存的对象
     * @return 是否成功
     * @throws ServiceException 业务层异常
     */
    @Override
    public Boolean save(${entityNameHump}From ${entityNameHumpFirstLow}From) throws ServiceException {
        ${entityNameHump} ${entityNameHumpFirstLow} = new ${entityNameHump}();
        try {
            MyBeanUtils.copyBeanNotNull2Bean(${entityNameHumpFirstLow}From, ${entityNameHumpFirstLow});
        } catch (Exception e) {
            throw new ServiceException(e, ${entityNameHumpFirstLow}From);
        }
        Boolean isSuccess;
        try {
            isSuccess = this.save(${entityNameHumpFirstLow});
        } catch (ServiceException e) {
            throw new ServiceException(e, ${entityNameHumpFirstLow});
        }
        return isSuccess;
    }

    /**
     * 根据id获取详情DetailVO
     *
     * @param ${entityNameHumpFirstLow}Id 对象id
     * @return 详情DetailVO对象
     * @throws ServiceException 业务层异常
     */
    @Override
    public ${entityNameHump}DetailVO get${entityNameHump}DetailVO(String ${entityNameHumpFirstLow}Id) throws ServiceException {
        ${entityNameHump} ${entityNameHumpFirstLow};
        try {
            ${entityNameHumpFirstLow} = ${entityNameHumpFirstLow}Manager.getModel(${entityNameHumpFirstLow}Id);
        } catch (DAOException e) {
            throw new ServiceException(e, ${entityNameHumpFirstLow}Id);
        }
        if (!StringUtils.isEmpty(${entityNameHumpFirstLow})){
            return new ${entityNameHump}DetailVO(${entityNameHumpFirstLow});
        } else {
            return new ${entityNameHump}DetailVO();
        }
    }

    /**
     * 获取DetailVOList
     *
     * @return DetailVO的list对象
     * @throws ServiceException 业务层异常
     */
    @Override
    public List<${entityNameHump}DetailVO> list${entityNameHump}DetailVOs() throws ServiceException {
        List<${entityNameHump}> ${entityNameHumpFirstLow}List;
        try {
            ${entityNameHumpFirstLow}List = ${entityNameHumpFirstLow}Manager.listAllModels();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        List<${entityNameHump}DetailVO> ${entityNameHumpFirstLow}DetailVOList = this.listDetailVOS(${entityNameHumpFirstLow}List);
        return ${entityNameHumpFirstLow}DetailVOList;
    }

    /**
     * 根据Query对象获取VOList
     *
     * @param ${entityNameHumpFirstLow}Query 条件搜索对象
     * @return VO的list对象
     * @throws ServiceException 业务层异常
     */
    @Override
    public List<${entityNameHump}DetailVO> list${entityNameHump}DetailVOs(${entityNameHump}Query ${entityNameHumpFirstLow}Query) throws ServiceException {
        List<${entityNameHump}> ${entityNameHumpFirstLow}List;
        try {
            ${entityNameHumpFirstLow}List  = ${entityNameHumpFirstLow}Manager.list${entityNameHump}s(${entityNameHumpFirstLow}Query);
        } catch (DAOException e) {
            throw new ServiceException(e, ${entityNameHumpFirstLow}Query);
        }
        List<${entityNameHump}DetailVO> ${entityNameHumpFirstLow}DetailVOList = this.listDetailVOS(${entityNameHumpFirstLow}List);
        return ${entityNameHumpFirstLow}DetailVOList;
    }


    /**
     * 根据PageQuery对象获取page对象
     *
     * @param ${entityNameHumpFirstLow}PageQuery 条件搜索对象
     * @return page对象
     * @throws ServiceException 业务层异常
     */
    @Override
    public ${entityNameHump}Page get${entityNameHump}Page(${entityNameHump}PageQuery ${entityNameHumpFirstLow}PageQuery) throws ServiceException {
        List<${entityNameHump}> ${entityNameHumpFirstLow}List;
        int total;
        try {
            ${entityNameHumpFirstLow}List  = ${entityNameHumpFirstLow}Manager.list${entityNameHump}s(${entityNameHumpFirstLow}PageQuery);
            total = ${entityNameHumpFirstLow}Manager.count${entityNameHump}s(${entityNameHumpFirstLow}PageQuery);
        }catch (DAOException e){
            throw new ServiceException(e, ${entityNameHumpFirstLow}PageQuery);
        }
        List<${entityNameHump}BriefVO> ${entityNameHumpFirstLow}BriefVOList = this.listBriefVOS(${entityNameHumpFirstLow}List);
        return new ${entityNameHump}Page(${entityNameHumpFirstLow}PageQuery, total, ${entityNameHumpFirstLow}BriefVOList);
    }

}
