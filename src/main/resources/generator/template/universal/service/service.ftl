package ${basePackage}.${entityNameWithOutUnderline}.service;

import com.seed.core.service.Service;
import com.seed.core.exception.ServiceException;
import ${basePackage}.${entityNameWithOutUnderline}.model.*;
import ${basePackage}.${entityNameWithOutUnderline}.vo.*;
import ${basePackage}.${entityNameWithOutUnderline}.query.*;
import ${basePackage}.${entityNameWithOutUnderline}.from.*;
import java.util.List;

/**
 * @author ${author}
 * @date ${date}
 */
public interface ${entityNameHump}ServiceI extends Service<${entityNameHump}>{

    /**
     * 持久化
     *
     * @param ${entityNameHumpFirstLow}From 需要保存的对象
     * @return 是否成功
     * @throws ServiceException 业务层异常
     */
    Boolean save(${entityNameHump}From ${entityNameHumpFirstLow}From) throws ServiceException;

    /**
     * 根据id获取详情VO
     *
     * @param ${entityNameHumpFirstLow}Id 对象id
     * @return 详情VO对象
     * @throws ServiceException 业务层异常
     */
    ${entityNameHump}DetailVO get${entityNameHump}DetailVO(String ${entityNameHumpFirstLow}Id) throws ServiceException;

    /**
     * 获取DetailVOList
     *
     * @return DetailVO的list对象
     * @throws ServiceException 业务层异常
     */
    List<${entityNameHump}DetailVO> list${entityNameHump}DetailVOs() throws ServiceException;

    /**
     * 根据Query对象获取DetailVOList
     *
     * @param ${entityNameHumpFirstLow}Query 条件搜索对象
     * @return DetailVO的list对象
     * @throws ServiceException 业务层异常
     */
    List<${entityNameHump}DetailVO> list${entityNameHump}DetailVOs(${entityNameHump}Query ${entityNameHumpFirstLow}Query) throws ServiceException;

    /**
     * 根据PageQuery对象获取page对象
     *
     * @param ${entityNameHumpFirstLow}PageQuery 条件搜索对象
     * @return page对象
     * @throws ServiceException 业务层异常
     */
    ${entityNameHump}Page get${entityNameHump}Page(${entityNameHump}PageQuery ${entityNameHumpFirstLow}PageQuery) throws ServiceException;
}
