package ${basePackage}.${entityNameWithOutUnderline}.vo;

import ${basePackage}.${entityNameWithOutUnderline}.model.*;
import com.seed.core.vo.BaseVO;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * ${tableComment}的detailVO对象
 * @author ${author}
 * @date ${date}
 */
@Getter
@Setter
@ToString
public class ${entityNameHump}DetailVO extends BaseVO {

    public ${entityNameHump}DetailVO() {

    }

    public ${entityNameHump}DetailVO(${entityNameHump} ${entityNameHumpFirstLow}) {

    }


}