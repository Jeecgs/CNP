package ${basePackage}.${entityNameWithOutUnderline}.vo;

import com.seed.core.vo.BasePage;
import com.seed.core.query.BasePageQuery;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * ${tableComment}的page对象
 * @author ${author}
 * @date ${date}
 */
@Getter
@Setter
@ToString
public class ${entityNameHump}Page extends BasePage<${entityNameHump}BriefVO>{

    public ${entityNameHump}Page() {
        super();
    }

    public ${entityNameHump}Page(BasePageQuery basePageQuery, int totalPage, List<${entityNameHump}BriefVO> ${entityNameHumpFirstLow}BriefVOList) {
        super(basePageQuery, totalPage, ${entityNameHumpFirstLow}BriefVOList);
    }

}