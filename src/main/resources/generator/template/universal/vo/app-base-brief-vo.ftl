package ${basePackage}.${entityNameWithOutUnderline}.vo;

import ${basePackage}.${entityNameWithOutUnderline}.model.*;
import com.seed.core.vo.BaseVO;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * ${tableComment}的BriefVO对象
 * @author ${author}
 * @date ${date}
 */
@Getter
@Setter
@ToString
public class ${entityNameHump}BriefVO extends BaseVO {

    public ${entityNameHump}BriefVO() {

    }

    public ${entityNameHump}BriefVO(${entityNameHump} ${entityNameHumpFirstLow}) {

    }

}