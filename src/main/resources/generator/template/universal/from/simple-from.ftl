package ${basePackage}.${entityNameWithOutUnderline}.from;

import com.seed.core.from.BaseFrom;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * ${tableComment}的From对象
 *
 * @author ${author}
 * @date ${date}
 */
@Getter
@Setter
@ToString
public class ${entityNameHump}From extends BaseFrom {


}