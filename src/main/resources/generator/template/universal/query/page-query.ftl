package ${basePackage}.${entityNameWithOutUnderline}.query;

import com.seed.core.query.BasePageQuery;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * ${tableComment}PageQuery
 * @author ${author}
 * @date ${date}
 */
@Getter
@Setter
@ToString
public class ${entityNameHump}PageQuery extends BasePageQuery {

}