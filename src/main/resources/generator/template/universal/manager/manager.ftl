package ${basePackage}.${entityNameWithOutUnderline}.manager;

import com.seed.core.manager.Manager;
import com.seed.core.exception.DAOException;
import ${basePackage}.${entityNameWithOutUnderline}.model.${entityNameHump};
import ${basePackage}.${entityNameWithOutUnderline}.query.*;

import java.util.List;

/**
 * @author ${author}
 * @date ${date}
 */
public interface ${entityNameHump}ManagerI extends Manager<${entityNameHump}>{

    /**
     * 根据Query对象获取List
     *
     * @param ${entityNameHumpFirstLow}Query 条件搜索对象
     * @return list对象
     * @throws DAOException 持久层异常
     */
    List<${entityNameHump}> list${entityNameHump}s(${entityNameHump}Query ${entityNameHumpFirstLow}Query) throws DAOException;

    /**
     * 根据PageQuery对象获取list对象
     *
     * @param ${entityNameHumpFirstLow}PageQuery 条件搜索对象
     * @return list对象
     * @throws DAOException 持久层异常
     */
    List<${entityNameHump}> list${entityNameHump}s(${entityNameHump}PageQuery ${entityNameHumpFirstLow}PageQuery) throws DAOException;


    /**
     * 根据PageQuery对象获取对象计数值
     *
     * @param ${entityNameHumpFirstLow}PageQuery 条件搜索对象
     * @return 计数值
     * @throws DAOException 持久层异常
     */
    Integer count${entityNameHump}s(${entityNameHump}PageQuery ${entityNameHumpFirstLow}PageQuery) throws DAOException;

}
