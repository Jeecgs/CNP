package ${basePackage}.${entityNameWithOutUnderline}.manager.impl;

import ${basePackage}.${entityNameWithOutUnderline}.dao.${entityNameHump}Mapper;
import ${basePackage}.${entityNameWithOutUnderline}.manager.${entityNameHump}ManagerI;
import ${basePackage}.${entityNameWithOutUnderline}.model.*;
import ${basePackage}.${entityNameWithOutUnderline}.query.*;
import com.seed.core.exception.DAOException;
import com.seed.core.manager.impl.AbstractManager;
import org.apache.ibatis.session.RowBounds;
import org.jeecgframework.core.util.MyBeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ${author}
 * @date ${date}
 */
@Service("${entityNameHumpFirstLow}Manager")
@Transactional(rollbackFor = DAOException.class)
public class ${entityNameHump}ManagerImpl extends AbstractManager<${entityNameHump}> implements ${entityNameHump}ManagerI {

    private final ${entityNameHump}Mapper ${entityNameHumpFirstLow}Mapper;

    @Autowired
    public ${entityNameHump}ManagerImpl(${entityNameHump}Mapper ${entityNameHumpFirstLow}Mapper) {
        this.${entityNameHumpFirstLow}Mapper = ${entityNameHumpFirstLow}Mapper;
    }

    /**
     * 查询对象转换为model对象
     *
     * @param query 查询对象
     * @return model对象
     * @throws DAOException 持久层异常
     */
    private ${entityNameHump} getModel(${entityNameHump}Query query) throws DAOException {
        ${entityNameHump} model = new ${entityNameHump}();
        try {
            MyBeanUtils.copyBeanNotNull2Bean(query, model);
        } catch (Exception e) {
            throw new DAOException(e, query);
        }
        return model;
    }

    /**
     * 查询对象转换为model对象
     *
     * @param query 查询对象
     * @return model对象
     * @throws DAOException 持久层异常
     */
    private ${entityNameHump} getModel(${entityNameHump}PageQuery query) throws DAOException {
        ${entityNameHump} model = new ${entityNameHump}();
        try {
            MyBeanUtils.copyBeanNotNull2Bean(query, model);
        } catch (Exception e) {
            throw new DAOException(e, query);
        }
        return model;
    }

    /**
     * 根据Query对象获取List
     *
     * @param ${entityNameHumpFirstLow}Query 条件搜索对象
     * @return list对象
     * @throws DAOException 持久层异常
     */
    @Override
    public List<${entityNameHump}> list${entityNameHump}s(${entityNameHump}Query ${entityNameHumpFirstLow}Query) throws DAOException {
        ${entityNameHump} search${entityNameHump} = this.getModel(${entityNameHumpFirstLow}Query);
        List<${entityNameHump}> ${entityNameHumpFirstLow}List;
        try {
            ${entityNameHumpFirstLow}List = this.listModels(search${entityNameHump});
        } catch (Exception e) {
            throw new DAOException(e, ${entityNameHumpFirstLow}Query);
        }
        return ${entityNameHumpFirstLow}List;
    }


    /**
     * 根据PageQuery对象获取list对象
     *
     * @param ${entityNameHumpFirstLow}PageQuery 条件搜索对象
     * @return list对象
     * @throws DAOException 持久层异常
     */
    @Override
    public List<${entityNameHump}> list${entityNameHump}s(${entityNameHump}PageQuery ${entityNameHumpFirstLow}PageQuery) throws DAOException {
        ${entityNameHump} search${entityNameHump} = this.getModel(${entityNameHumpFirstLow}PageQuery);
        List<${entityNameHump}> ${entityNameHumpFirstLow}List;
        try {
            RowBounds rowBounds = new RowBounds(${entityNameHumpFirstLow}PageQuery.getStart(),${entityNameHumpFirstLow}PageQuery.getSize());
            ${entityNameHumpFirstLow}List = this.listModels(search${entityNameHump},rowBounds);
        }catch (Exception e){
            throw new DAOException(e, ${entityNameHumpFirstLow}PageQuery);
        }
        return ${entityNameHumpFirstLow}List;
    }


    /**
    * 根据PageQuery对象获取对象计数值
    *
    * @param ${entityNameHumpFirstLow}PageQuery 条件搜索对象
    * @return 计数值
    * @throws DAOException 持久层异常
    */
    @Override
    public Integer count${entityNameHump}s(${entityNameHump}PageQuery ${entityNameHumpFirstLow}PageQuery) throws DAOException {
        ${entityNameHump} search${entityNameHump} = this.getModel(${entityNameHumpFirstLow}PageQuery);
        Integer total;
        try {
            total = this.countModels(search${entityNameHump});
        }catch (Exception e){
            throw new DAOException(e, ${entityNameHumpFirstLow}PageQuery);
        }
        return total;
    }
}