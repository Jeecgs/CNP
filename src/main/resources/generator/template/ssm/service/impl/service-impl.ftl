package ${basePackage}.${entityName}.service.impl;

import ${basePackage}.${entityName}.vo.*;
import ${basePackage}.${entityName}.dao.*;
import ${basePackage}.${entityName}.from.*;
import ${basePackage}.${entityName}.model.*;
import ${basePackage}.${entityName}.service.*;

import com.seed.core.service.impl.AbstractService;
import org.apache.ibatis.session.RowBounds;
import org.jeecgframework.core.util.MyBeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;


/**
 * @author ${author}
 * @date ${date}
 */
@Service("${entityNameHumpFirstLow}Service")
@Transactional(rollbackFor = Exception.class)
public class ${entityNameHump}ServiceImpl extends AbstractService<${entityNameHump}> implements ${entityNameHump}Service {

    @Resource
    private ${entityNameHump}Mapper ${entityName}Mapper;


    /**
    * 根据id获取VO对象
    * @param ${entityNameHumpFirstLow}Id 对象id
    * @return 单个VO对象
    */
    @Override
    public ${entityNameHump}VO get${entityNameHump}VOBy${entityNameHump}Id(String ${entityNameHumpFirstLow}Id){
        ${entityNameHump}VO ${entityNameHumpFirstLow}VO = new ${entityNameHump}VO();
        ${entityNameHump} ${entityNameHumpFirstLow} = ${entityName}Mapper.selectByPrimaryKey(${entityNameHumpFirstLow}Id);
        if (!StringUtils.isEmpty(${entityNameHumpFirstLow})){
            BeanUtils.copyProperties(${entityNameHumpFirstLow}, ${entityNameHumpFirstLow}VO);
        }
        return ${entityNameHumpFirstLow}VO;
    }

    /**
    * 根据id获取详情VO
    * @param ${entityNameHumpFirstLow}Id 对象id
    * @return 详情VO对象
    */
    @Override
    public ${entityNameHump}DetailVO get${entityNameHump}DetailVOById(String ${entityNameHumpFirstLow}Id){
        ${entityNameHump}DetailVO ${entityNameHumpFirstLow}DetailVO = new ${entityNameHump}DetailVO();
        ${entityNameHump} ${entityNameHumpFirstLow} = ${entityName}Mapper.selectByPrimaryKey(${entityNameHumpFirstLow}Id);
        if (!StringUtils.isEmpty(${entityNameHumpFirstLow})){
            BeanUtils.copyProperties(${entityNameHumpFirstLow}, ${entityNameHumpFirstLow}DetailVO);
        }
        return ${entityNameHumpFirstLow}DetailVO;
    }

    /**
    * 获取VOList
    * @return VO的list对象
    */
    @Override
    public List<${entityNameHump}VO> get${entityNameHump}VOList(){
        List<${entityNameHump}VO> ${entityNameHumpFirstLow}VOList = new ArrayList<>();
        List<${entityNameHump}> ${entityNameHumpFirstLow}List = ${entityName}Mapper.selectAll();
        for (${entityNameHump} ${entityNameHumpFirstLow}:${entityNameHumpFirstLow}List) {
            ${entityNameHump}VO ${entityNameHumpFirstLow}VO = new ${entityNameHump}VO();
            BeanUtils.copyProperties(${entityNameHumpFirstLow}, ${entityNameHumpFirstLow}VO);
            ${entityNameHumpFirstLow}VOList.add(${entityNameHumpFirstLow}VO);
        }
        return ${entityNameHumpFirstLow}VOList;
    }

    /**
    * 根据from对象获取VOList
    * @param ${entityNameHumpFirstLow}From 条件搜索对象
    * @return VO的list对象
    */
    @Override
    public List<${entityNameHump}VO> get${entityNameHump}VOListBy${entityNameHump}From(${entityNameHump}From ${entityNameHumpFirstLow}From){
        List<${entityNameHump}VO> ${entityNameHumpFirstLow}VOList = new ArrayList<>();
        ${entityNameHump} search${entityNameHumpFirstLow} = new ${entityNameHump}();
        try {
            MyBeanUtils.copyBeanNotNull2Bean( ${entityNameHumpFirstLow}From,search${entityNameHumpFirstLow});
        }catch (Exception e){
            e.printStackTrace();
            return ${entityNameHumpFirstLow}VOList;
        }
        List<${entityNameHump}> ${entityNameHumpFirstLow}List = ${entityName}Mapper.selectAll();
        for (${entityNameHump} ${entityNameHumpFirstLow}:${entityNameHumpFirstLow}List) {
            ${entityNameHump}VO ${entityNameHumpFirstLow}VO = new ${entityNameHump}VO();
            BeanUtils.copyProperties(${entityNameHumpFirstLow}, ${entityNameHumpFirstLow}VO);
            ${entityNameHumpFirstLow}VOList.add(${entityNameHumpFirstLow}VO);
        }
        return ${entityNameHumpFirstLow}VOList;
    }


    /**
    * 根据SearchFrom对象获取page对象
    * @param ${entityNameHumpFirstLow}SearchFrom 条件搜索对象
    * @return page对象
    */
    @Override
    public ${entityNameHump}Page get${entityNameHump}PageBy${entityNameHump}SearchFrom (${entityNameHump}SearchFrom ${entityNameHumpFirstLow}SearchFrom){
        ${entityNameHump}Page ${entityNameHumpFirstLow}Page = new ${entityNameHump}Page();
        ${entityNameHump} search${entityNameHump} = new ${entityNameHump}();
        try {
            MyBeanUtils.copyBeanNotNull2Bean(${entityNameHumpFirstLow}SearchFrom,search${entityNameHump});
        }catch (Exception e){
            e.printStackTrace();
            return ${entityNameHumpFirstLow}Page;
        }
        int start = (${entityNameHumpFirstLow}SearchFrom.getPage() - 1) * ${entityNameHumpFirstLow}SearchFrom.getSize();
        ${entityNameHumpFirstLow}SearchFrom.setStart(start);

        RowBounds rowBounds = new RowBounds(${entityNameHumpFirstLow}SearchFrom.getStart(),${entityNameHumpFirstLow}SearchFrom.getSize());
        List<${entityNameHump}> ${entityNameHumpFirstLow}List = ${entityName}Mapper.selectByRowBounds(search${entityNameHump},rowBounds);

        int total = ${entityName}Mapper.selectCount(search${entityNameHump});
        List<${entityNameHump}ListVO> ${entityNameHumpFirstLow}VOList = new ArrayList<>();
        for (${entityNameHump} simple:${entityNameHumpFirstLow}List) {
            ${entityNameHump}ListVO ${entityNameHumpFirstLow}ListVO = new ${entityNameHump}ListVO();
            BeanUtils.copyProperties(simple, ${entityNameHumpFirstLow}ListVO);
            ${entityNameHumpFirstLow}VOList.add(${entityNameHumpFirstLow}ListVO);
        }
        ${entityNameHumpFirstLow}Page = new ${entityNameHump}Page(${entityNameHumpFirstLow}SearchFrom, total, ${entityNameHumpFirstLow}VOList);
        return ${entityNameHumpFirstLow}Page;
    }


}
