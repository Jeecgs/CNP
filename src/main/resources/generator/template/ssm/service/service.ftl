package ${basePackage}.${entityName}.service;

import com.seed.core.service.Service;
import ${basePackage}.${entityName}.model.${entityNameHump};
import ${basePackage}.${entityName}.from.*;
import ${basePackage}.${entityName}.vo.*;
import java.util.List;

/**
 * @author ${author}
 * @date ${date}
*/
public interface ${entityNameHump}Service extends Service<${entityNameHump}>{
    /**
    * 根据id获取VO对象
    * @param ${entityNameHumpFirstLow}Id 对象id
    * @return 单个VO对象
    */
    ${entityNameHump}VO get${entityNameHump}VOBy${entityNameHump}Id(String ${entityNameHumpFirstLow}Id);

    /**
    * 根据id获取详情VO
    * @param ${entityNameHumpFirstLow}Id 对象id
    * @return 详情VO对象
    */
    ${entityNameHump}DetailVO get${entityNameHump}DetailVOById(String ${entityNameHumpFirstLow}Id);

    /**
    * 获取VOList
    * @return VO的list对象
    */
    List<${entityNameHump}VO> get${entityNameHump}VOList();

    /**
    * 根据from对象获取VOList
    * @param ${entityNameHumpFirstLow}From 条件搜索对象
    * @return VO的list对象
    */
    List<${entityNameHump}VO> get${entityNameHump}VOListBy${entityNameHump}From(${entityNameHump}From ${entityNameHumpFirstLow}From);

    /**
    * 根据SearchFrom对象获取page对象
    * @param ${entityNameHumpFirstLow}SearchFrom 条件搜索对象
    * @return page对象
    */
    ${entityNameHump}Page get${entityNameHump}PageBy${entityNameHump}SearchFrom(${entityNameHump}SearchFrom ${entityNameHumpFirstLow}SearchFrom);

}
