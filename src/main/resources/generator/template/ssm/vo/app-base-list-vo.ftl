package ${basePackage}.${entityName}.vo;

import lombok.Data;

/**
* ${tableComment}的listVO对象
* @author ${author}
* @date ${date}
*/
@Data
public class ${entityNameHump}ListVO {

}