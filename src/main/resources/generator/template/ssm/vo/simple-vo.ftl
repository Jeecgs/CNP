package ${basePackage}.${entityName}.vo;

import lombok.Data;

/**
* ${tableComment}的VO对象
* @author ${author}
* @date ${date}
*/
@Data
public class ${entityNameHump}VO {

}