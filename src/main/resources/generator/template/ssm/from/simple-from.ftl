package ${basePackage}.${entityName}.from;

import lombok.Data;

/**
* ${tableComment}的From对象
* @author ${author}
* @date ${date}
*/
@Data
public class ${entityNameHump}From {


}