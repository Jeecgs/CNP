package ${basePackage}.${entityName}.from;

import lombok.Data;

/**
* ${tableComment}的SearchFrom对象
* @author ${author}
* @date ${date}
*/
@Data
public class ${entityNameHump}SearchFrom {
    private int page = 1;
    private int size = 5;
    private int start = 0;


}