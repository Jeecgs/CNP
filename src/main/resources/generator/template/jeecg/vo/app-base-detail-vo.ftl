package ${basePackage}.${entityName}.VO;

import lombok.Data;

/**
* ${tableComment}的detailVO对象
* @author ${author}
* @date ${date}
*/
@Data
public class ${entityNameHump}DetailVO {

}