package ${basePackage}.${entityName}.VO;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import ${basePackage}.${entityName}.from.*;

/**
* ${tableComment}的page对象
* @author ${author}
* @date ${date}
*/
@Data
public class ${entityNameHump}Page {


    public ${entityNameHump}Page() {
    }

    public ${entityNameHump}Page(${entityNameHump}SearchFrom ${entityNameHumpFirstLow}SearchFrom, int totalPage, List<${entityNameHump}ListVO> ${entityNameHumpFirstLow}ListVOList) {
        this.page = ${entityNameHumpFirstLow}SearchFrom.getPage();
        this.size = ${entityNameHumpFirstLow}SearchFrom.getSize();
        this.totalPage = totalPage;
        this.${entityNameHumpFirstLow}ListVOList = ${entityNameHumpFirstLow}ListVOList;
    }

    private int page = 1;
    private int size = 5;
    private int totalPage = 0;

    private List<${entityNameHump}ListVO> ${entityNameHumpFirstLow}ListVOList = new ArrayList<>();

}