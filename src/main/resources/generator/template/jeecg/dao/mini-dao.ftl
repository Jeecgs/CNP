package ${basePackage}.${entityName}.dao;

import org.jeecgframework.minidao.annotation.Param;
import org.jeecgframework.minidao.annotation.ResultType;
import org.springframework.stereotype.Repository;
import ${basePackage}.${entityName}.from.*;
import ${basePackage}.${entityName}.vo.*;

import java.util.List;

/**
* ${tableComment}的MiniDao对象
* @author ${author}
* @date ${date}
*/
@Repository
public interface ${entityNameHump}MiniDao {

    /**
    * 根据SearchFrom对象获取总条数
    * @param ${entityNameHumpFirstLow}SearchFrom 条件搜索对象
    * @return 总条数
    */
    int get${entityNameHump}PageBy${entityNameHump}SearchFromTotal(@Param("${entityNameHumpFirstLow}SearchFrom") ${entityNameHump}SearchFrom  ${entityNameHumpFirstLow}SearchFrom);

    /**
    * 根据SearchFrom对象获取list对象
    * @param ${entityNameHumpFirstLow}SearchFrom 条件搜索对象
    * @return VOList对象
    */
    @ResultType(${entityNameHump}ListVO.class)
    List<${entityNameHump}ListVO> get${entityNameHump}PageBy${entityNameHump}SearchFrom(@Param("${entityNameHumpFirstLow}SearchFrom") ${entityNameHump}SearchFrom  ${entityNameHumpFirstLow}SearchFrom);

    /**
    * 根据SearchFrom对象获取list对象
    * @param ${entityNameHumpFirstLow}From 条件搜索对象
    * @return VOList对象
    */
    @ResultType(${entityNameHump}VO.class)
    List<${entityNameHump}VO> get${entityNameHump}VOListBy${entityNameHump}From(@Param("${entityNameHumpFirstLow}From") ${entityNameHump}From ${entityNameHumpFirstLow}From);
}
