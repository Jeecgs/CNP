package ${basePackage}.${entityName}.controller;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecgframework.core.common.controller.BaseController;
import ${basePackage}.${entityName}.service.${entityNameHump}Service;
import org.jeecgframework.core.common.model.json.AjaxJson;
import org.jeecgframework.p3.core.logger.Logger;
import org.jeecgframework.p3.core.logger.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import ${basePackage}.${entityName}.from.*;
import ${basePackage}.${entityName}.vo.*;
import ${businessBasePackage}.${modelNameAllLow}.service.*;

import java.util.HashMap;
import java.util.Map;
import java.util.List;

/**
* @author ${author}
* @date ${date}
*/
@RestController
@Api(value = "${entityNameHump}Controller", description = "${tableComment}管理", tags = "${entityNameHump}Controller")
@RequestMapping("/app/v1/${entityNameHump}Controller")
public class ${entityNameHump}Controller extends BaseController{
    private static final Logger logger = LoggerFactory.getLogger(${entityNameHump}Controller.class);

    private final ${entityNameHump}Service ${entityNameHumpFirstLow}Service;

    private final ${modelNameUpperCamel}ServiceI ${modelNameLowerCamel}Service;

    @Autowired
    public ${entityNameHump}Controller(${entityNameHump}Service ${entityNameHumpFirstLow}Service, ${modelNameUpperCamel}ServiceI ${modelNameLowerCamel}Service){
        this.${entityNameHumpFirstLow}Service = ${entityNameHumpFirstLow}Service;
        this.${modelNameLowerCamel}Service = ${modelNameLowerCamel}Service;
    }

    @RequestMapping(value = "get${entityNameHump}VOBy${entityNameHump}Id", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "根据id获取${tableComment}详细信息")
    public AjaxJson get${entityNameHump}VOBy${entityNameHump}Id(@RequestBody JSONObject object) {
        logger.info("[api-v1]${entityNameHump}Controller/get${entityNameHump}VOBy${entityNameHump}Id接口,根据id获取${tableComment}详细信息");

        AjaxJson result = new AjaxJson();

        String ${entityNameHumpFirstLow}Id = object.getString("${entityNameHumpFirstLow}Id");

        if (StringUtils.isEmpty(${entityNameHumpFirstLow}Id)) {
            result.setMsg("id不能为空!");
            result.setSuccess(false);
            return result;
        }

        ${entityNameHump}VO ${entityNameHumpFirstLow}VO = ${entityNameHumpFirstLow}Service.get${entityNameHump}VOBy${entityNameHump}Id(${entityNameHumpFirstLow}Id);

        Map<String, Object> map = new HashMap<>(16);
        map.put("${entityNameHumpFirstLow}VO", ${entityNameHumpFirstLow}VO);
        result.setObj(map);
        return result;
    }

    @RequestMapping(value = "get${entityNameHump}VOList", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "获取全部${tableComment}VOList信息")
    public AjaxJson get${entityNameHump}VOList() {
        logger.info("[api-v1]${entityNameHump}Controller/get${entityNameHump}VOBy${entityNameHump}Id接口,获取全部${tableComment}VOList信息");

        AjaxJson result = new AjaxJson();

        List<${entityNameHump}VO> ${entityNameHumpFirstLow}VOList = ${entityNameHumpFirstLow}Service.get${entityNameHump}VOList();

        Map<String, Object> map = new HashMap<>(16);
        map.put("${entityNameHumpFirstLow}VOList", ${entityNameHumpFirstLow}VOList);
        result.setObj(map);
        return result;
    }

    @RequestMapping(value = "get${entityNameHump}VOListBy${entityNameHump}From", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "根据from获取${tableComment}详细信息")
    public AjaxJson get${entityNameHump}VOListBy${entityNameHump}From(@RequestBody JSONObject object) {
        logger.info("[api-v1]${entityNameHump}Controller/get${entityNameHump}VOBy${entityNameHump}Id接口,根据from获取${tableComment}详细信息");

        AjaxJson result = new AjaxJson();

        ${entityNameHump}From ${entityNameHumpFirstLow}From = object.getObject("${entityNameHumpFirstLow}From",${entityNameHump}From.class);
        if (StringUtils.isEmpty(${entityNameHumpFirstLow}From)) {
            result.setMsg("请求参数不能为空!");
            result.setSuccess(false);
            return result;
        }

        List<${entityNameHump}VO> ${entityNameHumpFirstLow}VOList = ${entityNameHumpFirstLow}Service.get${entityNameHump}VOListBy${entityNameHump}From(${entityNameHumpFirstLow}From);

        Map<String, Object> map = new HashMap<>(16);
        map.put("${entityNameHumpFirstLow}VOList", ${entityNameHumpFirstLow}VOList);
        result.setObj(map);
        return result;
    }

    @RequestMapping(value = "get${entityNameHump}DetailVOBy${entityNameHump}Id", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "根据id获取${tableComment}详细信息")
    public AjaxJson get${entityNameHump}DetailVOBy${entityNameHump}Id(@RequestBody JSONObject object) {
        logger.info("[api-v1]${entityNameHump}Controller/get${entityNameHump}VOBy${entityNameHump}Id接口,根据id获取${tableComment}详细信息");

        AjaxJson result = new AjaxJson();

        String ${entityNameHumpFirstLow}Id = object.getString("${entityNameHumpFirstLow}Id");

        if (StringUtils.isEmpty(${entityNameHumpFirstLow}Id)) {
            result.setMsg("id不能为空!");
            result.setSuccess(false);
            return result;
        }

        ${entityNameHump}DetailVO ${entityNameHumpFirstLow}DetailVO = ${entityNameHumpFirstLow}Service.get${entityNameHump}DetailVOById(${entityNameHumpFirstLow}Id);

        Map<String, Object> map = new HashMap<>(16);
        map.put("${entityNameHumpFirstLow}DetailVO", ${entityNameHumpFirstLow}DetailVO);
        result.setObj(map);
        return result;
    }

    @RequestMapping(value = "get${entityNameHump}DetailVOBy${entityNameHump}SearchFrom", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "根据searchFrom获取${tableComment}详细信息")
    public AjaxJson get${entityNameHump}PageBy${entityNameHump}SearchFrom(@RequestBody JSONObject object) {
        logger.info("[api-v1]${entityNameHump}Controller/get${entityNameHump}VOBy${entityNameHump}Id接口,根据searchFrom获取${tableComment}详细信息");

        AjaxJson result = new AjaxJson();

        ${entityNameHump}SearchFrom ${entityNameHumpFirstLow}SearchFrom = object.getObject("${entityNameHumpFirstLow}SearchFrom",${entityNameHump}SearchFrom.class);
        if (StringUtils.isEmpty(${entityNameHumpFirstLow}SearchFrom)) {
            result.setMsg("请求参数不能为空!");
            result.setSuccess(false);
            return result;
        }

        ${entityNameHump}Page ${entityNameHumpFirstLow}Page = ${entityNameHumpFirstLow}Service.get${entityNameHump}PageBy${entityNameHump}SearchFrom(${entityNameHumpFirstLow}SearchFrom);

        Map<String, Object> map = new HashMap<>(16);
        map.put("${entityNameHumpFirstLow}Page", ${entityNameHumpFirstLow}Page);
        result.setObj(map);
        return result;
    }



}
