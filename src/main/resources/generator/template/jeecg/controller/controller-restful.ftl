package ${basePackage}.${entityName}.controller;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecgframework.core.common.controller.BaseController;
import ${basePackage}.${entityName}.service.${entityNameHump}Service;
import org.jeecgframework.core.common.model.json.AjaxJson;
import org.jeecgframework.p3.core.logger.Logger;
import org.jeecgframework.p3.core.logger.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import ${basePackage}.${entityName}.from.*;
import ${basePackage}.${entityName}.vo.*;
import ${businessBasePackage}.${modelNameAllLow}.service.*;

import java.util.HashMap;
import java.util.Map;
import java.util.List;

/**
* @author ${author}
* @date ${date}
*/
@RestController
@Api(value = "${entityNameHump}Controller", description = "${tableComment}管理", tags = "${entityNameHump}Controller")
@RequestMapping("/app/v1/${entityNameHump}Controller")
public class ${entityNameHump}Controller extends BaseController{
    private static final Logger logger = LoggerFactory.getLogger(${entityNameHump}Controller.class);

    private final ${entityNameHump}Service ${entityNameHumpFirstLow}Service;

    private final ${modelNameUpperCamel}ServiceI ${modelNameLowerCamel}Service;

    @Autowired
    public ${entityNameHump}Controller(${entityNameHump}Service ${entityNameHumpFirstLow}Service, ${modelNameUpperCamel}ServiceI ${modelNameLowerCamel}Service){
        this.${entityNameHumpFirstLow}Service = ${entityNameHumpFirstLow}Service;
        this.${modelNameLowerCamel}Service = ${modelNameLowerCamel}Service;
    }


    @PostMapping
    public Result add(@RequestBody ${modelNameUpperCamel} ${modelNameLowerCamel}) {
        ${modelNameLowerCamel}Service.save(${modelNameLowerCamel});
        return ResultGenerator.genSuccessResult();
    }

    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id) {
        ${modelNameLowerCamel}Service.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @PutMapping
    public Result update(@RequestBody ${modelNameUpperCamel} ${modelNameLowerCamel}) {
        ${modelNameLowerCamel}Service.update(${modelNameLowerCamel});
        return ResultGenerator.genSuccessResult();
    }

    @GetMapping("/{id}")
    public Result detail(@PathVariable Integer id) {
        ${modelNameUpperCamel} ${modelNameLowerCamel} = ${modelNameLowerCamel}Service.findById(id);
        return ResultGenerator.genSuccessResult(${modelNameLowerCamel});
    }

    @GetMapping
    public Result list(@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "0") Integer size) {
        PageHelper.startPage(page, size);
        List<${modelNameUpperCamel}> list = ${modelNameLowerCamel}Service.findAll();
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
