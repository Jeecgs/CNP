package ${basePackage}.${entityName}.service.impl;

import ${businessBasePackage}.${modelNameAllLow}.entity.*;
import ${businessBasePackage}.${modelNameAllLow}.service.*;
import org.jeecgframework.core.common.service.impl.CommonServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.jeecgframework.web.system.service.SystemService;
import ${basePackage}.${entityName}.service.${entityNameHump}Service;
import ${basePackage}.${entityName}.dao.${entityNameHump}MiniDao;
import ${basePackage}.${entityName}.from.*;
import ${basePackage}.${entityName}.vo.*;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ${author}
 * @date ${date}
 */
@Service("${entityNameHumpFirstLow}Service")
@Transactional(rollbackFor = Exception.class)
public class ${entityNameHump}ServiceImpl extends CommonServiceImpl implements ${entityNameHump}Service {

    @Autowired
    private ${modelNameUpperCamel}ServiceI ${modelNameLowerCamel}Service;

    @Autowired
    private ${entityNameHump}MiniDao ${entityNameHumpFirstLow}MiniDao;

    @Autowired
    private SystemService systemService;


    /**
    * 根据id获取VO对象
    * @param ${entityNameHumpFirstLow}Id 对象id
    * @return 单个VO对象
    */
    @Override
    public ${entityNameHump}VO get${entityNameHump}VOBy${entityNameHump}Id(String ${entityNameHumpFirstLow}Id){
        ${entityNameHump}VO ${entityNameHumpFirstLow}VO = new ${entityNameHump}VO();
        ${modelNameUpperCamel}Entity ${modelNameLowerCamel}Entity = ${modelNameLowerCamel}Service.get(${modelNameUpperCamel}Entity.class,${entityNameHumpFirstLow}Id);
        if (!StringUtils.isEmpty(${modelNameLowerCamel}Entity)){
            BeanUtils.copyProperties(${modelNameLowerCamel}Entity, ${entityNameHumpFirstLow}VO);
        }
        return ${entityNameHumpFirstLow}VO;
    }

    /**
    * 获取VOList
    * @return VO的list对象
    */
    @Override
    public List<${entityNameHump}VO> get${entityNameHump}VOList(){
        List<${entityNameHump}VO> ${entityNameHumpFirstLow}VOList = new ArrayList<>();
        List<${modelNameUpperCamel}Entity> ${modelNameLowerCamel}EntityList = ${modelNameLowerCamel}Service.getList(${modelNameUpperCamel}Entity.class);
        for (${modelNameUpperCamel}Entity ${modelNameLowerCamel}Entity:${modelNameLowerCamel}EntityList) {
            ${entityNameHump}VO ${entityNameHumpFirstLow}VO = new ${entityNameHump}VO();
            BeanUtils.copyProperties(${modelNameLowerCamel}Entity, ${entityNameHumpFirstLow}VO);
            ${entityNameHumpFirstLow}VOList.add(${entityNameHumpFirstLow}VO);
        }
        return ${entityNameHumpFirstLow}VOList;
    }

    /**
    * 根据from对象获取VOList
    * @param ${entityNameHumpFirstLow}From 条件搜索对象
    * @return VO的list对象
    */
    @Override
    public List<${entityNameHump}VO> get${entityNameHump}VOListBy${entityNameHump}From(${entityNameHump}From ${entityNameHumpFirstLow}From){
        return ${entityNameHumpFirstLow}MiniDao.get${entityNameHump}VOListBy${entityNameHump}From(${entityNameHumpFirstLow}From);
    }

    /**
    * 根据id获取详情VO
    * @param ${entityNameHumpFirstLow}Id 对象id
    * @return 详情VO对象
    */
    @Override
    public ${entityNameHump}DetailVO get${entityNameHump}DetailVOById(String ${entityNameHumpFirstLow}Id){
        ${entityNameHump}DetailVO ${entityNameHumpFirstLow}DetailVO = new ${entityNameHump}DetailVO();
        ${modelNameUpperCamel}Entity ${modelNameLowerCamel}Entity = ${modelNameLowerCamel}Service.get(${modelNameUpperCamel}Entity.class,${entityNameHumpFirstLow}Id);
        if (!StringUtils.isEmpty(${modelNameLowerCamel}Entity)){
            BeanUtils.copyProperties(${modelNameLowerCamel}Entity, ${entityNameHumpFirstLow}DetailVO);
        }
        return ${entityNameHumpFirstLow}DetailVO;
    }

    /**
    * 根据SearchFrom对象获取page对象
    * @param ${entityNameHumpFirstLow}SearchFrom 条件搜索对象
    * @return page对象
    */
    @Override
    public ${entityNameHump}Page get${entityNameHump}PageBy${entityNameHump}SearchFrom (${entityNameHump}SearchFrom ${entityNameHumpFirstLow}SearchFrom){

        int start = (${entityNameHumpFirstLow}SearchFrom.getPage() - 1) * ${entityNameHumpFirstLow}SearchFrom.getSize();
        ${entityNameHumpFirstLow}SearchFrom.setStart(start);

        List<${entityNameHump}ListVO> ${entityNameHumpFirstLow}VOList = ${entityNameHumpFirstLow}MiniDao.get${entityNameHump}PageBy${entityNameHump}SearchFrom(${entityNameHumpFirstLow}SearchFrom);
        int total = ${entityNameHumpFirstLow}MiniDao.get${entityNameHump}PageBy${entityNameHump}SearchFromTotal(${entityNameHumpFirstLow}SearchFrom);
        ${entityNameHump}Page ${entityNameHumpFirstLow}Page = new ${entityNameHump}Page(${entityNameHumpFirstLow}SearchFrom, total, ${entityNameHumpFirstLow}VOList);
        return ${entityNameHumpFirstLow}Page;
    }


}
