package com.business.app.util;


import lombok.Data;
import org.jeecgframework.core.common.model.json.AjaxJson;

import java.util.List;

@Data
public class Result {
    private boolean isSuccess;
    private String msg;

    public Result() {
    }

    public Result(Boolean isSuccess, String msg) {
        this.isSuccess = isSuccess;
        this.msg = msg;
    }

    public Result set(Boolean isSuccess, String msg) {
        this.isSuccess = isSuccess;
        this.msg = msg;
        return this;
    }

    public AjaxJson set(AjaxJson ajaxJson){
        ajaxJson.setSuccess(this.isSuccess);
        ajaxJson.setMsg(this.msg);
        return ajaxJson;
    }

    public static AjaxJson set(List<Result> results, AjaxJson ajaxJson){
        Result result = new Result();
        result.isSuccess = results.contains(true);
        if(!result.isSuccess){
            result.msg = "操作失败！";
            return result.set(ajaxJson);
        }
        result.msg = "操作成功！<p>信息如下：<p>";
        for(Result r : results){
            result.msg += r.msg + "<p>";
        }
        results.remove(false);
        result.msg += "共改变" + results.size() + "个！";
        return result.set(ajaxJson);
    }



    public Result success(String msg) {
        this.msg = msg;
        this.isSuccess = true;
        return this;
    }


    public Result flied(String msg) {
        this.msg = msg;
        this.isSuccess = false;
        return this;
    }
}
