package com.business.app.util;

import org.springframework.util.StringUtils;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 序列号生成器
 *
 * @author keriezhang
 * @date 2016/10/31
 */
public class SerialNumberGenerator {

    private static final String SERIAL_NUMBER = "XXXX";
    private static SerialNumberGenerator serialNumberGenerator = null;

    private static String serialNumber = null;

    private SerialNumberGenerator() {
    }

    public static String getSerialNumber() {
        return serialNumber;
    }

    public static void setSerialNumber(String serialNumber) {
        SerialNumberGenerator.serialNumber = serialNumber;
    }

    /**
     * 取得SerialNumberGenerator的单例实现
     *
     * @return
     */
    public static SerialNumberGenerator getInstance() {
        if (serialNumberGenerator == null) {
            synchronized (SerialNumberGenerator.class) {
                if (serialNumberGenerator == null) {
                    serialNumberGenerator = new SerialNumberGenerator();
                }
            }
        }
        return serialNumberGenerator;
    }

    /**
     * 生成下一个编号
     */
    public synchronized String nextSerialNumber(String code, String sno) {
        String id = null;
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        if (StringUtils.isEmpty(sno)) {
            id = formatter.format(date) + "0001";
        } else {
            int count = SERIAL_NUMBER.length();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < count; i++) {
                sb.append("0");
            }
            DecimalFormat df = new DecimalFormat("0000");
            id = formatter.format(date)
                    + df.format(1 + Integer.parseInt(sno.substring(8, 12)));
        }
        SerialNumberGenerator.setSerialNumber(id);
        return code + id;
    }

    public static void main(String[] args) {
        System.out.println("++++++++++++++" + SerialNumberGenerator.getInstance().nextSerialNumber("NO", SerialNumberGenerator.getSerialNumber()));
    }
}