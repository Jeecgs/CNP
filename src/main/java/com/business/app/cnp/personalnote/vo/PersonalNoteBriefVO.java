package com.business.app.cnp.personalnote.vo;

import com.business.app.cnp.personalnote.model.PersonalNote;
import com.seed.core.vo.BaseVO;
import lombok.Data;

/**
 * 的BriefVO对象
 * @author CodeGenerator
 * @date 2020年04月03日
 */
@Data
public class PersonalNoteBriefVO extends BaseVO {

    public PersonalNoteBriefVO() {

    }

    public PersonalNoteBriefVO(PersonalNote personalNote) {

    }

}