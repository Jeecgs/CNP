package com.business.app.cnp.personalnote.from;

import com.seed.core.from.BaseFrom;
import lombok.Data;

/**
 * 的From对象
 *
 * @author CodeGenerator
 * @date 2020年04月03日
 */
@Data
public class PersonalNoteFrom extends BaseFrom {
    private String id;

    /**
     * 笔记创建人
     */
    private String userId;

    /**
     * 笔记父id
     */
    private String noteParentId;

    /**
     * 笔记名称
     */
    private String noteName;

    /**
     * 是否顶置
     */
    private String isTop;

    /**
     * 笔记内容
     */
    private String noteContent;

    /**
     * 流水code
     */
    private String noteSerialCode;
}