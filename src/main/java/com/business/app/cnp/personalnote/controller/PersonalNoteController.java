package com.business.app.cnp.personalnote.controller;

import com.alibaba.fastjson.JSONObject;
import com.business.app.cnp.personalnote.enums.PersonalNoteEnum;
import com.business.app.cnp.personalnote.from.DelPersonNoteFrom;
import com.business.app.cnp.personalnote.from.PersonalNoteFrom;
import com.business.app.cnp.personalnote.model.PersonalNote;
import com.business.app.cnp.personalnote.vo.PersonalNoteDetailVO;
import com.business.app.cnp.personalnote.service.PersonalNoteServiceI;
import com.business.app.util.Result;
import com.seed.core.controller.AppBaseController;
import com.seed.core.exception.ServiceException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecgframework.core.common.model.json.AjaxJson;
import org.jeecgframework.p3.core.logger.Logger;
import org.jeecgframework.p3.core.logger.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @author CodeGenerator
 * @date 2020年04月03日
 */
@RestController
@Api(value = "PersonalNoteController", description = "管理", tags = "PersonalNoteController")
@RequestMapping("/app/v1/PersonalNoteController")
public class PersonalNoteController extends AppBaseController {

    private static final Logger logger = LoggerFactory.getLogger(PersonalNoteController.class);

    private final PersonalNoteServiceI personalNoteService;

    @Autowired
    public PersonalNoteController(PersonalNoteServiceI personalNoteService) {
        this.personalNoteService = personalNoteService;
    }

    /**
     * @ description:
     * @ author: longxin
     * @ date: 2020/4/8 0008 17:27
     * * @param: null
     * @ return: a
     */
    @RequestMapping(value = "/creatOrUpdatePersonNote", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "新增/更新笔记")
    public AjaxJson creatOrUpdatePersonNote(JSONObject object) {
        logger.info("[api-v1]PersonalnoteController.creatOrUpdatePersonNote接口,新增/更新笔记");

        AjaxJson result = new AjaxJson();

        PersonalNoteFrom personalnoteFrom = object.toJavaObject(PersonalNoteFrom.class);

        if (StringUtils.isEmpty(personalnoteFrom)) {
            result.filed("信息不能为空!");
            return result;
        }
        Result r;
        try {
            r = personalNoteService.creatOrUpdatePersonNote(personalnoteFrom);
            if(r.isSuccess()){
                return result.success("操作成功！");
            }
            return result.filed("操作失败. : " + r.getMsg());
        } catch (ServiceException e) {
            return result.filed("操作异常.");
        }
    }

    /**
     * @ description:
     * @ author: longxin
     * @ date: 2020/4/8 0008 17:27
     * * @param: null
     * @ return: a
     */

    @RequestMapping(value = "/deletePersonNote", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "根据id永久删除笔记")
    public AjaxJson deletePersonNote(JSONObject object) {
        logger.info("[api-v1]PersonalNoteController.getPersonalNoteVO接口,根据id永久删除笔记");

        AjaxJson result = new AjaxJson();

        DelPersonNoteFrom delPersonNoteFrom = object.toJavaObject(DelPersonNoteFrom.class);

        if (StringUtils.isEmpty(delPersonNoteFrom)) {
            result.filed("id不能为空!");
            return result;
        }

        Result r =  personalNoteService.deletePersonNote(delPersonNoteFrom);
        return r.set(result);
    }

    /**
     * @ description:
     * @ author: longxin
     * @ date: 2020/4/8 0008 17:27
     * * @param: null
     * @ return: a
     */
    @RequestMapping(value = "/recoveryPersonNote", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "恢复笔记")
    public AjaxJson recoveryPersonNote(JSONObject object) {
        logger.info("[api-v1]PersonalNoteController.recoveryPersonNote接口,恢复笔记");

        AjaxJson result = new AjaxJson();

        DelPersonNoteFrom delPersonNoteFrom = object.toJavaObject(DelPersonNoteFrom.class);

        if (StringUtils.isEmpty(delPersonNoteFrom)) {
            return result.filed("参数不能为空.");
        }

        personalNoteService.recoveryPersonNote(delPersonNoteFrom);
        result.success("操作成功！");
        return result;
    }

    /**
     * @ description:
     * @ author: longxin
     * @ date: 2020/4/8 0008 17:27
     * * @param: null
     * @ return: a
     */
    @RequestMapping(value = "/delRecoveryPersonNote", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "根据id删除笔记(回收)")
    public AjaxJson delRecoveryPersonNote(JSONObject object) {
        logger.info("[api-v1]PersonalNoteController.delRecoveryPersonNote接口,根据id删除笔记(回收)");

        AjaxJson result = new AjaxJson();

        DelPersonNoteFrom delPersonNoteFrom = object.toJavaObject(DelPersonNoteFrom.class);

        if (StringUtils.isEmpty(delPersonNoteFrom)) {
            return result.filed("id不能为空!");
        }

        Boolean isSuccess  = personalNoteService.delRecoveryPersonNote(delPersonNoteFrom);
        if(isSuccess){
            return result.filed("操作失败.");
        }
        return result.success("操作成功！");
    }

    /**
     * @description: 根据id获取个人笔记信息
     * @param: id
     * @return: AjaxJson
     * @author: liupeng
     * @date: 2020/4/8 10:23
     */
    @RequestMapping(value = "/getPersonalNoteVO", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "根据id获取个人笔记信息")
    public AjaxJson getPersonalNoteVO(JSONObject object) {
        logger.info("[api-v1]PersonalNoteController.getPersonalNoteVO接口,根据id获取个人笔记信息");
        AjaxJson result = new AjaxJson();

        String id=object.getObject("id", String.class);

        if (StringUtils.isEmpty(id)) {
            result.setMsg("id不能为空!");
            result.setSuccess(false);
            return result;
        }

        try {
            PersonalNoteDetailVO  personalNoteDetailVO  = personalNoteService.getPersonalNoteDetailVO(id);
            Map<String, Object> map = new HashMap<>(16);
            map.put("personalNoteDetailVO", personalNoteDetailVO);
            return result.success(map);
        } catch (ServiceException e) {
            e.printStackTrace();
            return result.filed("查询失败");
        }
    }



    /**
     * @description: 根据id获取个人笔记信息
     * @param: id
     * @return: AjaxJson
     * @author: liupeng
     * @date: 2020/4/8 10:50
     */
    @RequestMapping(value = "/pullPersonalNoteOverheadPosition", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "根据id将个人笔记顶置")
    public AjaxJson pullPersonalNoteOverheadPosition (JSONObject object) {
        logger.info("[api-v1]PersonalNoteController.getPersonalNoteVO接口,根据id将个人笔记顶置");

        AjaxJson result = new AjaxJson();
        String id=object.getObject("id", String.class);

        if (StringUtils.isEmpty(id)) {
            result.setMsg("id不能为空!");
            result.setSuccess(false);
            return result;
        }

        try {
            PersonalNote personalNote = personalNoteService.getModel(id);
            personalNote.setIsTop(PersonalNoteEnum.OverheadPositionEnum.OVERHEAD_POSITION.getCode());
            personalNoteService.save(personalNote);
            return result.success("顶置成功");
        } catch (ServiceException e) {
            e.printStackTrace();
            return result.filed("顶置失败");
        }
    }




}
