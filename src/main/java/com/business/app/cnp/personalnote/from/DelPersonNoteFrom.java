package com.business.app.cnp.personalnote.from;

import lombok.Data;

@Data
public class DelPersonNoteFrom {
    /**
     * ID集合
     */
    private String ids;
    /**
     * 父ID
     */
    private String parentId;
}
