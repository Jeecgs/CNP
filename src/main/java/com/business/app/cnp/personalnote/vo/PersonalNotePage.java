package com.business.app.cnp.personalnote.vo;

import com.seed.core.vo.BasePage;
import com.seed.core.query.BasePageQuery;
import lombok.Data;

import java.util.List;

/**
 * 的page对象
 * @author CodeGenerator
 * @date 2020年04月03日
 */
@Data
public class PersonalNotePage extends BasePage<PersonalNoteBriefVO> {

    public PersonalNotePage() {
        super();
    }

    public PersonalNotePage(BasePageQuery basePageQuery, int totalPage, List<PersonalNoteBriefVO> personalNoteBriefVOList) {
        super(basePageQuery, totalPage, personalNoteBriefVOList);
    }

}