package com.business.app.cnp.personalnote.enums;

/**
 * @author peng
 * @title: PersonalNoteEnum
 * @projectName cnp
 * @description:
 * @date 2020/04/08 19:22
 */
public class PersonalNoteEnum {

    public enum  OverheadPositionEnum{
        OVERHEAD_POSITION("1", "顶置"),
        UN_OVERHEAD_POSITION("0", "不定值");


        OverheadPositionEnum(String code, String value) {
            this.code = code;
            this.value = value;
        }

        private String code;
        private String value;

        public String getCode() {
            return code;
        }

        public String getValue() {
            return value;
        }

        public static OverheadPositionEnum codeOf(String code){
            for (OverheadPositionEnum roleEnum : values()) {
                if (roleEnum.getCode().equals(code)){
                    return roleEnum;
                }
            }
            throw new RuntimeException("没有找到对应的枚举");
        }
    }

}
