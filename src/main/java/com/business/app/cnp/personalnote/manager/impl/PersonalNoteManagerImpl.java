package com.business.app.cnp.personalnote.manager.impl;

import com.business.app.cnp.personalnote.dao.PersonalNoteMapper;
import com.business.app.cnp.personalnote.manager.PersonalNoteManagerI;
import com.business.app.cnp.personalnote.model.PersonalNote;
import com.business.app.cnp.personalnote.query.PersonalNotePageQuery;
import com.business.app.cnp.personalnote.query.PersonalNoteQuery;
import com.seed.core.exception.DAOException;
import com.seed.core.manager.impl.AbstractManager;
import org.apache.ibatis.session.RowBounds;
import org.jeecgframework.core.util.MyBeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author CodeGenerator
 * @date 2020年04月03日
 */
@Service("personalNoteManager")
@Transactional(rollbackFor = DAOException.class)
public class PersonalNoteManagerImpl extends AbstractManager<PersonalNote> implements PersonalNoteManagerI {

    private final PersonalNoteMapper personal_noteMapper;

    @Autowired
    public PersonalNoteManagerImpl(PersonalNoteMapper personal_noteMapper) {
        this.personal_noteMapper = personal_noteMapper;
    }


    /**
     * 根据Query对象获取List
     *
     * @param personalNoteQuery 条件搜索对象
     * @return list对象
     * @throws DAOException 持久层异常
     */
    @Override
    public List<PersonalNote> listPersonalNotes(PersonalNoteQuery personalNoteQuery) throws DAOException {
        List<PersonalNote> personalNoteList = new ArrayList<>();
        PersonalNote searchPersonalNote = new PersonalNote();
        try {
            MyBeanUtils.copyBeanNotNull2Bean(personalNoteQuery, searchPersonalNote);
            personalNoteList = this.listModels(searchPersonalNote);
        } catch (Exception e) {
            throw new DAOException(e, personalNoteQuery);
        }
        return personalNoteList;
    }


    /**
     * 根据PageQuery对象获取list对象
     *
     * @param personalNotePageQuery 条件搜索对象
     * @return list对象
     * @throws DAOException 持久层异常
     */
    @Override
    public List<PersonalNote> listPersonalNotes(PersonalNotePageQuery personalNotePageQuery) throws DAOException {
        List<PersonalNote> personalNoteList = new ArrayList<>();
        PersonalNote searchPersonalNote = new PersonalNote();
        try {
            MyBeanUtils.copyBeanNotNull2Bean(personalNotePageQuery, searchPersonalNote);
            RowBounds rowBounds = new RowBounds(personalNotePageQuery.getStart(),personalNotePageQuery.getSize());
            personalNoteList = this.listModels(searchPersonalNote,rowBounds);
        }catch (Exception e){
            throw new DAOException(e, personalNotePageQuery);
        }
        return personalNoteList;
    }


    /**
    * 根据PageQuery对象获取对象计数值
    *
    * @param personalNotePageQuery 条件搜索对象
    * @return 计数值
    * @throws DAOException 持久层异常
    */
    @Override
    public int countPersonalNotes(PersonalNotePageQuery personalNotePageQuery) throws DAOException {
        int total = 0;
        PersonalNote searchPersonalNote = new PersonalNote();
        try {
            MyBeanUtils.copyBeanNotNull2Bean(personalNotePageQuery, searchPersonalNote);
            total = this.countModels(searchPersonalNote);
        }catch (Exception e){
            throw new DAOException(e, personalNotePageQuery);
        }
        return total;
    }
}