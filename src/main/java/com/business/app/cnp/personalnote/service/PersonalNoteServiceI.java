package com.business.app.cnp.personalnote.service;


import com.business.app.cnp.personalnote.from.DelPersonNoteFrom;
import com.business.app.cnp.personalnote.from.PersonalNoteFrom;
import com.business.app.cnp.personalnote.vo.PersonalNoteDetailVO;
import com.business.app.cnp.personalnote.vo.PersonalNotePage;
import com.business.app.cnp.personalnote.model.PersonalNote;
import com.business.app.cnp.personalnote.query.PersonalNotePageQuery;
import com.business.app.cnp.personalnote.query.PersonalNoteQuery;
import com.business.app.util.Result;
import com.seed.core.exception.ServiceException;
import com.seed.core.service.Service;

import java.util.List;

/**
 * @author CodeGenerator
 * @date 2020年04月03日
 */
public interface PersonalNoteServiceI extends Service<PersonalNote> {
    /**
     * 恢复笔记
     * @param delPersonNoteFrom
     * @return
     */
    Boolean recoveryPersonNote(DelPersonNoteFrom delPersonNoteFrom);
    /**
     * 回收笔记
     * @param delPersonNoteFrom
     * @return
     */
    Boolean delRecoveryPersonNote(DelPersonNoteFrom delPersonNoteFrom);
    /**
     * 删除笔记
     * @param
     * @return
     */
    Result deletePersonNote(DelPersonNoteFrom delPersonNoteFrom);
    /**
     * 创建/更新笔记
     * @param personalnoteFrom
     * @return
     */
    Result creatOrUpdatePersonNote(PersonalNoteFrom personalnoteFrom) throws ServiceException;

    /**
     * 根据id获取详情VO
     *
     * @param personalNoteId 对象id
     * @return 详情VO对象
     * @throws ServiceException 业务层异常
     */
    PersonalNoteDetailVO getPersonalNoteDetailVO(String personalNoteId) throws ServiceException;

    /**
     * 获取DetailVOList
     *
     * @return DetailVO的list对象
     * @throws ServiceException 业务层异常
     */
    List<PersonalNoteDetailVO> listPersonalNoteDetailVOs() throws ServiceException;

    /**
     * 根据Query对象获取DetailVOList
     *
     * @param personalNoteQuery 条件搜索对象
     * @return DetailVO的list对象
     * @throws ServiceException 业务层异常
     */
    List<PersonalNoteDetailVO> listPersonalNoteDetailVOs(PersonalNoteQuery personalNoteQuery) throws ServiceException;

    /**
     * 根据PageQuery对象获取page对象
     *
     * @param personalNotePageQuery 条件搜索对象
     * @return page对象
     * @throws ServiceException 业务层异常
     */
    PersonalNotePage listPersonalNotePage(PersonalNotePageQuery personalNotePageQuery) throws ServiceException;
}
