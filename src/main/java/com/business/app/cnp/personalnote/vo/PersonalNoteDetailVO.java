package com.business.app.cnp.personalnote.vo;

import com.seed.core.vo.BaseVO;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

/**
 * 的detailVO对象
 * @author CodeGenerator
 * @date 2020年04月03日
 */
@Data
public class PersonalNoteDetailVO extends BaseVO {

    public PersonalNoteDetailVO() {

    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    /**
     * 笔记流水号
     */
    @Column(name = "note_serial_number")
    private String noteSerialNumber;

    /**
     * 笔记创建人
     */
    @Column(name = "create_name")
    private String createName;

    /**
     * 笔记父本id
     */
    @Column(name = "note_parent_id")
    private String noteParentId;

    /**
     * 笔记名称
     */
    @Column(name = "note_name")
    private String noteName;

    /**
     * 是否顶置
     */
    @Column(name = "is_top")
    private String isTop;

    /**
     * 回收站
     */
    @Column(name = "is_recycle_bin")
    private String isRecycleBin;

    /**
     * 创建日期
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新日期
     */
    private Date updateTime;

    /**
     * 笔记内容
     */
    private String noteContent;


}