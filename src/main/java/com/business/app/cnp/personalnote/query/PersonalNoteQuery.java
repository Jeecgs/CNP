package com.business.app.cnp.personalnote.query;

import com.seed.core.query.BaseQuery;
import lombok.Data;

/**
 * Query
 * @author CodeGenerator
 * @date 2020年04月03日
 */
@Data
public class PersonalNoteQuery extends BaseQuery {

}