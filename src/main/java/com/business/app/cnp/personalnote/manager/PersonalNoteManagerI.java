package com.business.app.cnp.personalnote.manager;

import com.business.app.cnp.personalnote.model.PersonalNote;
import com.business.app.cnp.personalnote.query.PersonalNotePageQuery;
import com.business.app.cnp.personalnote.query.PersonalNoteQuery;
import com.seed.core.exception.DAOException;
import com.seed.core.manager.Manager;

import java.util.List;

/**
 * @author CodeGenerator
 * @date 2020年04月03日
 */
public interface PersonalNoteManagerI extends Manager<PersonalNote> {

    /**
     * 根据Query对象获取List
     * @param personalNoteQuery 条件搜索对象
     * @return list对象
     * @throws DAOException 持久层异常
     */
    List<PersonalNote> listPersonalNotes(PersonalNoteQuery personalNoteQuery) throws DAOException;

    /**
     * 根据PageQuery对象获取list对象
     *
     * @param personalNotePageQuery 条件搜索对象
     * @return list对象
     * @throws DAOException 持久层异常
     */
    List<PersonalNote> listPersonalNotes(PersonalNotePageQuery personalNotePageQuery) throws DAOException;


    /**
     * 根据PageQuery对象获取对象计数值
     *
     * @param personalNotePageQuery 条件搜索对象
     * @return 计数值
     * @throws DAOException 持久层异常
     */
    int countPersonalNotes(PersonalNotePageQuery personalNotePageQuery) throws DAOException;

}
