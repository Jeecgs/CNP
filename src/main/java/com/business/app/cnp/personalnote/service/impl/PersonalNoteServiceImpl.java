package com.business.app.cnp.personalnote.service.impl;

import com.business.app.Const.NoteConsts;
import com.business.app.cnp.note.dao.NoteMapper;
import com.business.app.cnp.note.model.Note;
import com.business.app.cnp.personalnote.dao.PersonalNoteMapper;
import com.business.app.cnp.personalnote.from.DelPersonNoteFrom;
import com.business.app.cnp.personalnote.from.PersonalNoteFrom;
import com.business.app.cnp.personalnote.vo.PersonalNoteBriefVO;
import com.business.app.cnp.personalnote.vo.PersonalNoteDetailVO;
import com.business.app.cnp.personalnote.vo.PersonalNotePage;
import com.business.app.cnp.personalnote.manager.PersonalNoteManagerI;
import com.business.app.cnp.personalnote.model.PersonalNote;
import com.business.app.cnp.personalnote.query.PersonalNotePageQuery;
import com.business.app.cnp.personalnote.query.PersonalNoteQuery;
import com.business.app.cnp.personalnote.service.PersonalNoteServiceI;
import com.business.app.util.Result;
import com.business.app.util.UuidUtil;
import com.seed.core.exception.DAOException;
import com.seed.core.exception.ServiceException;
import com.seed.core.service.impl.AbstractService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author CodeGenerator
 * @date 2020年04月03日
 */
@Service("personalNoteService")
@Transactional(rollbackFor = ServiceException.class)
public class PersonalNoteServiceImpl extends AbstractService<PersonalNote> implements PersonalNoteServiceI {

    private final PersonalNoteMapper personalNoteMapper;

    private final NoteMapper noteMapper;

    private final PersonalNoteManagerI personalNoteManagerI;

    @Autowired
    public PersonalNoteServiceImpl(PersonalNoteManagerI personalNoteManagerI,
                                   PersonalNoteMapper personalNoteMapper, NoteMapper noteMapper) {
        this.personalNoteManagerI = personalNoteManagerI;
        this.personalNoteMapper = personalNoteMapper;
        this.noteMapper = noteMapper;
    }

    @Override
    public Boolean recoveryPersonNote(DelPersonNoteFrom delPersonNoteFrom) {

        //筛选掉没在回收站的id
        List<String> ids = personalNoteMapper.getNoDelIds(delPersonNoteFrom.getIds());

        //成功回收数量
        int count = 0;
        for(String id : ids){
            count += personalNoteMapper.recoveryPersonNote(id, delPersonNoteFrom.getParentId());
        }
        if(count == 0){
            return false;
        }
        return true;
    }

    @Override
    public Boolean delRecoveryPersonNote(DelPersonNoteFrom delPersonNoteFrom) {
        Boolean isSuccess;
        try{
            String[] sourceArray = delPersonNoteFrom.getIds().split(",");
            for(String deleId : sourceArray){
                PersonalNote p = new PersonalNote();
                p.setId(deleId);
                p = personalNoteMapper.select(p).get(0);
                p.setIsRecycleBin(NoteConsts.NoteEnum.NOTE_RECYCLE.getKey());
                p.setNoteParentId(NoteConsts.DEFAULT_NOTE_RECYCLE_ID);
                personalNoteManagerI.save(p);
            }
            isSuccess = true;
        } catch (DAOException e) {
            isSuccess = false;
        }
        return isSuccess;
    }

    @Override
    public Result deletePersonNote(DelPersonNoteFrom delPersonNoteFrom) {
        Result result = new Result();
        try {
            //有几个ID
            int count = delPersonNoteFrom.getIds().split(",").length;
            if(count < 1){
                return result.flied("参数异常.");
            }
            //筛选可删除列表
            List<String> ids = personalNoteMapper.getNoDelIds(delPersonNoteFrom.getIds());
            //可删除数量
            int delCount = ids.size();
            if(delCount < 1){
                return result.flied("非法删除.");
            }
            //成功删除数量
            int sucCount = personalNoteMapper.deleteByIds(String.join(",", ids));
            if(sucCount < 1){
                return result.flied("删除失败.");
            }
            return result.success("删除成功，您操作删除" + count + "条数据，删除成功" + sucCount + "条");
        }catch (Exception e){
            return result.flied("删除异常.");
        }
    }

    @Override
    public Result creatOrUpdatePersonNote(PersonalNoteFrom pnf) throws ServiceException {
        Result result = new Result();
        try {
            //TODO 获取创建者名

            Note t = new Note();
            t.setParentId(pnf.getNoteParentId());

            //数据库能否找找到该父级
            boolean parent = noteMapper.select(t).size() < 1 ? true : false;

            PersonalNote p = new PersonalNote();
            p.setId(pnf.getId());

            if(p.getId().isEmpty()){  //新增
                if(!parent){
                    return result.flied("没有找到目标目录.");
                }
                //
                p.setNoteName(pnf.getNoteName());
                //笔记名加权 - 防重名
                pnf.setNoteName(duplicateName(pnf.getNoteName(), personalNoteMapper.select(p).size()));

                p = p.getModelByFrom(pnf, false);
                p.setId(UuidUtil.get32UUID());
                p.setCreateTime(new Date());
            } else {    ///更新
                if(!pnf.getNoteParentId().isEmpty() && !parent){
                    return result.flied("没有找到目标目录.");
                }
                p = personalNoteMapper.select(p).get(0);
                if(StringUtils.isEmpty(p)){
                    return result.flied("没有找到改笔记.");
                }
                p = p.getModelByFrom(pnf, true);
                p.setUpdateTime(new Date());
            }
            //进行保存
            personalNoteManagerI.save(p);
            return result.success("操作成功.");
        } catch (DAOException e) {
            return result.flied("系统异常.");
        }
    }

    /**
     * @ description:   笔记重名加序号
     * @ author: longxin
     * @ date: 2020/4/11 0011 8:56
     * * @param: null
     * @ return: a
    */
    private String duplicateName(String name, int duplicateNameNum){
        if(duplicateNameNum != 0){
            return name + duplicateNameNum + 1;
        }
        return name;
    }

    /**
     * 根据id获取详情DetailVO
     *
     * @param personalNoteId 对象id
     * @return 详情DetailVO对象
     * @throws ServiceException 业务层异常
     */
    @Override
    public PersonalNoteDetailVO getPersonalNoteDetailVO(String personalNoteId) throws ServiceException {
        PersonalNote personalNote;
        try {
            personalNote = personalNoteManagerI.getModel(personalNoteId);
        } catch (DAOException e) {
            throw new ServiceException(e, personalNoteId);
        }
        if (!StringUtils.isEmpty(personalNote)){
            PersonalNoteDetailVO personalNoteDetailVO = new PersonalNoteDetailVO();
            BeanUtils.copyProperties(personalNote, personalNoteDetailVO);
            return personalNoteDetailVO;
        } else {
            return new PersonalNoteDetailVO();
        }
    }

    /**
     * 获取DetailVOList
     *
     * @return DetailVO的list对象
     * @throws ServiceException 业务层异常
     */
    @Override
    public List<PersonalNoteDetailVO> listPersonalNoteDetailVOs() throws ServiceException {
        List<PersonalNoteDetailVO> personalNoteDetailVOList = new ArrayList<>();
        try {
            List<PersonalNote> personalNoteList = personalNoteManagerI.listAllModels();
            for (PersonalNote personalNote:personalNoteList) {
                PersonalNoteDetailVO personalNoteDetailVO = new PersonalNoteDetailVO();
                BeanUtils.copyProperties(personalNote,personalNoteDetailVO);
                personalNoteDetailVOList.add(personalNoteDetailVO);
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return personalNoteDetailVOList;
    }

    /**
     * 根据Query对象获取VOList
     *
     * @param personalNoteQuery 条件搜索对象
     * @return VO的list对象
     * @throws ServiceException 业务层异常
     */
    @Override
    public List<PersonalNoteDetailVO> listPersonalNoteDetailVOs(PersonalNoteQuery personalNoteQuery) throws ServiceException {
        List<PersonalNote> personalNoteList;
        try {
            personalNoteList  = personalNoteManagerI.listPersonalNotes(personalNoteQuery);
        } catch (DAOException e) {
            throw new ServiceException(e, personalNoteQuery);
        }
        List<PersonalNoteDetailVO> personalNoteDetailVOList = new ArrayList<>();
        for (PersonalNote personalNote:personalNoteList) {
            PersonalNoteDetailVO personalNoteDetailVO = new PersonalNoteDetailVO();
            BeanUtils.copyProperties(personalNote,personalNoteDetailVO);
            personalNoteDetailVOList.add(personalNoteDetailVO);
        }
        return personalNoteDetailVOList;
    }


    /**
     * 根据PageQuery对象获取page对象
     *
     * @param personalNotePageQuery 条件搜索对象
     * @return page对象
     * @throws ServiceException 业务层异常
     */
    @Override
    public PersonalNotePage listPersonalNotePage(PersonalNotePageQuery personalNotePageQuery) throws ServiceException {
        List<PersonalNote> personalNoteList;
        int total;
        try {
            personalNoteList  = personalNoteManagerI.listPersonalNotes(personalNotePageQuery);
            total = personalNoteManagerI.countPersonalNotes(personalNotePageQuery);
        }catch (DAOException e){
            throw new ServiceException(e, personalNotePageQuery);
        }
        List<PersonalNoteBriefVO> personalNoteBriefVOList = new ArrayList<>();
        for (PersonalNote personalNote:personalNoteList) {
            PersonalNoteBriefVO personalNoteBriefVO = new PersonalNoteBriefVO(personalNote);
            personalNoteBriefVOList.add(personalNoteBriefVO);
        }
        return new PersonalNotePage(personalNotePageQuery, total, personalNoteBriefVOList);
    }

}
