package com.business.app.cnp.personalnote.dao;

import com.business.app.cnp.personalnote.model.PersonalNote;
import com.seed.core.mapper.Mapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface PersonalNoteMapper extends Mapper<PersonalNote> {
    /**
     * @ description: 将回收站笔记还原到指定位置
     * @ author: longxin
     * @ date: 2020/4/8 0008 20:02
     * * @param: null
     * @ return:
     */
    @Update("update cnp_t_personal_note set note_parent_id = ${noteParentId} , is_recycle_bin = ${@com.business.app.Const.NoteConsts$NoteEnum@NOTE_STATUS_RECYCLE.value}  where is_recycle_bin = ${@com.business.app.Const.NoteConsts$NoteEnum@NOTE_STATUS_RECYCLE.value} and id = ${id}")
    int recoveryPersonNote(String id, String noteParentId);

    /**
     * @ description: 批量删除
     * @ author: longxin
     * @ date: 2020/4/8 0008 18:49
     * * @param: null
     * @ return: a
     */
    @Delete(" DELETE  from cnp_t_personal_note where id in ${ids}")
    int deleteByIds(String ids);

    /**
     * @ description: 筛选掉不需或无法删除的id：只有在回收站才能删除
     * @ author: longxin
     * @ date: 2020/4/8 0008 18:42
     * * @param: list
     * @ return: ids
     */
    @Select("SELECT id FROM cnp_t_personal_note as b WHERE id in ${ids} and b.is_recycle_bin = ${@com.business.app.Const.NoteConsts$NoteEnum@NOTE_STATUS_RECYCLE.value}")
    List<String> getNoDelIds(String ids);
}