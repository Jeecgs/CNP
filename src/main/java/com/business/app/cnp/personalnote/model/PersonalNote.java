package com.business.app.cnp.personalnote.model;

import com.business.app.Const.NoteConsts;
import com.business.app.cnp.personalnote.from.PersonalNoteFrom;
import com.business.app.util.SerialNumberGenerator;

import javax.persistence.*;
import java.util.Date;

@Table(name = "cnp_t_personal_note")
public class PersonalNote {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    /**
     * 笔记流水号
     */
    @Column(name = "note_serial_number")
    private String noteSerialNumber;

    /**
     * 笔记创建人
     */
    @Column(name = "create_name")
    private String createName;

    /**
     * 笔记父本id
     */
    @Column(name = "note_parent_id")
    private String noteParentId;

    /**
     * 笔记名称
     */
    @Column(name = "note_name")
    private String noteName;

    /**
     * 是否顶置
     */
    @Column(name = "is_top")
    private String isTop;

    /**
     * 回收站
     */
    @Column(name = "is_recycle_bin")
    private String isRecycleBin;

    /**
     * 创建日期
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新日期
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 笔记内容
     */
    @Column(name = "note_content")
    private String noteContent;

    public PersonalNote getModelByFrom(PersonalNoteFrom pnf, boolean update){
        if(update){
            this.noteContent = pnf.getNoteContent();
            this.noteParentId = pnf.getNoteParentId();
            this.noteName = pnf.getNoteName();
            this.isTop = pnf.getIsTop();
            this.isRecycleBin = NoteConsts.NoteEnum.NOTE_STATUS_DEFAULT.getKey();
            return this;
        }
        this.id = pnf.getId();
        //TODO 创建者
        this.createName = "";
        this.setNoteSerialNumber(SerialNumberGenerator.getInstance().nextSerialNumber(pnf.getNoteSerialCode(), SerialNumberGenerator.getSerialNumber()));
        return this;
    }

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取笔记流水号
     *
     * @return note_serial_number - 笔记流水号
     */
    public String getNoteSerialNumber() {
        return noteSerialNumber;
    }

    /**
     * 设置笔记流水号
     *
     * @param noteSerialNumber 笔记流水号
     */
    public void setNoteSerialNumber(String noteSerialNumber) {
        this.noteSerialNumber = noteSerialNumber;
    }

    /**
     * 获取笔记创建人
     *
     * @return create_name - 笔记创建人
     */
    public String getCreateName() {
        return createName;
    }

    /**
     * 设置笔记创建人
     *
     * @param createName 笔记创建人
     */
    public void setCreateName(String createName) {
        this.createName = createName;
    }

    /**
     * 获取笔记父本id
     *
     * @return note_parent_id - 笔记父本id
     */
    public String getNoteParentId() {
        return noteParentId;
    }

    /**
     * 设置笔记父本id
     *
     * @param noteParentId 笔记父本id
     */
    public void setNoteParentId(String noteParentId) {
        this.noteParentId = noteParentId;
    }

    /**
     * 获取笔记名称
     *
     * @return note_name - 笔记名称
     */
    public String getNoteName() {
        return noteName;
    }

    /**
     * 设置笔记名称
     *
     * @param noteName 笔记名称
     */
    public void setNoteName(String noteName) {
        this.noteName = noteName;
    }

    /**
     * 获取是否顶置
     *
     * @return is_top - 是否顶置
     */
    public String getIsTop() {
        return isTop;
    }

    /**
     * 设置是否顶置
     *
     * @param isTop 是否顶置
     */
    public void setIsTop(String isTop) {
        this.isTop = isTop;
    }

    /**
     * 获取回收站
     *
     * @return is_recycle_bin - 回收站
     */
    public String getIsRecycleBin() {
        return isRecycleBin;
    }

    /**
     * 设置回收站
     *
     * @param isRecycleBin 回收站
     */
    public void setIsRecycleBin(String isRecycleBin) {
        this.isRecycleBin = isRecycleBin;
    }

    /**
     * 获取创建日期
     *
     * @return create_time - 创建日期
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建日期
     *
     * @param createTime 创建日期
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取更新日期
     *
     * @return udate_time - 更新日期
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新日期
     *
     * @param updateTime 更新日期
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取笔记内容
     *
     * @return note_content - 笔记内容
     */
    public String getNoteContent() {
        return noteContent;
    }

    /**
     * 设置笔记内容
     *
     * @param noteContent 笔记内容
     */
    public void setNoteContent(String noteContent) {
        this.noteContent = noteContent;
    }
}