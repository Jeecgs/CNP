package com.business.app.cnp.personalnote.query;

import com.seed.core.query.BasePageQuery;
import lombok.Data;

/**
 * PageQuery
 * @author CodeGenerator
 * @date 2020年04月03日
 */
@Data
public class PersonalNotePageQuery extends BasePageQuery {

}