package com.business.app.cnp.note.service;

import com.business.app.util.Result;
import com.seed.core.service.Service;
import com.seed.core.exception.ServiceException;
import com.business.app.cnp.note.model.*;
import com.business.app.cnp.note.vo.*;
import com.business.app.cnp.note.query.*;
import com.business.app.cnp.note.from.*;
import java.util.List;

/**
 * @author CodeGenerator
 * @date 2020年04月03日
 */
public interface NoteServiceI extends Service<Note>{
    /**
     * 获取全部目录
     * @return
     */
    List<NoteVO> getAllFolder();

    /**
     * 删除笔记本（目录）
     * @param
     * @return
     */
    List<Result> deleteNote(DelNodeFrom delNodeFrom) throws ServiceException;

    /**
     * 新建/更新目录
     * @param noteFrom
     * @return
     */
    Boolean creatOrUpdateNote(NoteFrom noteFrom) throws ServiceException;

    /**
     * 持久化
     *
     * @param noteFrom 需要保存的对象
     * @return 是否成功
     * @throws ServiceException 业务层异常
     */
    Boolean save(NoteFrom noteFrom) throws ServiceException;

    /**
     * 根据id获取详情VO
     *
     * @param noteId 对象id
     * @return 详情VO对象
     * @throws ServiceException 业务层异常
     */
    NoteDetailVO getNoteDetailVO(String noteId) throws ServiceException;

    /**
     * 获取DetailVOList
     *
     * @return DetailVO的list对象
     * @throws ServiceException 业务层异常
     */
    List<NoteDetailVO> listNoteDetailVOs() throws ServiceException;

    /**
     * 根据Query对象获取DetailVOList
     *
     * @param noteQuery 条件搜索对象
     * @return DetailVO的list对象
     * @throws ServiceException 业务层异常
     */
    List<NoteDetailVO> listNoteDetailVOs(NoteQuery noteQuery) throws ServiceException;

    /**
     * 根据PageQuery对象获取page对象
     *
     * @param notePageQuery 条件搜索对象
     * @return page对象
     * @throws ServiceException 业务层异常
     */
    NotePage getNotePage(NotePageQuery notePageQuery) throws ServiceException;
}
