package com.business.app.cnp.note.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

/**
 * @ ClassName: NoteVO
 * @ Author: longxin
 * @ CreatTime: 2020/4/8 0008 20:08
 * @ version: 1.0
 */

@Getter
@Setter
@ToString
public class NoteVO {
    private String noteName;
    private String id;
    private NoteVO noteVOParent;
    private List<NoteVO> noteVOS = new ArrayList<>();

    public NoteVO() {
    }

    public NoteVO(String noteName, String id) {
        this.noteName = noteName;
        this.id = id;
    }

    public void addNoteVO(NoteVO noteVO){
        this.noteVOS.add(noteVO);
    }

}
