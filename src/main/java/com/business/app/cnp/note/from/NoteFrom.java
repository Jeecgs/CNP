package com.business.app.cnp.note.from;

import com.seed.core.from.BaseFrom;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 的From对象
 *
 * @author CodeGenerator
 * @date 2020年04月03日
 */
@Getter
@Setter
@ToString
public class NoteFrom extends BaseFrom {
    /**
     * ID（留空表示新建，反之更新）
     */
    String id;
    /**
     * 用户ID
     */
    String userId;
    /**
     * 父ID（父文件夹）
     */
    String parentId;
    /**
     * 名称
     */
    String noteName;
}