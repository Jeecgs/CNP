package com.business.app.cnp.note.vo;

import com.seed.core.vo.BaseVO;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * ��detailVO����
 * @author CodeGenerator
 * @date 2020��04��03��
 */
@Getter
@Setter
@ToString
public class NoteDetailVO extends BaseVO {

    private String id;

    /**
     * �ʼ�����
     */
    private String noteName;

    /**
     * �ʼǻ�Ŀ¼
     */
    private String noteOrCatalog;

    /**
     * ����id
     */
    private String parentId;

    /**
     * �ʼ�id
     */
    private String noteId;

    /**
     * ��������
     */
    private Date createTime;

    /**
     * ��������
     */
    private Date updateTime;


}