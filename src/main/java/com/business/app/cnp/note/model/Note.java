package com.business.app.cnp.note.model;

import java.util.Date;
import javax.persistence.*;
import com.business.app.Const.NoteConsts;
import com.business.app.cnp.note.from.NoteFrom;


@Table(name = "cnp_t_note")
public class Note {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    /**
     * 笔记名称
     */
    @Column(name = "note_name")
    private String noteName;

    /**
     * 笔记或目录
     */
    @Column(name = "note_or_catalog")
    private String noteOrCatalog;

    /**
     * 父级id
     */
    @Column(name = "parent_id")
    private String parentId;

    /**
     * 笔记id
     */
    @Column(name = "note_id")
    private String noteId;

    /**
     * 创建日期
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新日期
     */
    @Column(name = "update_time")
    private Date updateTime;

    public Note() {
    }

    public Note getModelByFrom(NoteFrom n){
        this.id = n.getId();
        this.noteName = n.getNoteName();
        this.noteOrCatalog = NoteConsts.NoteEnum.NOTETYPE_CATLOG.getKey();
        this.parentId = n.getParentId();
        return this;
    }

    public Note(String id, String noteName, String noteOrCatalog, String parentId,  Date createTime, Date updateTime) {
        this.id = id;
        this.noteName = noteName;
        this.noteOrCatalog = noteOrCatalog;
        this.parentId = parentId;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public Note initNote(){
        Note note = new Note(NoteConsts.DEFAULT_NOTE_DIRECTORY_ID,
                NoteConsts.DEFAULT_NOTE_DIRECTORY_NAME,
                NoteConsts.NoteEnum.NOTETYPE_CATLOG.getKey(),
                NoteConsts.DEFAULT_NOTE_PARTENT_ID,
                new Date(), null);
        return note;
    }


    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取笔记名称
     *
     * @return note_name - 笔记名称
     */
    public String getNoteName() {
        return noteName;
    }

    /**
     * 设置笔记名称
     *
     * @param noteName 笔记名称
     */
    public void setNoteName(String noteName) {
        this.noteName = noteName;
    }

    /**
     * 获取笔记或目录
     *
     * @return note_or_catalog - 笔记或目录
     */
    public String getNoteOrCatalog() {
        return noteOrCatalog;
    }

    /**
     * 设置笔记或目录
     *
     * @param noteOrCatalog 笔记或目录
     */
    public void setNoteOrCatalog(String noteOrCatalog) {
        this.noteOrCatalog = noteOrCatalog;
    }

    /**
     * 获取父级id
     *
     * @return parent_id - 父级id
     */
    public String getParentId() {
        return parentId;
    }

    /**
     * 设置父级id
     *
     * @param parentId 父级id
     */
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    /**
     * 获取笔记id
     *
     * @return note_id - 笔记id
     */
    public String getNoteId() {
        return noteId;
    }

    /**
     * 设置笔记id
     *
     * @param noteId 笔记id
     */
    public void setNoteId(String noteId) {
        this.noteId = noteId;
    }

    /**
     * 获取创建日期
     *
     * @return create_time - 创建日期
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建日期
     *
     * @param createTime 创建日期
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取更新日期
     *
     * @return update_time - 更新日期
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新日期
     *
     * @param updateTime 更新日期
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}