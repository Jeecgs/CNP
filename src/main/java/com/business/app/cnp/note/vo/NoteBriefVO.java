package com.business.app.cnp.note.vo;

import com.seed.core.vo.BaseVO;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * 的BriefVO对象
 * @author CodeGenerator
 * @date 2020年04月03日
 */
@Getter
@Setter
@ToString
public class NoteBriefVO extends BaseVO {

    private String id;

    /**
     * 笔记名称
     */
    private String noteName;

    /**
     * 笔记或目录
     */
    private String noteOrCatalog;

    /**
     * 父级id
     */
    private String parentId;

    /**
     * 笔记id
     */
    private String noteId;

    /**
     * 创建日期
     */
    private Date createTime;

    /**
     * 更新日期
     */
    private Date updateTime;

}