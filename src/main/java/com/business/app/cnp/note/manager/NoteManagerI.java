package com.business.app.cnp.note.manager;

import com.seed.core.manager.Manager;
import com.seed.core.exception.DAOException;
import com.business.app.cnp.note.model.Note;
import com.business.app.cnp.note.query.*;

import java.util.List;

/**
 * @author CodeGenerator
 * @date 2020年04月03日
 */
public interface NoteManagerI extends Manager<Note>{

    /**
     * 根据Query对象获取List
     *
     * @param noteQuery 条件搜索对象
     * @return list对象
     * @throws DAOException 持久层异常
     */
    List<Note> listNotes(NoteQuery noteQuery) throws DAOException;

    /**
     * 根据PageQuery对象获取list对象
     *
     * @param notePageQuery 条件搜索对象
     * @return list对象
     * @throws DAOException 持久层异常
     */
    List<Note> listNotes(NotePageQuery notePageQuery) throws DAOException;


    /**
     * 根据PageQuery对象获取对象计数值
     *
     * @param notePageQuery 条件搜索对象
     * @return 计数值
     * @throws DAOException 持久层异常
     */
    Integer countNotes(NotePageQuery notePageQuery) throws DAOException;

}
