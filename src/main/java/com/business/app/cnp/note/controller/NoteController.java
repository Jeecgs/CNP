package com.business.app.cnp.note.controller;

import com.alibaba.fastjson.JSONObject;
import com.business.app.util.Result;
import com.google.gson.JsonObject;
import com.seed.core.exception.ServiceException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecgframework.core.common.model.json.AjaxJson;
import org.jeecgframework.p3.core.logger.Logger;
import org.jeecgframework.p3.core.logger.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import com.seed.core.controller.AppBaseController;

import com.business.app.cnp.note.from.*;
import com.business.app.cnp.note.vo.*;
import com.business.app.cnp.note.model.*;
import com.business.app.cnp.note.query.*;
import com.business.app.cnp.note.service.*;

import java.util.HashMap;
import java.util.Map;
import java.util.List;

/**
 * @author CodeGenerator
 * @date
 */
@RestController
@Api(value = "NoteController", description = "笔记信息管理", tags = "NoteController")
@RequestMapping("/app/v1/NoteController")
public class NoteController extends AppBaseController {

    private static final Logger logger = LoggerFactory.getLogger(NoteController.class);

    private final NoteServiceI noteService;

    @Autowired
    public NoteController(NoteServiceI noteService) {
        this.noteService = noteService;
    }


    /**
     * @ description:
     * @ author: longxin
     * @ date: 2020/4/8 0008 17:21
     * * @param: null
     * @ return: a
     */
    @RequestMapping(value = "/getAllFolder", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "获取所有文件夹")
    public AjaxJson getAllFolder() {
        logger.info("[api-v1]NoteController.getAllFolder获取所有文件夹");

        AjaxJson result = new AjaxJson();

        List<NoteVO> noteVOList = noteService.getAllFolder();

        Map<String, Object> map = new HashMap<>(16);
        map.put("noteVOList", noteVOList);
        return result.success(map);
    }


    /**
     * @ description:
     * @ author: longxin
     * @ date: 2020/4/8 0008 17:21
     * * @param:
     * @ return:
     */
    @RequestMapping(value = "/creatOrUpdateNote", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "创建/更新笔记本")
    public AjaxJson creatOrUpdatePersonalNote(JSONObject object) {
        logger.info("[api-v1]NoteController.creatOrUpdateNote创建/更新笔记本");

        AjaxJson result = new AjaxJson();

        NoteFrom noteFrom = object.toJavaObject(NoteFrom.class);

        if (StringUtils.isEmpty(noteFrom)) {
            result.filed("参数不能为空！");
            return result;
        }

        Boolean isSuccess;

        try {
            isSuccess  = noteService.creatOrUpdateNote(noteFrom);
            if(isSuccess){
                result.success("操作成功！");
            }
            result.filed("操作失败！");
        } catch (ServiceException e) {
            result.filed("操作异常！");
        }
        return result;
    }




    /**
     * @ description:
     * @ author: longxin
     * @ date: 2020/4/8 0008 17:22
     * * @param: null
     * @ return: a
     */
    @RequestMapping(value = "/deleteNote", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "删除笔记本")
    public AjaxJson deleteNote(JSONObject object) {
        logger.info("[api-v1]NoteController.deleteNote删除笔记本");

        AjaxJson result = new AjaxJson();

        DelNodeFrom delNodeFrom = object.toJavaObject(DelNodeFrom.class);

        if (StringUtils.isEmpty(delNodeFrom)) {
            result.filed("参数不能为空！");
            return result;
        }
        List<Result> results;
        try {
            results = noteService.deleteNote(delNodeFrom);
            result = Result.set(results, result);
        } catch (ServiceException e) {
            result.filed("操作异常！");
        }
        return result;
    }

    @RequestMapping(value = "getNoteSearchFrom", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "根据条件搜索笔记本")
    public AjaxJson getNoteSearchFrom(@RequestBody JSONObject object) {
        logger.info("[api-v1],根据条件搜索笔记本");
        AjaxJson result = new AjaxJson();
        NotePageQuery notePageQuery = object.getObject("notePageQuery", NotePageQuery.class);
        if (StringUtils.isEmpty(notePageQuery)) {
            return result.filed("参数不能为空");
        }
        try {
            NotePage dbOwnerPage = noteService.getNotePage(notePageQuery);
            Map<String, Object> map = new HashMap<>(16);
            map.put("dbOwnerPage", dbOwnerPage);
            return result.success(map);
        } catch (Exception e) {
            e.printStackTrace();
            return result.filed("获取信息失败");
        }
    }

    @RequestMapping(value = "/getNoteList", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "获取笔记表列表")
    public AjaxJson getNoteList() {
        logger.info("[api-v1]NoteController.getNoteVO,获取笔记表列表");

        AjaxJson result = new AjaxJson();

        try {
            List<Note> notes = noteService.listModels();
            Map<String, Object> map = new HashMap<>(16);
            map.put("notes", notes);
            return result.success(map);
        } catch (ServiceException e) {
            e.printStackTrace();
            return result.filed("获取数据失败");
        }
    }


}
