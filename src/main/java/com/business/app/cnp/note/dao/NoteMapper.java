package com.business.app.cnp.note.dao;

import com.business.app.cnp.note.dto.NoteDTO;
import com.business.app.cnp.note.model.Note;
import com.seed.core.mapper.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface NoteMapper extends Mapper<Note> {
    /**
     * 获取全部目录id和名称
     * @return
     */
    @Select("select id, note_name, parent_id from cnp_t_note")
    List<NoteDTO> geAllNote();
}