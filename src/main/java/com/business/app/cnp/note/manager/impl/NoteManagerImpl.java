package com.business.app.cnp.note.manager.impl;

import com.alibaba.excel.util.StringUtils;
import com.business.app.cnp.note.dao.NoteMapper;
import com.business.app.cnp.note.manager.NoteManagerI;
import com.business.app.cnp.note.model.*;
import com.business.app.cnp.note.query.*;
import com.seed.core.exception.DAOException;
import com.seed.core.manager.impl.AbstractManager;
import org.apache.ibatis.session.RowBounds;
import org.jeecgframework.core.util.MyBeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

/**
 * @author CodeGenerator
 * @date 2020年04月03日
 */
@Service("noteManager")
@Transactional(rollbackFor = DAOException.class)
public class NoteManagerImpl extends AbstractManager<Note> implements NoteManagerI {

    private final NoteMapper noteMapper;

    @Autowired
    public NoteManagerImpl(NoteMapper noteMapper) {
        this.noteMapper = noteMapper;
    }

    /**
     * 查询对象转换为model对象
     *
     * @param query 查询对象
     * @return model对象
     * @throws DAOException 持久层异常
     */
    private Note getModel(NoteQuery query) throws DAOException {
        Note model = new Note();
        try {
            MyBeanUtils.copyBeanNotNull2Bean(query, model);
        } catch (Exception e) {
            throw new DAOException(e, query);
        }
        return model;
    }

    /**
     * 查询对象转换为model对象
     *
     * @param query 查询对象
     * @return model对象
     * @throws DAOException 持久层异常
     */
    private Note getModel(NotePageQuery query) throws DAOException {
        Note model = new Note();
        try {
            MyBeanUtils.copyBeanNotNull2Bean(query, model);
        } catch (Exception e) {
            throw new DAOException(e, query);
        }
        return model;
    }

    /**
     * 根据Query对象获取List
     *
     * @param noteQuery 条件搜索对象
     * @return list对象
     * @throws DAOException 持久层异常
     */
    @Override
    public List<Note> listNotes(NoteQuery noteQuery) throws DAOException {
        Note searchNote = this.getModel(noteQuery);
        List<Note> noteList;
        try {
            noteList = this.listModels(searchNote);
        } catch (Exception e) {
            throw new DAOException(e, noteQuery);
        }
        return noteList;
    }


    /**
     * 根据PageQuery对象获取list对象
     *
     * @param notePageQuery 条件搜索对象
     * @return list对象
     * @throws DAOException 持久层异常
     */
    @Override
    public List<Note> listNotes(NotePageQuery notePageQuery) throws DAOException {

        List<Note> noteList;
        Example example = this.getCondition(notePageQuery);
        try {
            RowBounds rowBounds = new RowBounds(notePageQuery.getStart(), notePageQuery.getSize());
            noteList = this.mapper.selectByExampleAndRowBounds(example, rowBounds);
        } catch (Exception e) {
            throw new DAOException(e, notePageQuery);
        }
        return noteList;
    }




    /**
    * 根据PageQuery对象获取对象计数值
    *
    * @param notePageQuery 条件搜索对象
    * @return 计数值
    * @throws DAOException 持久层异常
    */
    @Override
    public Integer countNotes(NotePageQuery notePageQuery) throws DAOException {
        Example example = this.getCondition(notePageQuery);
        Integer total;
        try {
            total = this.mapper.selectCountByCondition(example);
        }catch (Exception e){
            throw new DAOException(e, notePageQuery);
        }
        return total;
    }
    /**
     * 根据PageQuery获取查询条件
     *
     * @param notePageQuery 条件搜索对象
     * @return example
     */
    private Example getCondition(NotePageQuery notePageQuery){
        Example example = new Example(Note.class);
        if (!StringUtils.isEmpty(notePageQuery.getNoteName())) {
            // 添加条件 TODO
            example.and().andLike("noteName", "%" + notePageQuery.getNoteName() + "%");
        }
        example.orderBy("createTime").desc();
        return example;
    }
}