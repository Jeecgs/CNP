package com.business.app.cnp.note.vo;

import com.seed.core.vo.BasePage;
import com.seed.core.query.BasePageQuery;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * ��page����
 * @author CodeGenerator
 * @date 2020��04��03��
 */
@Getter
@Setter
@ToString
public class NotePage extends BasePage<NoteBriefVO>{

    public NotePage() {
        super();
    }

    public NotePage(BasePageQuery basePageQuery, int totalPage, List<NoteBriefVO> noteBriefVOList) {
        super(basePageQuery, totalPage, noteBriefVOList);
    }

}