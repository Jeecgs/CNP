package com.business.app.cnp.note.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @ ClassName: NoteDTO
 * @ Author: longxin
 * @ CreatTime: 2020/4/8 0008 20:34
 * @ version: 1.0
 */
@Getter
@Setter
@ToString
public class NoteDTO {
        private String id;
        private String noteName;
        private String parentId;
}
