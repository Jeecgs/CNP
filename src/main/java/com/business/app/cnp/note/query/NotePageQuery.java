package com.business.app.cnp.note.query;

import com.seed.core.query.BasePageQuery;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * PageQuery
 * @author CodeGenerator
 * @date 2020年04月03日
 */
@Getter
@Setter
@ToString
public class NotePageQuery extends BasePageQuery {

    /**
     * 笔记名称 搜索条件
     *
     */
    private String noteName;

}