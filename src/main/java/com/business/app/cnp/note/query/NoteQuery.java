package com.business.app.cnp.note.query;

import com.seed.core.query.BaseQuery;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Query
 * @author CodeGenerator
 * @date 2020��04��03��
 */
@Getter
@Setter
@ToString
public class NoteQuery extends BaseQuery {

}