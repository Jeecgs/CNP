package com.business.app.cnp.note.service.impl;

import com.business.app.Const.NoteConsts;
import com.business.app.cnp.note.dto.NoteDTO;
import com.business.app.cnp.note.vo.*;
import com.business.app.cnp.note.query.*;
import com.business.app.cnp.note.dao.*;
import com.business.app.cnp.note.model.*;
import com.business.app.cnp.note.service.*;
import com.business.app.cnp.note.manager.*;
import com.business.app.cnp.note.from.*;
import com.business.app.cnp.personalnote.dao.PersonalNoteMapper;
import com.business.app.cnp.personalnote.model.PersonalNote;
import com.business.app.util.Result;
import com.business.app.util.UuidUtil;
import com.seed.core.exception.DAOException;
import com.seed.core.exception.ServiceException;
import com.seed.core.service.impl.AbstractService;
import org.jeecgframework.core.util.MyBeanUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author CodeGenerator
 * @date 2020年04月03日
 */
@Service("noteService")
@Transactional(rollbackFor = ServiceException.class)
public class NoteServiceImpl extends AbstractService<Note> implements NoteServiceI {

    private final PersonalNoteMapper personalnoteMapper;

    private final NoteMapper noteMapper;

    private final NoteManagerI noteManager;

    @Autowired
    public NoteServiceImpl(PersonalNoteMapper personalnoteMapper, NoteManagerI noteManager, NoteMapper noteMapper) {
        this.personalnoteMapper = personalnoteMapper;
        this.noteManager = noteManager;
        this.noteMapper = noteMapper;
    }


    /**
     * 列表树
     * @return
     */
    @Override
    public List<NoteVO> getAllFolder() {
        //TODO 未测试
        List<NoteDTO> notes = noteMapper.geAllNote();
        //多颗树
        List<NoteVO> noteVOS = new ArrayList<>();
        for(NoteDTO noteDto : notes){
            NoteVO note = new NoteVO(noteDto.getNoteName(), noteDto.getId());
            if(noteDto.getParentId().equals(NoteConsts.DEFAULT_NOTE_PARTENT_ID)){
                noteVOS.add(note);
                continue;
            }
            for(NoteDTO noteDto2 : notes){
                NoteVO note2 = new NoteVO(noteDto.getNoteName(), noteDto.getId());
                if(noteDto2.getParentId().equals(noteDto.getId())){
                    note.addNoteVO(note2);
                }
                if(note2.getId().equals(note.getNoteVOParent())){
                    note.setNoteVOParent(note2);
                }
            }

        }
        return noteVOS;
    }



    @Override
    public List<Result> deleteNote(DelNodeFrom delNodeFrom) throws ServiceException{
        List<Result> results = new ArrayList<>();
        PersonalNote p = new PersonalNote();
        List<PersonalNote> ps;
        boolean isSuccess;
        String msg;
        try {
            String[] sourceArray = delNodeFrom.getIds().split(",");
            for(String deleId : sourceArray){
                Result result = new Result();
                msg = "请检查该目录是否存在子信息";
                isSuccess = false;
                p.setNoteParentId(deleId);
                ps = personalnoteMapper.select(p);
                if(ps.size() == 0){
                    noteManager.deleteById(deleId);
                    isSuccess = true;
                    msg = "操作成功";
                }
                result.set(isSuccess, msg);
                results.add(result);
            }
        } catch (DAOException e) {
            isSuccess = false;
            msg = "操作异常！！";
            Result result = new Result(isSuccess, msg);
            results.add(result);
        }

        return results;
    }

    @Override
    public Boolean creatOrUpdateNote(NoteFrom noteFrom) throws ServiceException  {
        Boolean isSuccess = false;
        try {
            //检查默认目录
            inspectionNote();

            Note t = new Note();
            t.setParentId(noteFrom.getParentId());
            List<Note> notes = noteMapper.select(t);
            //数据库能否找找到该父级
            boolean parent = notes.size() < 1 ? true : false;
            t.setParentId(null);

            if(noteFrom.getId().isEmpty()){  //创建
                //判断将要创建的目标文件（文件夹/笔记）的父级是否存在
                if(!parent){
                    return isSuccess;
                }
                t.setNoteName(noteFrom.getNoteName());
                //查询数据库是否存在该文件夹，存在则在改文件夹名后自动加上当前文件夹数量
                notes = noteMapper.select(t);

                if (notes.size() != 0) {
                    noteFrom.setNoteName(noteFrom.getNoteName() + notes.size() + 1);
                }

                t = t.getModelByFrom(noteFrom);
                t.setId(UuidUtil.get32UUID());
                t.setCreateTime(new Date());
            }else {  //更新
                //是否更新父级？判断将要更新的目标文件（文件夹/笔记）的父级是否存在 ：
                if(!noteFrom.getParentId().isEmpty() && !parent){
                    return isSuccess;
                }
                //更新文件夹
                t = t.getModelByFrom(noteFrom);
                t.setUpdateTime(new Date());
            }
            //保存
            noteManager.save(t);
            isSuccess = true;
        } catch (DAOException e) {
            //TODO
            e.printStackTrace();
        }
        return isSuccess;
    }



    /**
     * 检查数据库默认笔记文件夹是否存在
     * 注：默认文件夹不可删
     */
    private void inspectionNote(){
        Note t = new Note().initNote();
        List<Note> notes = noteMapper.select(t);
        if(notes.isEmpty()){
            try {
                noteManager.save(t);
            } catch (DAOException e) {
                //TODO
                e.printStackTrace();
            }
        }
    }

    /**
     * 转换为VO对象
     *
     * @param
     * @return VO对象列表
     */
    private List<NoteDetailVO> listDetailVOS(List<Note> noteList) {
        List<NoteDetailVO> noteDetailVOList = new ArrayList<>();
        for (Note note : noteList) {
            NoteDetailVO noteDetailVO = new NoteDetailVO();
            noteDetailVOList.add(noteDetailVO);
        }
        return noteDetailVOList;
    }

    /**
     * 转换为VO对象
     *
     * @param
     * @return VO对象列表
     */
    private List<NoteBriefVO> listBriefVOS(List<Note> noteList) {
        List<NoteBriefVO> noteBriefVOList = new ArrayList<>();
        for (Note note : noteList) {
            NoteBriefVO noteBriefVO = new NoteBriefVO();
            BeanUtils.copyProperties(note, noteBriefVO);
            noteBriefVOList.add(noteBriefVO);
        }
        return noteBriefVOList;
    }

    /**
     * 持久化
     *
     * @param noteFrom 需要保存的对象
     * @return 是否成功
     * @throws ServiceException 业务层异常
     */
    @Override
    public Boolean save(NoteFrom noteFrom) throws ServiceException {
        Note note = new Note();
        try {
            MyBeanUtils.copyBeanNotNull2Bean(noteFrom, note);
        } catch (Exception e) {
            throw new ServiceException(e, noteFrom);
        }
        Boolean isSuccess;
        try {
            isSuccess = this.save(note);
        } catch (ServiceException e) {
            throw new ServiceException(e, note);
        }
        return isSuccess;
    }

    /**
     * 根据id获取详情DetailVO
     *
     * @param noteId 对象id
     * @return 详情DetailVO对象
     * @throws ServiceException 业务层异常
     */
    @Override
    public NoteDetailVO getNoteDetailVO(String noteId) throws ServiceException {
        Note note;
        try {
            note = noteManager.getModel(noteId);
        } catch (DAOException e) {
            throw new ServiceException(e, noteId);
        }
        if (!StringUtils.isEmpty(note)){
            NoteDetailVO noteDetailVO=new NoteDetailVO();
            BeanUtils.copyProperties(note,noteDetailVO);
            return noteDetailVO;
        } else {
            return new NoteDetailVO();
        }
    }

    /**
     * 获取DetailVOList
     *
     * @return DetailVO的list对象
     * @throws ServiceException 业务层异常
     */
    @Override
    public List<NoteDetailVO> listNoteDetailVOs() throws ServiceException {
        List<Note> noteList;
        try {
            noteList = noteManager.listAllModels();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        List<NoteDetailVO> noteDetailVOList = this.listDetailVOS(noteList);
        return noteDetailVOList;
    }

    /**
     * 根据Query对象获取VOList
     *
     * @param noteQuery 条件搜索对象
     * @return VO的list对象
     * @throws ServiceException 业务层异常
     */
    @Override
    public List<NoteDetailVO> listNoteDetailVOs(NoteQuery noteQuery) throws ServiceException {
        List<Note> noteList;
        try {
            noteList  = noteManager.listNotes(noteQuery);
        } catch (DAOException e) {
            throw new ServiceException(e, noteQuery);
        }
        List<NoteDetailVO> noteDetailVOList = this.listDetailVOS(noteList);
        return noteDetailVOList;
    }


    /**
     * 根据PageQuery对象获取page对象
     *
     * @param notePageQuery 条件搜索对象
     * @return page对象
     * @throws ServiceException 业务层异常
     */
    @Override
    public NotePage getNotePage(NotePageQuery notePageQuery) throws ServiceException {
        List<Note> noteList;
        int total;
        try {
            noteList  = noteManager.listNotes(notePageQuery);
            total = noteManager.countNotes(notePageQuery);
        }catch (DAOException e){
            throw new ServiceException(e, notePageQuery);
        }
        List<NoteBriefVO> noteBriefVOList = this.listBriefVOS(noteList);
        return new NotePage(notePageQuery, total, noteBriefVOList);
    }

}
