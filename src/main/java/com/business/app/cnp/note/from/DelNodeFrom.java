package com.business.app.cnp.note.from;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DelNodeFrom {
    /**
     * 目标ID
     */
    private String ids;
    /**
     * 删除类型：回收/永久删除
     */
    //private String delType;
}
