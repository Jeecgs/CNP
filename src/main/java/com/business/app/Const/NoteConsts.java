package com.business.app.Const;

public class NoteConsts {

    //初始默认文件夹ID
    public static final String DEFAULT_NOTE_DIRECTORY_ID = "aaaaaaaa";
    //初始默认文件夹名称
    public static final String DEFAULT_NOTE_DIRECTORY_NAME = "我的笔记";
    //默认根ID
    public static final String DEFAULT_NOTE_PARTENT_ID = "bbbbbbb";
    //回收站ID
    public static final String DEFAULT_NOTE_RECYCLE_ID = "cccccccc";

    public static final boolean IsTop = true;


    public enum NoteEnum {

        NOTETYPE_NOTE("11", "笔记"),
        NOTETYPE_CATLOG("12", "目录"),


        NOTE_RECYCLE("21", "回收站"),


        NOTE_STATUS_DEFAULT("31", "默认"),
        NOTE_STATUS_PUBLISH("32", "发布"),
        NOTE_STATUS_RECYCLE("33", "回收"),
        NOTE_STATUS_DELETE("34", "永久删除")

        ;
        private String key;
        private String value;


        NoteEnum(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

}
