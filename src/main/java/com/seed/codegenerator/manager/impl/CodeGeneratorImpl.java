package com.seed.codegenerator.manager.impl;

import com.seed.codegenerator.generator.config.GenConfig;
import com.seed.codegenerator.generator.entity.DataBaseTable;
import com.seed.codegenerator.generator.entity.PageData;
import com.seed.codegenerator.generator.entity.TemplateConfig;
import com.seed.codegenerator.generator.service.controller.ControllerGeneratorI;
import com.seed.codegenerator.generator.service.dao.DAOGeneratorI;
import com.seed.codegenerator.generator.service.manager.ManagerGeneratorI;
import com.seed.codegenerator.generator.service.model.ModelGeneratorI;
import com.seed.codegenerator.generator.service.service.ServiceGeneratorI;
import com.seed.codegenerator.generator.service.table.DataBaseTableServiceI;
import com.seed.codegenerator.generator.service.table.entity.DataBaseTableDescribe;
import com.seed.codegenerator.generator.type.GenTypeEnum;
import com.seed.codegenerator.manager.CodeGeneratorI;
import freemarker.template.Configuration;
import org.jeecgframework.p3.core.logger.Logger;
import org.jeecgframework.p3.core.logger.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.util.List;


/**
 * @author gchiaway
 *         日期: 2020-02-12
 *         时间: 21:36
 */
@Service("codeGeneratorService")
@Transactional(rollbackFor = Exception.class)
public class CodeGeneratorImpl implements CodeGeneratorI {

    private static final Logger logger = LoggerFactory.getLogger(CodeGeneratorImpl.class);

    /**
     * 控制层生成类
     */
    private final ControllerGeneratorI controllerGenerator;
    /**
     * 业务层生成类
     */
    private final ServiceGeneratorI serviceGenerator;
    /**
     * 通用业务层生成类
     */
    private final ManagerGeneratorI managerGenerator;
    /**
     * 持久层生成类
     */
    private final DAOGeneratorI daoGenerator;
    /**
     * 实体生成类
     */
    private final ModelGeneratorI modelGenerator;
    /**
     * 表辅助类
     */
    private final DataBaseTableServiceI dataBaseTableService;

    /**
     * 生成配置文件
     */
    private GenConfig genConfig;

    @Autowired
    public CodeGeneratorImpl(ControllerGeneratorI controllerGenerator,
                             DAOGeneratorI daoGenerator,
                             ModelGeneratorI modelGenerator,
                             ServiceGeneratorI serviceGenerator,
                             ManagerGeneratorI managerGenerator,
                             DataBaseTableServiceI dataBaseTableService,
                             GenConfig genConfig) {
        this.controllerGenerator = controllerGenerator;
        this.daoGenerator = daoGenerator;
        this.modelGenerator = modelGenerator;
        this.serviceGenerator = serviceGenerator;
        this.managerGenerator = managerGenerator;
        this.dataBaseTableService = dataBaseTableService;
        this.genConfig = genConfig;
    }

    /**
     * 根据类型生成代码
     *
     * @param genTypeEnum 代码类型
     * @return 生成结果
     */
    @Override
    public Boolean generateCode(GenTypeEnum genTypeEnum, DataBaseTableDescribe dataBaseTableDescribe, String... tableNames) {

        List<DataBaseTable> dataBaseTableList = dataBaseTableService.listDataBaseTablesByTableNamesPrefix(dataBaseTableDescribe, tableNames);

        Configuration cfg;
        try {
            cfg = controllerGenerator.getConfiguration(genTypeEnum);
        } catch (Exception e) {
            logger.error("获取Configuration出错");
            return false;
        }

        Yaml yaml = new Yaml(new Constructor(TemplateConfig.class));
        TemplateConfig templateConfig = (TemplateConfig) yaml.load(this.getClass().getClassLoader()
                .getResourceAsStream(genConfig.getUnderResourcesTemplateFileDirectory() + genTypeEnum.getPath() + "/template.yml"));

        if (StringUtils.isEmpty(templateConfig)) {
            logger.error("获取template信息出错");
            return false;
        }

        Boolean isSuccess;
        for (DataBaseTable dataBaseTable : dataBaseTableList) {
            PageData pageData = new PageData(dataBaseTable, genConfig);

            isSuccess = modelGenerator.genModels(cfg, pageData, templateConfig.getModelFtlList());
            if (!isSuccess) {
                return false;
            }

            isSuccess = serviceGenerator.genService(cfg, pageData, templateConfig.getServiceFtlList());
            if (!isSuccess) {
                return false;
            }

            isSuccess = managerGenerator.genManager(cfg, pageData, templateConfig.getManagerFtlList());
            if (!isSuccess) {
                return false;
            }

            isSuccess = controllerGenerator.genController(cfg, pageData, templateConfig.getControllerFtlList());
            if (!isSuccess) {
                return false;
            }

            isSuccess = daoGenerator.genDAO(pageData);
            if (!isSuccess) {
                return false;
            }
        }

        return true;
    }

    /**
     * 生成JEECG代码
     *
     * @param dataBaseTableDescribe 表前缀
     * @param tableNames            表名
     * @return 生成结果
     */
    @Override
    public Boolean generateJeecgCode(DataBaseTableDescribe dataBaseTableDescribe, String... tableNames) {
        return this.generateCode(GenTypeEnum.JEECG, dataBaseTableDescribe, tableNames);
    }

    /**
     * 生成SSM代码
     *
     * @param dataBaseTableDescribe 表前缀
     * @param tableNames            表名
     * @return 生成结果
     */
    @Override
    public Boolean generateSSMCode(DataBaseTableDescribe dataBaseTableDescribe, String... tableNames) {
        return this.generateCode(GenTypeEnum.SSM, dataBaseTableDescribe, tableNames);
    }

    /**
     * 生成通用代码
     *
     * @param dataBaseTableDescribe 表前缀
     * @param tableNames            表名
     * @return 生成结果
     */
    @Override
    public Boolean generateUniversalCode(DataBaseTableDescribe dataBaseTableDescribe, String... tableNames) {
        return this.generateCode(GenTypeEnum.UNIVERSAL, dataBaseTableDescribe, tableNames);
    }
}
