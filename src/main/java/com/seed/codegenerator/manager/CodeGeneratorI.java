package com.seed.codegenerator.manager;

import com.seed.codegenerator.generator.service.table.entity.DataBaseTableDescribe;
import com.seed.codegenerator.generator.type.GenTypeEnum;

/**
 * @author gchiaway
 *         日期: 2020-02-12
 *         时间: 21:30
 */
public interface CodeGeneratorI {

    /**
     * 根据类型生成代码
     *
     * @param genTypeEnum           代码类型
     * @param dataBaseTableDescribe 表前缀
     * @param tableNames            表名
     * @return 生成结果
     */
    Boolean generateCode(GenTypeEnum genTypeEnum, DataBaseTableDescribe dataBaseTableDescribe, String... tableNames);

    /**
     * 生成JEECG代码
     *
     * @param dataBaseTableDescribe 表前缀
     * @param tableNames            表名
     * @return 生成结果
     */
    Boolean generateJeecgCode(DataBaseTableDescribe dataBaseTableDescribe, String... tableNames);

    /**
     * 生成SSM代码
     *
     * @param dataBaseTableDescribe 表前缀
     * @param tableNames            表名
     * @return 生成结果
     */
    Boolean generateSSMCode(DataBaseTableDescribe dataBaseTableDescribe, String... tableNames);

    /**
     * 生成通用代码
     *
     * @param dataBaseTableDescribe 表前缀
     * @param tableNames            表名
     * @return 生成结果
     */
    Boolean generateUniversalCode(DataBaseTableDescribe dataBaseTableDescribe, String... tableNames);
}
