package com.seed.codegenerator.generator.service.dao;

import com.seed.codegenerator.generator.entity.PageData;
import com.seed.codegenerator.generator.service.base.BaseGeneratorI;

/**
 * @author gchiaway
 *         日期: 2020-02-12
 *         时间: 18:25
 */
public interface DAOGeneratorI extends BaseGeneratorI {


    /**
     * 生成相关的持久层
     *
     * @param pageData 页面数据
     * @return 是否成功
     */
    Boolean genDAO(PageData pageData);

}
