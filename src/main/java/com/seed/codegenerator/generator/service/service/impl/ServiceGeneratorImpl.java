package com.seed.codegenerator.generator.service.service.impl;

import com.seed.codegenerator.generator.config.GenConfig;
import com.seed.codegenerator.generator.entity.FtlConfig;
import com.seed.codegenerator.generator.entity.PageData;
import com.seed.codegenerator.generator.service.base.impl.BaseGeneratorImpl;
import com.seed.codegenerator.generator.service.service.ServiceGeneratorI;
import freemarker.template.Configuration;
import org.jeecgframework.p3.core.logger.Logger;
import org.jeecgframework.p3.core.logger.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author gchiaway
 *         日期: 2020-02-12
 *         时间: 21:54
 */
@Service("serviceGeneratorService")
@Transactional(rollbackFor = Exception.class)
public class ServiceGeneratorImpl extends BaseGeneratorImpl implements ServiceGeneratorI {

    private static final Logger logger = LoggerFactory.getLogger(ServiceGeneratorImpl.class);

    @Autowired
    public ServiceGeneratorImpl(GenConfig genConfig) {
        super(genConfig);
    }

    /**
     * 生成相关的业务类
     *
     * @param configuration ftl信息
     * @param pageData      页面数据
     * @param ftlConfigList 配置文件信息列表
     * @return 是否成功
     */
    @Override
    public Boolean genService(Configuration configuration, PageData pageData, List<FtlConfig> ftlConfigList) {
        return this.genFile(configuration, pageData, ftlConfigList);
    }
}
