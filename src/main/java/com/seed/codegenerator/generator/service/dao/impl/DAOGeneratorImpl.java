package com.seed.codegenerator.generator.service.dao.impl;

import com.seed.codegenerator.generator.config.GenConfig;
import com.seed.codegenerator.generator.entity.PageData;
import com.seed.codegenerator.generator.service.base.impl.BaseGeneratorImpl;
import com.seed.codegenerator.generator.service.dao.DAOGeneratorI;
import org.jeecgframework.p3.core.logger.Logger;
import org.jeecgframework.p3.core.logger.LoggerFactory;
import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.*;
import org.mybatis.generator.internal.DefaultShellCallback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author gchiaway
 *         日期: 2020-02-12
 *         时间: 21:53
 */
@Service("daoGeneratorService")
@Transactional(rollbackFor = Exception.class)
public class DAOGeneratorImpl extends BaseGeneratorImpl implements DAOGeneratorI {

    private static final Logger logger = LoggerFactory.getLogger(DAOGeneratorImpl.class);

    @Autowired
    public DAOGeneratorImpl(GenConfig genConfig) {
        super(genConfig);
    }

    /**
     * 生成相关的持久层
     *
     * @param pageData 页面数据
     * @return 是否成功
     */
    @Override
    public Boolean genDAO(PageData pageData) {
        Context context = new Context(ModelType.FLAT);
        context.setId("Potato");
        context.setTargetRuntime("MyBatis3Simple");
        context.addProperty(PropertyRegistry.CONTEXT_BEGINNING_DELIMITER, "`");
        context.addProperty(PropertyRegistry.CONTEXT_ENDING_DELIMITER, "`");

        JDBCConnectionConfiguration jdbcConnectionConfiguration = new JDBCConnectionConfiguration();
        jdbcConnectionConfiguration.setConnectionURL(genConfig.getJdbcUrl());
        jdbcConnectionConfiguration.setUserId(genConfig.getJdbcUsername());
        jdbcConnectionConfiguration.setPassword(genConfig.getJdbcPassword());
        jdbcConnectionConfiguration.setDriverClass(genConfig.getJdbcDiverClassName());
        context.setJdbcConnectionConfiguration(jdbcConnectionConfiguration);

        PluginConfiguration pluginConfiguration = new PluginConfiguration();
        pluginConfiguration.setConfigurationType("tk.mybatis.mapper.generator.MapperPlugin");
        pluginConfiguration.addProperty("mappers", genConfig.getMapperInterfaceReference());
        context.addPluginConfiguration(pluginConfiguration);

        JavaModelGeneratorConfiguration javaModelGeneratorConfiguration = new JavaModelGeneratorConfiguration();
        javaModelGeneratorConfiguration.setTargetProject(genConfig.getJavaPath());
        javaModelGeneratorConfiguration.setTargetPackage(genConfig.getApiBasePackage() + "." + pageData.getProjectName() + "." + pageData.getEntityNameWithOutUnderline() + ".model");
        context.setJavaModelGeneratorConfiguration(javaModelGeneratorConfiguration);

        SqlMapGeneratorConfiguration sqlMapGeneratorConfiguration = new SqlMapGeneratorConfiguration();
        sqlMapGeneratorConfiguration.setTargetProject(genConfig.getResourcesPath());
        sqlMapGeneratorConfiguration.setTargetPackage("mapper");
        context.setSqlMapGeneratorConfiguration(sqlMapGeneratorConfiguration);

        JavaClientGeneratorConfiguration javaClientGeneratorConfiguration = new JavaClientGeneratorConfiguration();
        javaClientGeneratorConfiguration.setTargetProject(genConfig.getJavaPath());
        javaClientGeneratorConfiguration.setTargetPackage(genConfig.getApiBasePackage() + "." + pageData.getProjectName() + "." + pageData.getEntityNameWithOutUnderline() + ".dao");
        javaClientGeneratorConfiguration.setConfigurationType("XMLMAPPER");
        context.setJavaClientGeneratorConfiguration(javaClientGeneratorConfiguration);

        TableConfiguration tableConfiguration = new TableConfiguration(context);
        tableConfiguration.setTableName(pageData.getTableName());
        tableConfiguration.setDomainObjectName(pageData.getEntityNameHump());
        tableConfiguration.setGeneratedKey(new GeneratedKey("id", "Mysql", true, null));
        context.addTableConfiguration(tableConfiguration);

        List<String> warnings;
        MyBatisGenerator generator;
        try {
            Configuration config = new Configuration();
            config.addContext(context);
            config.validate();
            boolean overwrite = true;
            DefaultShellCallback callback = new DefaultShellCallback(overwrite);
            warnings = new ArrayList<>();
            generator = new MyBatisGenerator(config, callback, warnings);
            generator.generate(null);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("DAOGeneratorImpl.genMyBatis 生成Model和Mapper失败");
            return false;
        }
        return true;
    }
}
