package com.seed.codegenerator.generator.service.manager.impl;

import com.seed.codegenerator.generator.config.GenConfig;
import com.seed.codegenerator.generator.entity.FtlConfig;
import com.seed.codegenerator.generator.entity.PageData;
import com.seed.codegenerator.generator.service.base.impl.BaseGeneratorImpl;
import com.seed.codegenerator.generator.service.manager.ManagerGeneratorI;
import freemarker.template.Configuration;
import org.jeecgframework.p3.core.logger.Logger;
import org.jeecgframework.p3.core.logger.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author gchiaway
 *         日期: 2020-03-23
 *         时间: 19:46
 */
@Service("managerGeneratorService")
@Transactional(rollbackFor = Exception.class)
public class ManagerGeneratorImpl extends BaseGeneratorImpl implements ManagerGeneratorI {

    private static final Logger logger = LoggerFactory.getLogger(ManagerGeneratorImpl.class);

    @Autowired
    public ManagerGeneratorImpl(GenConfig genConfig) {
        super(genConfig);
    }

    /**
     * 生成相关的通用业务层
     *
     * @param configuration ftl信息
     * @param pageData      页面数据
     * @param ftlConfigList 配置文件信息列表
     * @return 是否成功
     */
    @Override
    public Boolean genManager(Configuration configuration, PageData pageData, List<FtlConfig> ftlConfigList) {
        return this.genFile(configuration, pageData, ftlConfigList);
    }
}
