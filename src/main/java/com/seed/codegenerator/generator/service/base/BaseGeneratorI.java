package com.seed.codegenerator.generator.service.base;

import com.seed.codegenerator.generator.entity.FtlConfig;
import com.seed.codegenerator.generator.entity.PageData;
import com.seed.codegenerator.generator.type.GenTypeEnum;
import freemarker.template.Configuration;

import java.io.IOException;
import java.util.List;

/**
 * @author gchiaway
 *         日期: 2020-02-14
 *         时间: 17:21
 */
public interface BaseGeneratorI {

    /**
     * 获取freemarker模板配置文件
     *
     * @param genTypeEnum 生成策略
     * @return freemarker 模板配置文件
     * @throws IOException 可能会因为找不到文件而抛出文件错误
     */
    Configuration getConfiguration(GenTypeEnum genTypeEnum) throws IOException;

    /**
     * 根据模板生成文件
     *
     * @param configuration 模板配置文件
     * @param pageData      页面数据
     * @param ftlConfig     模板配置文件
     * @return 是否成功
     */
    Boolean genFile(Configuration configuration, PageData pageData, FtlConfig ftlConfig);

    /**
     * 根据模板生成文件
     *
     * @param configuration 模板配置文件
     * @param pageData      页面数据
     * @param ftlConfigList 模板配置文件
     * @return 是否成功
     */
    Boolean genFile(Configuration configuration, PageData pageData, List<FtlConfig> ftlConfigList);


}
