package com.seed.codegenerator.generator.service.base.impl;

import com.seed.codegenerator.generator.config.GenConfig;
import com.seed.codegenerator.generator.entity.FtlConfig;
import com.seed.codegenerator.generator.entity.PageData;
import com.seed.codegenerator.generator.service.base.BaseGeneratorI;
import com.seed.codegenerator.generator.type.GenTypeEnum;
import com.seed.util.EntityUtil;
import freemarker.template.Configuration;
import freemarker.template.TemplateExceptionHandler;
import org.jeecgframework.p3.core.logger.Logger;
import org.jeecgframework.p3.core.logger.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * @author gchiaway
 *         日期: 2020-02-14
 *         时间: 17:23
 */
@Service("baseGeneratorService")
@Transactional(rollbackFor = Exception.class)
public class BaseGeneratorImpl implements BaseGeneratorI {

    private static final Logger logger = LoggerFactory.getLogger(BaseGeneratorImpl.class);

    /**
     * 生成配置文件
     * 想让子类可以直接用这个配置类，所以使用 受保护的 这个修饰
     */
    protected GenConfig genConfig;

    @Autowired
    public BaseGeneratorImpl(GenConfig genConfig) {
        this.genConfig = genConfig;
    }

    /**
     * 获取freemarker模板配置文件
     *
     * @param genTypeEnum 生成策略
     * @return freemarker 模板配置文件
     * @throws IOException 可能会因为找不到文件而抛出文件错误
     */
    @Override
    public Configuration getConfiguration(GenTypeEnum genTypeEnum) throws IOException {
        Configuration cfg = new Configuration();
        cfg.setDirectoryForTemplateLoading(new File(genConfig.getTemplateFileDirectory() + genTypeEnum.getPath()));
        cfg.setDefaultEncoding("UTF-8");
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.IGNORE_HANDLER);
        return cfg;
    }



    /**
     * 根据模板生成文件
     *
     * @param configuration 模板配置文件
     * @param pageData      页面数据
     * @param ftlConfig     模板配置文件
     * @return 是否成功
     */
    @Override
    public Boolean genFile(Configuration configuration, PageData pageData, FtlConfig ftlConfig) {
        String dirPath = genConfig.getCodeFileTargetDirectory() + pageData.getModule() + ftlConfig.getTargetDirPath();
        String fileName = File.separator + pageData.getEntityNameHump() + ftlConfig.getTargetNameSuffix();

        File interfaceFile = new File(dirPath + fileName);
        if (!interfaceFile.getParentFile().exists()) {
            boolean isMkdirsSuccess = interfaceFile.getParentFile().mkdirs();
            if (!isMkdirsSuccess) {
                return false;
            }
        }

        try {
            configuration.getTemplate(ftlConfig.getFtlDirPath() + File.separator + ftlConfig.getFtlName()).process(EntityUtil.beanToMap(pageData), new FileWriter(interfaceFile));
        } catch (Exception e) {
            logger.error("模板生成文件失败");
            return false;
        }
        return true;
    }

    /**
     * 根据模板生成文件
     *
     * @param configuration 模板配置文件
     * @param pageData      页面数据
     * @param ftlConfigList 模板配置文件
     * @return 是否成功
     */
    @Override
    public Boolean genFile(Configuration configuration, PageData pageData, List<FtlConfig> ftlConfigList) {
        for (FtlConfig ftlConfig : ftlConfigList) {
            Boolean isThisSuccess = this.genFile(configuration, pageData, ftlConfig);
            if (!isThisSuccess) {
                return false;
            }
        }
        return true;
    }
}
