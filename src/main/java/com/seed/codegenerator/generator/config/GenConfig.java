package com.seed.codegenerator.generator.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author gchiaway
 *         日期: 2020-02-12
 *         时间: 22:21
 */
@Getter
@Component
public class GenConfig {
    /**
     * 数据库名称
     */
    @Value("${project.database.name}")
    private String dataBaseName;

    /**
     * JDBC配置，请修改为你项目的实际配置
     */
    @Value("${jdbc.url.jeecg}")
    private String jdbcUrl;
    @Value("${jdbc.username.jeecg}")
    private String jdbcUsername;
    @Value("${jdbc.password.jeecg}")
    private String jdbcPassword;
    private String jdbcDiverClassName = "com.mysql.jdbc.Driver";

    /**
     * 业务包路径
     */
    @Value("${project.api.package}")
    private String apiBasePackage;

    /**
     * 项目在硬盘上的基础路径
     */
    private String projectPath = System.getProperty("user.dir");

    /**
     * 模板位置
     */
    private String underResourcesTemplateFileDirectory = "generator/template/";

    /**
     * 模板位置
     */
    private String templateFileDirectory = projectPath + "/src/main/resources/" + underResourcesTemplateFileDirectory;

    /**
     * java文件路径
     */
    private String javaPath = projectPath + "/src/main/java";

    /**
     * 资源文件路径
     */
    private String resourcesPath = projectPath + "/src/main/resources";

    /**
     * Mapper插件基础接口的完全限定名
     */
    private String mapperInterfaceReference = "com.seed.core.mapper.Mapper";

    /**
     * 作者信息
     */
    private String author = "CodeGenerator";

    /**
     * 日期信息
     */
    private String date = new SimpleDateFormat("yyyy年MM月dd日").format(new Date());

    /**
     * 获取生成代码位置的根级目录
     *
     * @return 路径
     */
    public String getCodeFileTargetDirectory() {
        return javaPath + this.packageConvertPath(apiBasePackage);
    }

    /**
     * 转换包路径
     *
     * @param packageName 包名称
     * @return 路径
     */
    private String packageConvertPath(String packageName) {
        return String.format("/%s/", packageName.contains(".") ? packageName.replaceAll("\\.", "/") : packageName);
    }

}
