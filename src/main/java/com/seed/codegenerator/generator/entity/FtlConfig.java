package com.seed.codegenerator.generator.entity;

import lombok.Data;

/**
 * @author gchiaway
 *         日期: 2020-03-23
 *         时间: 17:08
 */
@Data
public class FtlConfig {

    /**
     * 目录路径
     */
    private String ftlDirPath;

    /**
     * 文件名称
     */
    private String ftlName;

    /**
     * 目录路径
     */
    private String targetDirPath;

    /**
     * 生成文件名称后缀
     */
    private String targetNameSuffix;

}
