package com.seed.codegenerator.generator.entity;

import lombok.Data;

import java.util.List;

/**
 * @author gchiaway
 *         日期: 2020-03-23
 *         时间: 17:05
 */
@Data
public class TemplateConfig {

    /**
     * 模板名称
     */
    private String templateName;

    /**
     * 版本
     */
    private String version;

    /**
     * controller模板文件
     */
    private List<FtlConfig> controllerFtlList;

    /**
     * service模板文件
     */
    private List<FtlConfig> serviceFtlList;

    /**
     * manager模板文件
     */
    private List<FtlConfig> managerFtlList;

    /**
     * model模板文件
     */
    private List<FtlConfig> modelFtlList;

}
