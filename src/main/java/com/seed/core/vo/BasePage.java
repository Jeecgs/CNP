package com.seed.core.vo;

import com.seed.core.query.BasePageQuery;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author gchiaway
 *         日期: 2020-02-21
 *         时间: 15:51
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BasePage<T> extends BaseVO {

    public BasePage() {
    }

    public BasePage(BasePageQuery basePageQuery, int totalPage, List<T> list) {
        this(basePageQuery.getPage(), basePageQuery.getSize(), totalPage, list);
    }

    public BasePage(int page, int size, int totalPage, List<T> list) {
        super();
        if (totalPage < size) {
            this.page = 1;
        } else {
            if (page * size > totalPage) {
                this.page = totalPage / size;
            } else {
                this.page = page;
            }
        }
        this.size = size;
        this.totalPage = totalPage;
        this.list = list;
    }

    @ApiModelProperty(value = "页数", example = "1")
    private int page;

    @ApiModelProperty(value = "每页条数", example = "1")
    private int size;

    @ApiModelProperty(value = "总页数", example = "1")
    private int totalPage;

    @ApiModelProperty(value = "数据")
    private List<T> list;
}
