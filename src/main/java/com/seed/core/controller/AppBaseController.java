package com.seed.core.controller;

import io.jsonwebtoken.Claims;
import org.jeecgframework.core.interceptors.DateConvertEditor;
import org.jeecgframework.jwt.def.JwtConstants;
import org.jeecgframework.web.system.pojo.base.TSUser;
import org.jeecgframework.web.system.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * @author gchiaway
 *         日期: 2020-02-22
 *         时间: 17:27
 */
@Controller
@RequestMapping("/appBaseController")
public class AppBaseController {

    @Autowired
    private UserService userService;

    /**
     * 将前台传递过来的日期格式的字符串，自动转化为Date类型
     *
     * @param binder MVC日期绑定
     */
    @InitBinder
    public void initBinder(ServletRequestDataBinder binder) {
        binder.registerCustomEditor(Date.class, new DateConvertEditor());
    }


    /**
     * 根据请求中的token获取 TS用户id
     *
     * @param request 请求
     * @return TS用户id
     */
    public String getCurrentUserId(HttpServletRequest request) {
        Claims claims = (Claims) request.getAttribute(JwtConstants.CURRENT_TOKEN_CLAIMS);
        if (StringUtils.isEmpty(claims)) {
            return "";
        }

        String userName = claims.getId();
        TSUser user = userService.findUniqueByProperty(TSUser.class, "userName", userName);
        if (StringUtils.isEmpty(user)) {
            return "";
        }

        return user.getId();
    }


    /**
     * 为了支持H5页面跨域访问，对options请求统一进行处理
     *
     * @param theHttpServletResponse OPTIONS响应
     */
    @RequestMapping(method = RequestMethod.OPTIONS, value = "/**")
    public void commonOptions(HttpServletResponse theHttpServletResponse) {
        theHttpServletResponse.addHeader("Access-Control-Allow-Origin", "*");
        theHttpServletResponse.addHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
        theHttpServletResponse.addHeader("Access-Control-Allow-Headers", "content-type,X-AUTH-TOKEN");
    }
}
