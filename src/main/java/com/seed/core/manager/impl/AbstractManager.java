package com.seed.core.manager.impl;

import com.seed.core.exception.DAOException;
import com.seed.core.manager.Manager;
import com.seed.core.mapper.Mapper;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Condition;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

/**
 * 持久层对象
 *
 * @author gchiaway
 *         日期: 2020-01-17
 *         时间: 14:30
 */
public abstract class AbstractManager<T> implements Manager<T> {

    @Autowired
    protected Mapper<T> mapper;

    /**
     * 当前泛型真实类型的Class
     */
    private Class<T> modelClass;

    public AbstractManager() {
        ParameterizedType pt = (ParameterizedType) this.getClass().getGenericSuperclass();
        modelClass = (Class<T>) pt.getActualTypeArguments()[0];
    }


    /**
     * 持久化
     *
     * @param model 需要保存的对象
     * @return 是否成功
     * @throws DAOException 持久层通用错误
     */
    @Override
    public Boolean save(T model) throws DAOException {
        try {
            return mapper.insertSelective(model) > 0;
        } catch (Exception e) {
            throw new DAOException(e, model);
        }
    }

    /**
     * 批量持久化
     *
     * @param models 需要保存的对象们
     * @return 是否成功
     * @throws DAOException 持久层通用错误
     */
    @Override
    public Boolean save(List<T> models) throws DAOException {
        try {
            return mapper.insertList(models) > 0;
        } catch (Exception e) {
            throw new DAOException(e, models);
        }
    }

    /**
     * 根据主键删除
     *
     * @param id 主键
     * @return 是否成功
     * @throws DAOException 持久层通用错误
     */
    @Override
    public Boolean deleteById(Object id) throws DAOException {
        try {
            return mapper.deleteByPrimaryKey(id) > 0;
        } catch (Exception e) {
            throw new DAOException(e, id);
        }
    }

    /**
     * 批量刪除
     *
     * @param ids eg：ids -> “1,2,3,4”
     * @return 是否成功
     * @throws DAOException 持久层通用错误
     */
    @Override
    public Boolean deleteByIds(String ids) throws DAOException {
        try {
            if (StringUtils.isEmpty(ids)){
                return false;
            }
            for (String id : ids.split(",")){
                if (!this.deleteById(id)){
                    return false;
                }
            }
        } catch (Exception e) {
            throw new DAOException(e, ids);
        }
        return true;
    }

    /**
     * 更新，完全更新
     *
     * @param model 需要更新的对象
     * @return 是否成功
     * @throws DAOException 持久层通用错误
     */
    @Override
    public Boolean update(T model) throws DAOException {
        try {
            return mapper.updateByPrimaryKey(model) > 0;
        } catch (Exception e) {
            throw new DAOException(e, model);
        }
    }

    /**
     * 更新，只更新查询到的字段
     *
     * @param model 需要更新的对象
     * @return 是否成功
     * @throws DAOException 持久层通用错误
     */
    @Override
    public Boolean updateSelective(T model) throws DAOException {
        try {
            return mapper.updateByPrimaryKeySelective(model) > 0;
        } catch (Exception e) {
            throw new DAOException(e, model);
        }
    }

    /**
     * 通过主键查找
     *
     * @param id 主键
     * @return 查找到的单个对象
     * @throws DAOException 持久层通用错误
     */
    @Override
    public T getModel(Object id) throws DAOException {
        try {
            return mapper.selectByPrimaryKey(id);
        } catch (Exception e) {
            throw new DAOException(e, id);
        }
    }

    /**
     * 通过多个ID查找
     *
     * @param ids eg：ids -> “1,2,3,4”
     * @return 查到的多个对象
     * @throws DAOException 持久层通用错误
     */
    @Override
    public List<T> listModels(String ids) throws DAOException {
        try {
            return mapper.selectByIds(ids);
        } catch (Exception e) {
            throw new DAOException(e, ids);
        }
    }

    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）查找,value需符合unique约束
     *
     * @param fieldName 字段名称
     * @param value     字段值
     * @return 查找到的对象
     * @throws DAOException 持久层通用错误
     */
    @Override
    public List<T> listModels(String fieldName, Object value) throws DAOException {
        T model;
        try {
            model = modelClass.newInstance();
            Field field = modelClass.getDeclaredField(fieldName);
            field.setAccessible(true);
            field.set(model, value);
        } catch (ReflectiveOperationException e) {
            throw new DAOException(e, fieldName + ":" + value);
        }

        List<T> list;
        try {
            list = this.listModels(model);
        } catch (DAOException e) {
            throw new DAOException(e, fieldName + ":" + value);
        }
        return list;
    }

    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）查找,value需符合unique约束
     *
     * @param fieldName 字段名称
     * @param value     字段值
     * @return 满足条件的总条数
     * @throws DAOException 持久层通用错误
     */
    @Override
    public Integer countModels(String fieldName, Object value) throws DAOException {
        try {
            List<T> list = this.listModels(fieldName, value);
            if (StringUtils.isEmpty(list)) {
                return 0;
            }
            return list.size();
        } catch (DAOException e) {
            throw new DAOException(e, fieldName + ":" + value);
        }
    }

    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）分页查找,value需符合unique约束
     *
     * @param fieldName 字段名称
     * @param value     字段值
     * @param rowBounds 分页条件
     * @return 查找到的对象
     * @throws DAOException 持久层通用错误
     */
    @Override
    public List<T> listModels(String fieldName, Object value, RowBounds rowBounds) throws DAOException {
        T model;
        try {
            model = modelClass.newInstance();
            Field field = modelClass.getDeclaredField(fieldName);
            field.setAccessible(true);
            field.set(model, value);
        } catch (ReflectiveOperationException e) {
            throw new DAOException(e, fieldName + ":" + value, rowBounds);
        }

        List<T> list;
        try {
            list = this.listModels(model, rowBounds);
        } catch (DAOException e) {
            throw new DAOException(e, fieldName + ":" + value, rowBounds);
        }
        return list;
    }

    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）查找,value需符合unique约束
     *
     * @param model 对象
     * @return 查找到的对象列表
     * @throws DAOException 持久层通用错误
     */
    @Override
    public List<T> listModels(T model) throws DAOException {
        try {
            return mapper.select(model);
        } catch (Exception e) {
            throw new DAOException(e, model);
        }
    }

    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）查找,value需符合unique约束
     *
     * @param model 对象
     * @return 满足条件的总条数
     * @throws DAOException 持久层通用错误
     */
    @Override
    public Integer countModels(T model) throws DAOException {
        try {
            List<T> list = this.listModels(model);
            if (StringUtils.isEmpty(list)) {
                return 0;
            }
            return list.size();
        } catch (Exception e) {
            throw new DAOException(e, model);
        }
    }

    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）查找,value需符合unique约束
     *
     * @param model     对象
     * @param rowBounds 分页条件
     * @return 查找到的单个对象
     * @throws DAOException 持久层通用错误
     */
    @Override
    public List<T> listModels(T model, RowBounds rowBounds) throws DAOException {
        try {
            return mapper.selectByRowBounds(model, rowBounds);
        } catch (Exception e) {
            throw new DAOException(e, model, rowBounds);
        }
    }

    /**
     * 根据条件查找
     *
     * @param condition tk.mybatis包装的对象，继承于Example
     * @return 查到的多个对象
     * @throws DAOException 持久层通用错误
     */
    @Override
    public List<T> listModels(Condition condition) throws DAOException {
        try {
            return mapper.selectByCondition(condition);
        } catch (Exception e) {
            throw new DAOException(e, condition);
        }
    }


    /**
     * 根据条件查找
     *
     * @param condition tk.mybatis包装的对象，继承于Example
     * @return 满足条件的总条数
     * @throws DAOException 持久层通用错误
     */
    @Override
    public Integer countModels(Condition condition) throws DAOException {
        try {
            List<T> list = this.listModels(condition);
            if (StringUtils.isEmpty(list)) {
                return 0;
            }
            return list.size();
        } catch (Exception e) {
            throw new DAOException(e, condition);
        }
    }

    /**
     * 根据条件查找
     *
     * @param condition tk.mybatis包装的对象，继承于Example
     * @param rowBounds 分页条件
     * @return 查到的多个对象
     * @throws DAOException 持久层通用错误
     */
    @Override
    public List<T> listModels(Condition condition, RowBounds rowBounds) throws DAOException {
        try {
            return mapper.selectByExampleAndRowBounds(condition, rowBounds);
        } catch (Exception e) {
            throw new DAOException(e, condition, rowBounds);
        }
    }

    /**
     * 获取所有
     *
     * @return 查到的多个对象
     * @throws DAOException 持久层通用错误
     */
    @Override
    public List<T> listAllModels() throws DAOException {
        try {
            return mapper.selectAll();
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }


}
