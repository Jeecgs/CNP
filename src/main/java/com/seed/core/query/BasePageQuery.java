package com.seed.core.query;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author gchiaway
 *         日期: 2020-02-21
 *         时间: 15:37
 */
@Getter
@Setter
@ToString
public class BasePageQuery extends BaseQuery {
    /**
     * 页数
     */
    @ApiModelProperty(value = "页数", example = "1")
    private int page = 1;

    /**
     * 每页条数
     */
    @ApiModelProperty(value = "每页条数", example = "5")
    private int size = 5;

    /**
     * 获取开始下标
     *
     * @return 开始下标
     */
    public int getStart() {
        if (page <= 0 || size <= 0) {
            return 0;
        } else {
            return (this.getPage() - 1) * this.getSize();
        }
    }

}
