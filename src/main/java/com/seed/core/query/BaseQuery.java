package com.seed.core.query;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author gchiaway
 *         日期: 2020-02-21
 *         时间: 15:42
 */
@Getter
@Setter
@ToString
public class BaseQuery {

}
