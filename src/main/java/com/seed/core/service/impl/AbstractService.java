package com.seed.core.service.impl;

import com.seed.core.exception.DAOException;
import com.seed.core.exception.ServiceException;
import com.seed.core.manager.Manager;
import com.seed.core.service.Service;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Condition;

import java.util.List;

/**
 * 基于通用MyBatis manager插件的Service接口的实现
 *
 * @author gchiaway
 *         日期: 2020-01-17
 *         时间: 14:30
 */
public abstract class AbstractService<T> implements Service<T> {

    @Autowired
    private Manager<T> manager;

    /**
     * 持久化
     *
     * @param model 需要保存的对象
     * @return 是否成功
     * @throws ServiceException 业务层通用错误
     */
    @Override
    public Boolean save(T model) throws ServiceException {
        try {
            return manager.save(model);
        } catch (DAOException e) {
            throw new ServiceException(e, model);
        }
    }

    /**
     * 批量持久化
     *
     * @param models 需要保存的对象们
     * @return 是否成功
     * @throws ServiceException 业务层通用错误
     */
    @Override
    public Boolean save(List<T> models) throws ServiceException {
        try {
            return manager.save(models);
        } catch (DAOException e) {
            throw new ServiceException(e, models);
        }
    }

    /**
     * 根据主键删除
     *
     * @param id 主键
     * @return 是否成功
     * @throws ServiceException 业务层通用错误
     */
    @Override
    public Boolean deleteById(Object id) throws ServiceException {
        try {
            return manager.deleteById(id);
        } catch (DAOException e) {
            throw new ServiceException(e, id);
        }
    }

    /**
     * 批量刪除
     *
     * @param ids eg：ids -> “1,2,3,4”
     * @return 是否成功
     * @throws ServiceException 业务层通用错误
     */
    @Override
    public Boolean deleteByIds(String ids) throws ServiceException {
        try {
            return manager.deleteByIds(ids);
        } catch (DAOException e) {
            throw new ServiceException(e, ids);
        }
    }

    /**
     * 更新
     *
     * @param model 需要更新的对象
     * @return 是否成功
     * @throws ServiceException 业务层通用错误
     */
    @Override
    public Boolean update(T model) throws ServiceException {
        try {
            return manager.update(model);
        } catch (DAOException e) {
            throw new ServiceException(e, model);
        }
    }

    /**
     * 更新，只更新不为空的字段
     *
     * @param model 需要更新的对象
     * @return 是否成功
     * @throws ServiceException 业务层通用错误
     */
    @Override
    public Boolean updateSelective(T model) throws ServiceException {
        try {
            return manager.updateSelective(model);
        } catch (DAOException e) {
            throw new ServiceException(e, model);
        }
    }

    /**
     * 通过主键查找
     *
     * @param id 主键
     * @return 查找到的单个对象
     * @throws ServiceException 业务层通用错误
     */
    @Override
    public T getModel(Object id) throws ServiceException {
        try {
            return manager.getModel(id);
        } catch (DAOException e) {
            throw new ServiceException(e, id);
        }
    }

    /**
     * 通过多个ID查找
     *
     * @param ids eg：ids -> “1,2,3,4”
     * @return 查到的多个对象
     * @throws ServiceException 业务层通用错误
     */
    @Override
    public List<T> listModels(String ids) throws ServiceException {
        try {
            return manager.listModels(ids);
        } catch (DAOException e) {
            throw new ServiceException(e, ids);
        }
    }


    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）查找,value需符合unique约束
     *
     * @param fieldName 字段名称
     * @param value     字段值
     * @return 查找到的对象
     * @throws ServiceException 业务层通用错误
     */
    @Override
    public List<T> listModels(String fieldName, Object value) throws ServiceException {
        try {
            return manager.listModels(fieldName, value);
        } catch (DAOException e) {
            throw new ServiceException(e, fieldName + ":" + value);
        }
    }

    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）查找,value需符合unique约束，返回计数值
     *
     * @param fieldName 字段名称
     * @param value     字段值
     * @return 满足条件的总条数
     * @throws ServiceException 业务层通用错误
     */
    @Override
    public Integer countModels(String fieldName, Object value) throws ServiceException {
        try {
            return manager.countModels(fieldName, value);
        } catch (DAOException e) {
            throw new ServiceException(e, fieldName + ":" + value);
        }
    }

    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）分页查找,value需符合unique约束
     *
     * @param fieldName 字段名称
     * @param value     字段值
     * @param rowBounds 分页条件
     * @return 查找到的对象
     * @throws ServiceException 业务层通用错误
     */
    @Override
    public List<T> listModels(String fieldName, Object value, RowBounds rowBounds) throws ServiceException {
        try {
            return manager.listModels(fieldName, value, rowBounds);
        } catch (DAOException e) {
            throw new ServiceException(e, fieldName + ":" + value, rowBounds);
        }
    }

    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）查找,value需符合unique约束
     *
     * @param model 对象
     * @return 查找到的对象列表
     * @throws ServiceException 业务层通用错误
     */
    @Override
    public List<T> listModels(T model) throws ServiceException {
        try {
            return manager.listModels(model);
        } catch (DAOException e) {
            throw new ServiceException(e, model);
        }
    }

    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）查找,value需符合unique约束
     *
     * @param model 对象
     * @return 满足条件的总条数
     * @throws ServiceException 业务层通用错误
     */
    @Override
    public Integer countModels(T model) throws ServiceException {
        try {
            return manager.countModels(model);
        } catch (DAOException e) {
            throw new ServiceException(e, model);
        }
    }

    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）查找,value需符合unique约束
     *
     * @param model     对象
     * @param rowBounds 分页条件
     * @return 查找到的单个对象
     * @throws ServiceException 业务层通用错误
     */
    @Override
    public List<T> listModels(T model, RowBounds rowBounds) throws ServiceException {
        try {
            return manager.listModels(model, rowBounds);
        } catch (DAOException e) {
            throw new ServiceException(e, model, rowBounds);
        }
    }

    /**
     * 根据条件查找
     *
     * @param condition tk.mybatis包装的对象，继承于Example
     * @return 查到的多个对象
     * @throws ServiceException 业务层通用错误
     */
    @Override
    public List<T> listModels(Condition condition) throws ServiceException {
        try {
            return manager.listModels(condition);
        } catch (DAOException e) {
            throw new ServiceException(e, condition);
        }
    }

    /**
     * 根据条件查找
     *
     * @param condition tk.mybatis包装的对象，继承于Example
     * @return 满足条件的总条数
     * @throws ServiceException 业务层通用错误
     */
    @Override
    public Integer countModels(Condition condition) throws ServiceException {
        try {
            return manager.countModels(condition);
        } catch (DAOException e) {
            throw new ServiceException(e, condition);
        }
    }

    /**
     * 根据条件查找
     *
     * @param condition tk.mybatis包装的对象，继承于Example
     * @param rowBounds 分页条件
     * @return 查到的多个对象
     * @throws ServiceException 业务层通用错误
     */
    @Override
    public List<T> listModels(Condition condition, RowBounds rowBounds) throws ServiceException {
        try {
            return manager.listModels(condition, rowBounds);
        } catch (DAOException e) {
            throw new ServiceException(e, condition, rowBounds);
        }
    }

    /**
     * 获取所有
     *
     * @return 查到的多个对象
     * @throws ServiceException 业务层通用错误
     */
    @Override
    public List<T> listModels() throws ServiceException {
        try {
            return manager.listAllModels();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
