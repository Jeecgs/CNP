package com.seed.core.service;

import com.seed.core.exception.ServiceException;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.entity.Condition;

import java.util.List;

/**
 * Service 层 基础接口，其他Service 接口 请继承该接口
 *
 * @author gchiaway
 *         日期: 2020-01-17
 *         时间: 14:30
 */
public interface Service<T> {
    /**
     * 持久化
     *
     * @param model 需要保存的对象
     * @return 是否成功
     * @throws ServiceException 业务层通用错误
     */
    Boolean save(T model) throws ServiceException;

    /**
     * 批量持久化
     *
     * @param models 需要保存的对象们
     * @return 是否成功
     * @throws ServiceException 业务层通用错误
     */
    Boolean save(List<T> models) throws ServiceException;

    /**
     * 根据主键删除
     *
     * @param id 主键
     * @return 是否成功
     * @throws ServiceException 业务层通用错误
     */
    Boolean deleteById(Object id) throws ServiceException;

    /**
     * 批量刪除
     *
     * @param ids eg：ids -> “1,2,3,4”
     * @return 是否成功
     * @throws ServiceException 业务层通用错误
     */
    Boolean deleteByIds(String ids) throws ServiceException;

    /**
     * 更新，完全更新，更新所有字段，包括为空的字段
     *
     * @param model 需要更新的对象
     * @return 是否成功
     * @throws ServiceException 业务层通用错误
     */
    Boolean update(T model) throws ServiceException;

    /**
     * 更新，只更新不为空的字段
     *
     * @param model 需要更新的对象
     * @return 是否成功
     * @throws ServiceException 业务层通用错误
     */
    Boolean updateSelective(T model) throws ServiceException;

    /**
     * 通过主键查找
     *
     * @param id 主键
     * @return 查找到的单个对象
     * @throws ServiceException 业务层通用错误
     */
    T getModel(Object id) throws ServiceException;

    /**
     * 通过多个ID查找
     *
     * @param ids eg：ids -> “1,2,3,4”
     * @return 查到的多个对象
     * @throws ServiceException 业务层通用错误
     */
    List<T> listModels(String ids) throws ServiceException;

    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）查找,value需符合unique约束
     *
     * @param fieldName 字段名称
     * @param value     字段值
     * @return 查找到的对象
     * @throws ServiceException 业务层通用错误
     */
    List<T> listModels(String fieldName, Object value) throws ServiceException;

    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）查找,value需符合unique约束，返回计数值
     *
     * @param fieldName 字段名称
     * @param value     字段值
     * @return 满足条件的总条数
     * @throws ServiceException 业务层通用错误
     */
    Integer countModels(String fieldName, Object value) throws ServiceException;

    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）分页查找,value需符合unique约束
     *
     * @param fieldName 字段名称
     * @param value     字段值
     * @param rowBounds 分页条件
     * @return 查找到的对象
     * @throws ServiceException 业务层通用错误
     */
    List<T> listModels(String fieldName, Object value, RowBounds rowBounds) throws ServiceException;

    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）查找,value需符合unique约束
     *
     * @param model 对象
     * @return 查找到的单个对象
     * @throws ServiceException 业务层通用错误
     */
    List<T> listModels(T model) throws ServiceException;

    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）查找,value需符合unique约束
     *
     * @param model 对象
     * @return 满足条件的总条数
     * @throws ServiceException 业务层通用错误
     */
    Integer countModels(T model) throws ServiceException;

    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）查找,value需符合unique约束
     *
     * @param model     对象
     * @param rowBounds 分页条件
     * @return 查找到的单个对象
     * @throws ServiceException 业务层通用错误
     */
    List<T> listModels(T model, RowBounds rowBounds) throws ServiceException;


    /**
     * 根据条件查找
     *
     * @param condition tk.mybatis包装的对象，继承于Example
     * @return 查到的多个对象
     * @throws ServiceException 业务层通用错误
     */
    List<T> listModels(Condition condition) throws ServiceException;


    /**
     * 根据条件查找
     *
     * @param condition tk.mybatis包装的对象，继承于Example
     * @return 满足条件的总条数
     * @throws ServiceException 业务层通用错误
     */
    Integer countModels(Condition condition) throws ServiceException;


    /**
     * 根据条件查找
     *
     * @param condition tk.mybatis包装的对象，继承于Example
     * @param rowBounds 分页条件
     * @return 查到的多个对象
     * @throws ServiceException 业务层通用错误
     */
    List<T> listModels(Condition condition, RowBounds rowBounds) throws ServiceException;

    /**
     * 获取所有
     *
     * @return 查到的多个对象
     * @throws ServiceException 业务层通用错误
     */
    List<T> listModels() throws ServiceException;

}
