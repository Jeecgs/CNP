package com.seed.sms.entity;

import com.seed.sms.enums.SMSTypeEnum;
import lombok.Data;

/**
 * @author gchiaway
 *         日期: 2019-10-23
 *         时间: 11:52
 */
@Data
public class SMSEntity {
    /**
     * 目标号码
     */
    private String phone;
    /**
     * 短信类型
     */
    private SMSTypeEnum smsTypeEnum;
}
