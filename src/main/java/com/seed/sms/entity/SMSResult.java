package com.seed.sms.entity;

import lombok.Data;

/**
 * @author gchiaway
 *         日期: 2019-10-23
 *         时间: 11:53
 */
@Data
public class SMSResult {
    private boolean isSuccess=true;
    private String msg="发送成功";
    private String verificationCode;
}
