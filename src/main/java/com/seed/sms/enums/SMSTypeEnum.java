package com.seed.sms.enums;

/**
 * @author gchiaway
 *         日期: 2019-10-23
 *         时间: 11:55
 */
public enum SMSTypeEnum {
    /**
     * 仅仅是信息
     */
    JUST_MSG("JUST_MSG",""),
    /**
     * 带验证码的短信
     */
    VERIFICATION_MSG("VERIFICATION_MSG","您的验证码为RANDOM_CODE，在10分钟内有效。"),
    ;

    /**
     * 短信类型
     */
    private String type;

    /**
     * 短信内容
     */
    private String content;

    SMSTypeEnum(String type, String content) {
        this.type = type;
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
