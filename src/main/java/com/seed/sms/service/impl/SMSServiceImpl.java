package com.seed.sms.service.impl;

import com.seed.sms.entity.SMSEntity;
import com.seed.sms.entity.SMSResult;
import com.seed.sms.service.SMSService;
import com.seed.util.HttpUtils;
import org.jeecgframework.core.common.service.impl.CommonServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author gchiaway
 *         日期: 2019-10-23
 *         时间: 11:41
 */
@Service("SMSService")
@Transactional(rollbackFor = Exception.class)
public class SMSServiceImpl extends CommonServiceImpl implements SMSService {

    @Value("${SMSConfig.actionUrl}")
    private String actionUrl;

    @Value("${SMSConfig.username}")
    private String username;

    @Value("${SMSConfig.password}")
    private String password;

    /**
     * 发送短信
     *
     * @param smsEntity 发送短信所需要的信息
     * @return 发送的结果
     */
    @Override
    public SMSResult sendSMS(SMSEntity smsEntity) {
        SMSResult smsResult = new SMSResult();
        String randomCode = this.createRandomCode();
        Map<String, Object> parameters = new HashMap<>(16);
        parameters.put("u",username);
        parameters.put("p",password);
        parameters.put("m",smsEntity.getPhone());
        try {
            parameters.put("c",java.net.URLEncoder.encode(smsEntity.getSmsTypeEnum().getContent(), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            smsResult.setSuccess(false);
            smsResult.setMsg("短信内容有误");
            return smsResult;
        }
        try {
            HttpUtils.sendGet(actionUrl,parameters);
        } catch (Exception e) {
            smsResult.setSuccess(false);
            smsResult.setMsg("发送失败");
            return smsResult;
        }
        smsResult.setVerificationCode(randomCode);
        return smsResult;
    }

    /***
     * 随机生成6位验证码
     *
     * @return 6位随机数
     */
    private String createRandomCode() {
        return String.valueOf((int) ((Math.random() * 9 + 1) * 100000));
    }
}
