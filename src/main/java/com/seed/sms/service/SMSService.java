package com.seed.sms.service;

import com.seed.sms.entity.SMSEntity;
import com.seed.sms.entity.SMSResult;
import org.jeecgframework.core.common.service.CommonService;

/**
 * @author gchiaway
 *         日期: 2019-10-23
 *         时间: 11:41
 */
public interface SMSService extends CommonService {
    /**
     * 发送短信
     * @param smsEntity 发送短信所需要的信息
     * @return 发送的结果
     */
    SMSResult sendSMS(SMSEntity smsEntity);
}
