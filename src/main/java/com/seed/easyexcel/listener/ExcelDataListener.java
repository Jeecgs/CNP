package com.seed.easyexcel.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.fastjson.JSON;
import com.seed.easyexcel.entity.DataEntityExample;
import com.seed.easyexcel.entity.ExcelDataExample;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author gchiaway
 *         日期: 2019-10-24
 *         时间: 16:47
 */
public class ExcelDataListener extends AnalysisEventListener<ExcelDataExample> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExcelDataListener.class);

    /**
     * 这个是原始的excel数据list
     */
    private List<ExcelDataExample> list = new ArrayList<>();
    /**
     * 这个是处理过后的数据list
     */
    private List<DataEntityExample> dataEntityExampleList = new ArrayList<>();

    public List<ExcelDataExample> getList() {
        return list;
    }

    public void setList(List<ExcelDataExample> list) {
        this.list = list;
    }

    public List<DataEntityExample> getDataEntityExampleList() {
        return dataEntityExampleList;
    }

    public void setDataEntityExampleList(List<DataEntityExample> dataEntityExampleList) {
        this.dataEntityExampleList = dataEntityExampleList;
    }

    @Override
    public void invoke(ExcelDataExample data, AnalysisContext context) {
        LOGGER.info("解析到一条数据:{}", JSON.toJSONString(data));

        //可以在这里对excel里面的数据进行处理
        DataEntityExample dataEntityExample = new DataEntityExample();
        //属性名称如果能够一致，这里的复制就可以把需要的属性全部复制完成
        BeanUtils.copyProperties(data,dataEntityExample);
        //也可以对属性继续处理
        dataEntityExample.getSn();

        //添加到list中，
        list.add(data);
        dataEntityExampleList.add(dataEntityExample);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        LOGGER.info("所有数据解析完成！");
        LOGGER.info(JSON.toJSONString(list));
        LOGGER.info(JSON.toJSONString(dataEntityExampleList));

    }

}
