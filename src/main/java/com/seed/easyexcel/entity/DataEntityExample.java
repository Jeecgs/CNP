package com.seed.easyexcel.entity;

import lombok.Data;

/**
 * 数据实体类
 * @author gchiaway
 *         日期: 2019-10-24
 *         时间: 16:52
 */
@Data
public class DataEntityExample {
    /** 标识号*/
    private String sn;
}
