package com.seed.easyexcel.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * excel中的数据
 * @author gchiaway
 *         日期: 2019-10-24
 *         时间: 16:46
 */
@Data
public class ExcelDataExample {
    /** 标识号*/
    @ExcelProperty(value = "S/N", index = 0)
    private String sn;
}
