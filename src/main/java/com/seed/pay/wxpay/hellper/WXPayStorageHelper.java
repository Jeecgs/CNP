package com.seed.pay.wxpay.hellper;

import com.egzosn.pay.wx.api.WxPayConfigStorage;
import lombok.Data;

@Data
public class WXPayStorageHelper {
    /**
     * 支付配置
     */
    private WxPayConfigStorage wxPayConfigStorage;

}
