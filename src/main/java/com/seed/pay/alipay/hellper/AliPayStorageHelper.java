package com.seed.pay.alipay.hellper;

import com.egzosn.pay.ali.api.AliPayConfigStorage;
import com.egzosn.pay.ali.api.AliPayService;
import com.egzosn.pay.common.bean.CertStoreType;
import com.egzosn.pay.common.http.HttpConfigStorage;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Data
@Component
public class AliPayStorageHelper {
    //合作者id
    @Value("${myAliPayConfigStorage.pid}")
    private String pid;
    //应用id
    @Value("${myAliPayConfigStorage.appId}")
    private String appId;
    //支付宝公钥
    @Value("${myAliPayConfigStorage.keyPublic}")
    private String keyPublic;
    //应用私钥
    @Value("${myAliPayConfigStorage.keyPrivate}")
    private String keyPrivate;
    //异步回调地址
    @Value("${myAliPayConfigStorage.notifyUrl}")
    private String notifyUrl;
    //同步回调地址
    @Value("${myAliPayConfigStorage.returnUrl}")
    private String returnUrl;
    //签名方式
    @Value("${myAliPayConfigStorage.signType}")
    private String signType;
    //收款账号
    @Value("${myAliPayConfigStorage.seller}")
    private String seller;
    //utf-8
    @Value("${myAliPayConfigStorage.inputCharset}")
    private String inputCharset;
    //是否为测试账号，沙箱环境
    @Value("${myAliPayConfigStorage.test}")
    private Boolean test;

    //http代理地址
    @Value("${httpConfigStorage.httpProxyHost}")
    private String httpProxyHost;
    //代理端口
    @Value("${httpConfigStorage.httpProxyPort}")
    private Integer httpProxyPort;
    //代理用户名
    @Value("${httpConfigStorage.authUsername}")
    private String authUsername;
    //代理密码
    @Value("${httpConfigStorage.authPassword}")
    private String authPassword;
    //网路代理配置 根据需求进行设置
    //是否设置ssl证书
    @Value("${httpConfigStorage.isSSL}")
    private Boolean isSSL;
    //网络请求ssl证书 根据需求进行设置
    //设置ssl证书路径 跟着setCertStoreType 进行对应 证书文件流，证书字符串信息或证书绝对地址
    @Value("${httpConfigStorage.keystore}")
    private String keystore;
    //设置ssl证书对应的密码
    @Value("${httpConfigStorage.storePassword}")
    private String storePassword;
    //设置ssl证书对应的存储方式
    @Value("${httpConfigStorage.certStoreType}")
    private String certStoreType;
    //最大连接数
    @Value("${httpConfigStorage.maxTotal}")
    private Integer maxTotal;
    //默认的每个路由的最大连接数
    @Value("${httpConfigStorage.defaultMaxPerRoute}")
    private Integer defaultMaxPerRoute;

    //支付配置
    private AliPayConfigStorage aliPayConfigStorage;
    //网络请求配置
    private HttpConfigStorage httpConfigStorage;
    //支付服务
    private AliPayService service;

    @PostConstruct
    private void init(){
        //初始化支付配置
        this.aliPayConfigStorage=new AliPayConfigStorage();
        this.aliPayConfigStorage.setPid(this.pid);
        this.aliPayConfigStorage.setAppid(this.appId);
        this.aliPayConfigStorage.setKeyPublic(this.keyPublic);
        this.aliPayConfigStorage.setKeyPrivate(this.keyPrivate);
        this.aliPayConfigStorage.setNotifyUrl(this.notifyUrl);
        this.aliPayConfigStorage.setReturnUrl(this.returnUrl);
        this.aliPayConfigStorage.setSignType(this.signType);
        this.aliPayConfigStorage.setSeller(this.seller);
        this.aliPayConfigStorage.setInputCharset(this.inputCharset);
        this.aliPayConfigStorage.setTest(this.test);
        //初始化支付服务
        this.service=new AliPayService(this.aliPayConfigStorage);
        //初始化网络请求配置
        this.httpConfigStorage = new HttpConfigStorage();
        httpConfigStorage.setHttpProxyHost(this.httpProxyHost);
        httpConfigStorage.setHttpProxyPort(this.httpProxyPort);
        httpConfigStorage.setAuthUsername(this.pid);
        httpConfigStorage.setAuthPassword(this.authPassword);
        if(isSSL) {
            httpConfigStorage.setKeystore(this.keystore);
            httpConfigStorage.setStorePassword(this.storePassword);
            switch (this.certStoreType){
                case "PATH":
                    httpConfigStorage.setCertStoreType(CertStoreType.PATH);break;
                case "STR":
                    httpConfigStorage.setCertStoreType(CertStoreType.STR);break;
                case "INPUT_STREAM":
                    httpConfigStorage.setCertStoreType(CertStoreType.INPUT_STREAM);break;
            }
        }
        httpConfigStorage.setMaxTotal(this.maxTotal);
        httpConfigStorage.setDefaultMaxPerRoute(this.defaultMaxPerRoute);
        //初始化网络请求配置
        this.service.setRequestTemplateConfigStorage(this.httpConfigStorage);
    }
}
