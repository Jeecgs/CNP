package com.seed.pay.alipay.controller;

import com.alibaba.fastjson.JSONObject;
import com.egzosn.pay.ali.bean.AliTransferType;
import com.egzosn.pay.common.bean.RefundOrder;
import com.egzosn.pay.common.bean.TransferOrder;
import com.seed.pay.alipay.service.AliPayHelperService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.jeecgframework.core.common.controller.BaseController;
import org.jeecgframework.core.common.model.json.AjaxJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;

@RestController
@Api(value = "AliPayController", description = "支付宝管理", tags = "AliPayController")
@RequestMapping("/app/v1/AliPayController")
public class AliPayController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(AliPayController.class);
    @Autowired
    private AliPayHelperService aliPayHelperService;

    @RequestMapping(value = "aliPayCallback", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "支付回调处理")
    public String aliPayCallback(HttpServletRequest request, HttpServletResponse response) throws IOException {
        logger.info("[api-v1]接口,支付回调处理 alipayCallback");
        Map<String, Object> params = aliPayHelperService.getParameter2Map(request.getParameterMap(), request.getInputStream());
        if (params.size() > 0) {
            String[] argArray = {params.get("sign").toString(), params.get("trade_status").toString(), params.toString()};
            logger.info("支付宝回调, sign:{},trade_status:{},参数:{}", argArray);
            if (aliPayHelperService.verify(params)) {
                //TODO:如果有支付功能，必须完善此方法
                //********************************进行支付成功操作
            } else {
                //********************************进行支付失败操作
            }
        }
        //返回字符串success，会让订单后期发送改变不再通知该回调函数
        //如果不返回，则每个订单会调用8次
        return "success";
    }

    @RequestMapping(value = "orderc", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "订单操作")
    public AjaxJson orderOperation(@RequestBody JSONObject object) {
        logger.info("[api-v1]接口,订单操作 orderOperation");
        AjaxJson result = new AjaxJson();
        String msg = "";
        //支付宝单号
        String trade_no = "";
        //我方系统单号
        String out_trade_no = "";
        //操作 0支付订单查询 1交易关闭 2交易撤销
        String operation = "";
        Map<String, Object> orderInfos = null;
        //传参效验
        if (StringUtils.isEmpty(msg)) {
            trade_no = object.getString("tradeNo");
            out_trade_no = object.getString("outTradeNo");
            operation = object.getString("operation");
            if (StringUtils.isEmpty(trade_no) || StringUtils.isEmpty(out_trade_no)
                    || StringUtils.isEmpty(operation)) {
                msg = "请求错误:参数缺失.";
            }
        }
        //获取支付宝支付订单查询返回的官方信息
        if (StringUtils.isEmpty(msg)) {
            switch (operation) {
                //支付订单查询
                case "0":
                    orderInfos = aliPayHelperService.orderQuery(trade_no, out_trade_no);
                    break;
                //交易关闭
                case "1":
                    orderInfos = aliPayHelperService.orderClosed(trade_no, out_trade_no);
                    break;
                //交易撤销
                case "2":
                    orderInfos = aliPayHelperService.orderCancel(trade_no, out_trade_no);
                    break;
                default:
                    msg = "请求异常:无此操作.";
            }
            if (StringUtils.isEmpty(msg)) {
                if (orderInfos == null || orderInfos.size() == 0) {
                    msg = "阿里服务返回异常:无返回结果.";
                } else {
                    //支付宝支付订单查询返回的官方信息，可以注释掉
                    result.setAttributes(orderInfos);
                }
            }
        }
        //业务操作
        if (StringUtils.isEmpty(msg)) {
            //TODO:如果有支付功能，必须完善此方法
            //************对比数据库订单操作
            //还要对应订单的操作分别对数据库进行操作
            //这里放数据库查到的东西，和前端约定好的东西
            //result.setObj();
            //****************************************
        }
        //最终返回结果处理
        if (StringUtils.isEmpty(msg)) {
            result.setSuccess(true);
        } else {
            result.setMsg(msg);
            result.setSuccess(false);
        }
        return result;
    }

    @RequestMapping(value = "orderRefundOperation", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "退款操作")
    public AjaxJson orderRefundOperation(@RequestBody JSONObject object) {
        logger.info("[api-v1]接口,退款操作 orderOperation");
        AjaxJson result = new AjaxJson();
        String msg = "";
        //支付宝单号
        String tradeNo = "";
        //我方系统单号
        String outTradeNo = "";
        //退款单号  非必填， 根据业务需求而定，可用于多次退款
        String refundNo = "";
        //退款金额
        BigDecimal refundAmount = null;
        //订单总金额
        BigDecimal totalAmount = null;
        //操作类型 0 退款 1 退款查询
        String operation = "";
        Map<String, Object> orderInfos = null;
        RefundOrder order = null;
        //传参效验
        if (StringUtils.isEmpty(msg)) {
            tradeNo = object.getString("tradeNo");
            outTradeNo = object.getString("outTradeNo");
            operation = object.getString("operation");
            refundNo = object.getString("refundNo");
            refundAmount = new BigDecimal(object.getString("refundAmount"));
            totalAmount = new BigDecimal(object.getString("totalAmount"));
            if (StringUtils.isEmpty(tradeNo) || StringUtils.isEmpty(outTradeNo) || StringUtils.isEmpty(operation)) {
                msg = "请求错误:参数缺失.";
            } else {
                order = new RefundOrder(tradeNo, outTradeNo, refundAmount, totalAmount);
                if (StringUtils.isNotEmpty(refundNo)) {
                    order.setRefundNo(refundNo);
                }
            }
        }
        //获取支付宝退款订单查询返回的官方信息
        if (StringUtils.isEmpty(msg)) {
            switch (operation) {
                //退款申请
                case "0":
                    orderInfos = aliPayHelperService.orderRefund(order);
                    break;
                //退款查询
                case "1":
                    orderInfos = aliPayHelperService.orderRefundQuery(order);
                    break;
                default:
                    msg = "请求异常:无此操作.";
            }
            if (StringUtils.isEmpty(msg)) {
                if (orderInfos == null || orderInfos.size() == 0) {
                    msg = "阿里服务返回异常:无返回结果.";
                } else {
                    //支付宝支付订单查询返回的官方信息，可以注释掉
                    result.setAttributes(orderInfos);
                }
            }
        }
        //业务操作
        if (StringUtils.isEmpty(msg)) {
            //TODO:如果有退款功能，必须完善此方法
            //************对比数据库退款操作
            //还要对应订单的操作分别对数据库进行操作
            //这里放数据库查到的东西，和前端约定好的东西
            //result.setObj();
            //****************************************
        }
        //最终返回结果处理
        if (StringUtils.isEmpty(msg)) {
            result.setSuccess(true);
        } else {
            result.setMsg(msg);
            result.setSuccess(false);
        }
        return result;
    }
    @RequestMapping(value = "orderTransfer", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "转账")
    public AjaxJson orderTransfer(@RequestBody JSONObject object) {
        logger.info("[api-v1]接口,退款操作 orderOperation");
        AjaxJson result = new AjaxJson();
        String msg="";
        //商户转账订单号
        String outNo="";
        //收款方账户,支付宝登录号，支持邮箱和手机号格式
        String payeeAccount="";
        //转账金额
        BigDecimal amount=null;
        //付款方姓名, 非必填
        String payerName="";
        //收款方真实姓名, 非必填
        String payeeName="";
        //转账备注, 非必填
        String remark="";
        TransferOrder order=null;
        Map<String, Object> orderInfos = null;
        //传参效验
        if (StringUtils.isEmpty(msg)) {
            outNo = object.getString("outNo");
            payeeAccount = object.getString("payeeAccount");
            payerName = object.getString("payerName");
            payeeName = object.getString("payeeName");
            remark=object.getString("remark");
            amount = new BigDecimal(object.getString("amount"));
            if (StringUtils.isEmpty(outNo) || StringUtils.isEmpty(payeeAccount) || amount.compareTo(new BigDecimal(0))==0) {
                msg = "请求错误:参数缺失.";
            } else {
                order = new TransferOrder();
                order.setOutNo(outNo);
                order.setPayeeAccount(payeeAccount);
                order.setAmount(amount);
                order.setPayerName(payerName);
                order.setPayeeName(payeeName);
                order.setRemark(remark);
                //收款方账户类型 ,默认值 ALIPAY_LOGONID：支付宝登录号，支持邮箱和手机号格式。
                order.setTransferType(AliTransferType.ALIPAY_LOGONID);
            }
        }
        //获取支付宝退款订单查询返回的官方信息
        if (StringUtils.isEmpty(msg)) {
            orderInfos = aliPayHelperService.orderTransfer(order);
            if (orderInfos == null || orderInfos.size() == 0) {
                msg = "阿里服务返回异常:无返回结果.";
            } else {
                //支付宝支付订单查询返回的官方信息，可以注释掉
                result.setAttributes(orderInfos);
            }
        }
        //业务操作
        if (StringUtils.isEmpty(msg)) {
            //TODO:如果有转账功能，必须完善此方法
            //************对比数据库转账操作
            //还要对应订单的操作分别对数据库进行操作
            //这里放数据库查到的东西，和前端约定好的东西
            //result.setObj();
            //****************************************
        }
        //最终返回结果处理
        if (StringUtils.isEmpty(msg)) {
            result.setSuccess(true);
        } else {
            result.setMsg(msg);
            result.setSuccess(false);
        }
        return result;
    }
}
