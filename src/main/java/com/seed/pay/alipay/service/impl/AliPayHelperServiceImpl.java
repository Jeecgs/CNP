package com.seed.pay.alipay.service.impl;

import com.egzosn.pay.ali.bean.AliTransactionType;
import com.egzosn.pay.common.bean.MethodType;
import com.egzosn.pay.common.bean.PayOrder;
import com.egzosn.pay.common.bean.RefundOrder;
import com.egzosn.pay.common.bean.TransferOrder;
import com.seed.pay.alipay.hellper.AliPayStorageHelper;
import com.seed.pay.alipay.hellper.ServiceResult;
import com.seed.pay.alipay.service.AliPayHelperService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.util.Map;

/**
 * @author kby
 * 日期: 2019-10-24
 * 时间: 10:18
 */
@Service("AliPayHelperService")
@Transactional(rollbackFor = Exception.class)
public class AliPayHelperServiceImpl  implements AliPayHelperService {
    @Autowired
    public AliPayStorageHelper aliPayStorageHelper;

    /**
     *  转账查询
     *
     * @param outNo 商户转账订单号
     * @param tradeNo 支付平台转账订单号
     * @return
     */
    @Override
    public Map orderTransferQuery(String outNo, String tradeNo) {
        return aliPayStorageHelper.getService().transferQuery(outNo,tradeNo);
    }

    /**
     * 转账
     *
     * @param order 转账表单
     * @return
     */
    @Override
    public Map orderTransfer(TransferOrder order) {
        return null;
    }

    /**
     * 退款查询
     *
     * @param order 申请单
     * @return 退款详情
     */
    @Override
    public Map orderRefundQuery(RefundOrder order) {
        return aliPayStorageHelper.getService().refundquery(order);
    }

    /**
     * 退款申请
     *
     * @param order 申请单
     * @return 退款详情
     */
    @Override
    public Map orderRefund(RefundOrder order) {
        return aliPayStorageHelper.getService().refund(order);
    }

    /**
     * 交易撤销
     *
     * @param trade_no     支付宝单号
     * @param out_trade_no 我方系统单号
     * @return 订单详情
     */
    @Override
    public Map orderCancel(String trade_no, String out_trade_no) {
        return aliPayStorageHelper.getService().cancel(trade_no,out_trade_no);
    }

    /**
     * 交易关闭
     *
     * @param trade_no     支付宝单号
     * @param out_trade_no 我方系统单号
     * @return 订单详情
     */
    @Override
    public Map orderClosed(String trade_no, String out_trade_no) {
        return aliPayStorageHelper.getService().close(trade_no,out_trade_no);
    }

    /**
     * 支付订单查询
     *
     * @param trade_no     支付宝单号
     * @param out_trade_no 我方系统单号
     * @return 订单详情
     */
    @Override
    public Map orderQuery(String trade_no, String out_trade_no) {
        return aliPayStorageHelper.getService().query(trade_no,out_trade_no);
    }

    /**
     * 支付宝支付工程
     * @param payOrder 订单
     * @param type 订单类型
     * @return 返回ServiceResult
     */
    @Override
    public ServiceResult AliPayFactory(PayOrder payOrder, AliTransactionType type) {
        ServiceResult result = null;
        Object obj = null;
        try {
            switch (type) {
                case SWEEPPAY:
                    //扫描支付 BufferedImage
                    obj = getQrPayImageByPayOrder(payOrder);
                    break;
                case APP:
                    //APP支付 Map
                    obj = getAppOrderInfoByPayOrder(payOrder);
                    break;
                case DIRECT:
                case WAP:
                    //PC网页支付 String//WAO支付 String
                    obj =getDirectHtmlByPayOrder(payOrder,type);
                case WAVE_CODE:
                case BAR_CODE:
                    //声波付
                    //条码付
                    //payOrder的AuthCode属性必须有值
                    obj =getParamsByPayOrder(payOrder,type);
                default:
                    result = null;
            }
        } catch (Exception e) {
            result.setMsg(e.getMessage());
            result.setSuccess(false);
        }
        if (obj == null) {
            result.setSuccess(false);
            result.setMsg("支付失败,获取不到支付宝系统信息.");
        }
        result.setData(obj);
        return result;
    }
    /**
     * 支付宝回调函数信息拆包
     * @param parameterMap
     * @param is
     * @return 信息map
     */
    @Override
    public Map<String, Object> getParameter2Map(Map<String, String[]> parameterMap, InputStream is){
        return aliPayStorageHelper.getService().getParameter2Map(parameterMap,is);
    }

    /**
     * 回调校验
     *
     * @param params 回调回来的参数集
     * @return 签名校验 true通过
     */
    @Override
    public boolean verify(Map<String, Object> params) {
        return aliPayStorageHelper.getService().verify(params);
    }


    /**
     * 根据订单生成扫付码
     *
     * @param payOrder 支付订单
     * @return 返回扫付码图片
     */
    private BufferedImage getQrPayImageByPayOrder(PayOrder payOrder) {
        payOrder.setTransactionType(AliTransactionType.SWEEPPAY);
        //获取扫码付的二维码
        BufferedImage qrPayImage = aliPayStorageHelper.getService().genQrPay(payOrder);
        return qrPayImage;
    }

    /**
     * 根据订单生成app支付所需的信息组
     *
     * @param payOrder 支付订单
     * @return 生成app支付所需的信息组 直接给app端就可使用
     */
    private Map getAppOrderInfoByPayOrder(PayOrder payOrder) {
        payOrder.setTransactionType(AliTransactionType.APP);
        //获取APP支付所需的信息组，直接给app端就可使用
        Map appOrderInfo = aliPayStorageHelper.getService().orderInfo(payOrder);
        return appOrderInfo;
    }

    /**
     * 根据订单生成app支付所需的信息组
     * @param payOrder 支付订单
     * @param type 订单类型 WAP支付 和 DIRECT即时到帐 PC网页支付
     * @return 返回支付页面
     */
    private String getDirectHtmlByPayOrder(PayOrder payOrder, AliTransactionType type) {
        if (type == AliTransactionType.WAP) {
            //WAP支付
            payOrder.setTransactionType(AliTransactionType.WAP);
        } else if (type == AliTransactionType.DIRECT) {
            // 即时到帐 PC网页支付
            payOrder.setTransactionType(AliTransactionType.DIRECT);
        } else {
            return null;
        }
        //获取支付所需的信息
        Map directOrderInfo = aliPayStorageHelper.getService().orderInfo(payOrder);
        //获取表单提交对应的字符串，将其序列化到页面即可,
        String directHtml = aliPayStorageHelper.getService().buildRequest(directOrderInfo, MethodType.POST);
        return directHtml;
    }

    /**
     *
     * @param payOrder 支付订单
     * @param type 订单类型 声波付和条码付
     * @return 支付结果
     */
    private Map getParamsByPayOrder(PayOrder payOrder, AliTransactionType type){
        if (type == AliTransactionType.WAVE_CODE) {
            //声波付
            payOrder.setTransactionType(AliTransactionType.WAVE_CODE);
        } else if (type == AliTransactionType.BAR_CODE) {
            //条码付
            payOrder.setTransactionType(AliTransactionType.BAR_CODE);
        } else {
            return null;
        }
        if(StringUtils.isEmpty(payOrder.getAuthCode())){
            return null;
        }
        // 支付结果
        Map params = aliPayStorageHelper.getService().microPay(payOrder);
        return params;
    }
}
