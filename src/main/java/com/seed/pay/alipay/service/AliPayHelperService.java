package com.seed.pay.alipay.service;

import com.egzosn.pay.ali.bean.AliTransactionType;
import com.egzosn.pay.common.bean.PayOrder;
import com.egzosn.pay.common.bean.RefundOrder;
import com.egzosn.pay.common.bean.TransferOrder;
import com.seed.pay.alipay.hellper.ServiceResult;

import java.io.InputStream;
import java.util.Map;

/**
 * @author gchiaway
 * 日期: 2019-10-23
 * 时间: 10:16
 */
public interface AliPayHelperService {

    /**
     *  转账查询
     * @param outNo 商户转账订单号
     * @param tradeNo 支付平台转账订单号
     * @return
     */
    Map orderTransferQuery(String outNo, String tradeNo);
    /**
     * 转账
     * @param order 转账表单
     * @return
     */
    Map orderTransfer(TransferOrder order);
    /**
     * 退款查询
     * @param order 申请单
     * @return 退款详情
     */
    Map orderRefundQuery(RefundOrder order);
    /**
     * 退款申请
     * @param order 申请单
     * @return 退款详情
     */
    Map orderRefund(RefundOrder order);
    /**
     * 交易撤销
     * @param trade_no 支付宝单号
     * @param out_trade_no 我方系统单号
     * @return 订单详情
     */
    Map orderCancel(String trade_no, String out_trade_no);
    /**
     * 交易关闭
     * @param trade_no 支付宝单号
     * @param out_trade_no 我方系统单号
     * @return 订单详情
     */
    Map orderClosed(String trade_no, String out_trade_no);
    /**
     * 支付订单查询
     * @param trade_no 支付宝单号
     * @param out_trade_no 我方系统单号
     * @return 订单详情
     */
    Map orderQuery(String trade_no, String out_trade_no);
    /**
     * 支付宝支付工程
     *
     * @param payOrder 订单
     * @param type     订单类型
     * @return 返回ServiceResult
     */
    ServiceResult AliPayFactory(PayOrder payOrder, AliTransactionType type);

    /**
     * 支付宝回调函数信息拆包
     *
     * @param parameterMap
     * @param is
     * @return 信息map
     */
    Map<String, Object> getParameter2Map(Map<String, String[]> parameterMap, InputStream is);

    /**
     * 回调校验
     *
     * @param params 回调回来的参数集
     * @return 签名校验 true通过
     */
    boolean verify(Map<String, Object> params);
}
