package com.seed.mail.util;

import com.seed.mail.entity.EmailSimpleEntity;
import lombok.Data;
import org.simplejavamail.email.Email;
import org.simplejavamail.email.EmailBuilder;
import org.simplejavamail.mailer.Mailer;
import org.simplejavamail.mailer.MailerBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author gchiaway
 *         日期: 2019-09-19
 *         时间: 16:28
 */
@Data
@Component(value = "mailHelper")
public class MailHelper {

    /**
     * 邮件发送者名称
     */
    @Value("${mailHelper.mailFrom}")
    private String mailFrom;

    /**
     * 邮件发送地址
     */
    @Value("${mailHelper.mailFromAddress}")
    private String mailFromAddress;

    /**
     * 邮件发送授权码
     */
    @Value("${mailHelper.mailFromPassword}")
    private String mailFromPassword;

    /**
     * 邮件发送服务商类型（161、qq等）
     */
    @Value("${mailHelper.mailType}")
    private String mailType;

    /**
     * 邮件发送者对象
     */
    private Mailer mailer;

    /**
     * 初始化邮件发送者对象
     */
    @PostConstruct
    private void initMailer() {
        if ("163".equals(mailType)) {
            this.mailer = MailerBuilder
                    .withSMTPServer("smtp.163.com", 25, mailFromAddress, mailFromPassword)
                    .buildMailer();
        } else {
            if ("qq".equals(mailType) || "QQ".equals(mailType)) {
                this.mailer = MailerBuilder
                        .withSMTPServer("smtp.qq.com", 465, mailFromAddress, mailFromPassword)
                        .buildMailer();
            }
        }
    }

    /**
     * 发送邮件
     *
     * @param emailSimpleEntity 简易邮件对象
     * @return 发送成功（true）/发送失败（false）
     */
    public boolean sendMail(EmailSimpleEntity emailSimpleEntity) {

        Email email = EmailBuilder.startingBlank()
                .to(emailSimpleEntity.getMailTo(), emailSimpleEntity.getMailToAddress())
                .withSubject(emailSimpleEntity.getMailTitle())
                .appendTextHTML(emailSimpleEntity.getMailText())
                .from(mailFrom, mailFromAddress)
                .buildEmail();

        if (null == mailer) {
            //构建发送对象失败
            return false;
        }

        if (mailer.validate(email)) {
            mailer.sendMail(email);
            return true;
        } else {
            //邮件校验不通过
            return false;
        }
    }

}
