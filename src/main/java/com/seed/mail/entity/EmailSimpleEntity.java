package com.seed.mail.entity;

import lombok.Data;

/**
 * @author gchiaway
 *         日期: 2019-10-24
 *         时间: 16:12
 */
@Data
public class EmailSimpleEntity {
    private String mailTo;
    private String mailToAddress;
    private String mailTitle;
    private String mailText;

    public EmailSimpleEntity() {
    }

    /**
     * 构造简易邮件对象
     * @param mailTo        收件人
     * @param mailToAddress 收件人地址
     * @param mailTitle     邮件标题
     * @param mailText      邮件内容
     */
    public EmailSimpleEntity(String mailTo, String mailToAddress, String mailTitle, String mailText) {
        this.mailTo = mailTo;
        this.mailToAddress = mailToAddress;
        this.mailTitle = mailTitle;
        this.mailText = mailText;
    }
}
