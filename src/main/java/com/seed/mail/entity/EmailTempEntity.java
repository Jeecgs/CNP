package com.seed.mail.entity;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author gchiaway
 *         日期: 2019-09-30
 *         时间: 18:40
 */
@Data
public class EmailTempEntity {
    /**
     * 邮件接收者名称
     */
    private String mailTo;
    /**
     * 邮件接收者地址
     */
    private String mailToAddress;
    /**
     * 邮件类型
     */
    private EmailEnum emailEnum;
    /**
     * 拓展参数
     */
    private Map<String,Object> exParam = new HashMap<>(16);

    public EmailTempEntity(String mailTo, String mailToAddress, EmailEnum emailEnum) {
        this.mailTo = mailTo;
        this.mailToAddress = mailToAddress;
        this.emailEnum = emailEnum;
    }
    public EmailTempEntity(String mailTo, String mailToAddress, Map<String, Object> exParam, EmailEnum emailEnum) {
        this.mailTo = mailTo;
        this.mailToAddress = mailToAddress;
        this.exParam = exParam;
        this.emailEnum = emailEnum;
    }
}
