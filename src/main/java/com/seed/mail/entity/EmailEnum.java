package com.seed.mail.entity;

/**
 * @author gchiaway
 *         日期: 2019-09-30
 *         时间: 17:56
 */
public enum EmailEnum {

    /**
     * 找回密码
     */
    PASSWORD_RESET("云平台账号找回","export/mail/password_reset.ftl"),
    ;

    /**
     * 邮件名称
     */
    private String name;

    /**
     * 邮件模板名称
     */
    private String ftlName;

    EmailEnum(String name, String ftlName) {
        this.name = name;
        this.ftlName = ftlName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFtlName() {
        return ftlName;
    }

    public void setFtlName(String ftlName) {
        this.ftlName = ftlName;
    }
}
