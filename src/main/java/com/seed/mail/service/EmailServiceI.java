package com.seed.mail.service;

import com.seed.mail.entity.EmailSimpleEntity;
import com.seed.mail.entity.EmailTempEntity;
import org.jeecgframework.core.common.service.CommonService;

/**
 * @author gchiaway
 *         日期: 2019-09-30
 *         时间: 17:54
 */
public interface EmailServiceI extends CommonService {

    /**
     * 发送邮件
     *
     * @param emailTempEntity 果园邮件
     * @return 发送成功（true）/发送失败（false）
     * @throws Exception
     */
    boolean sendMail(EmailTempEntity emailTempEntity) throws Exception;

    /**
     * 发送邮件
     * @param emailSimpleEntity 简易邮件对象
     * @return
     * @throws Exception
     */
    boolean sendMail(EmailSimpleEntity emailSimpleEntity) throws Exception;


}
