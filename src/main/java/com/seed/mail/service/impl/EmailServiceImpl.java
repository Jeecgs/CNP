package com.seed.mail.service.impl;

import com.seed.mail.entity.EmailSimpleEntity;
import com.seed.mail.entity.EmailTempEntity;
import com.seed.mail.service.EmailServiceI;
import com.seed.mail.util.MailHelper;
import org.jeecgframework.core.common.service.impl.CommonServiceImpl;
import org.jeecgframework.core.online.util.FreemarkerHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author gchiaway
 *         日期: 2019-09-30
 *         时间: 17:55
 */
@Service("emailService")
@Transactional(rollbackFor = Exception.class)
public class EmailServiceImpl extends CommonServiceImpl implements EmailServiceI {

    @Autowired
    private MailHelper mailHelper;

    @Value("${mailHelper.actionUrl}")
    private String actionUrl;


    /**
     * 发送邮件
     *
     * @param emailTempEntity 邮件对象
     * @return 发送成功（true）/发送失败（false）
     * @throws Exception
     */
    @Override
    public boolean sendMail(EmailTempEntity emailTempEntity) throws Exception {
        String mailTitle = emailTempEntity.getEmailEnum().getName();
        String mailContent = this.createMailContent(emailTempEntity);
        if (StringUtils.isEmpty(mailContent)) {
            return false;
        }
        EmailSimpleEntity emailSimpleEntity = new EmailSimpleEntity(emailTempEntity.getMailTo(), emailTempEntity.getMailToAddress(), mailTitle, mailContent);
        return this.sendMail(emailSimpleEntity);
    }

    /**
     * 发送邮件
     *
     * @param emailSimpleEntity 简易邮件对象
     * @return 发送成功（true）/发送失败（false）
     * @throws Exception
     */
    @Override
    public boolean sendMail(EmailSimpleEntity emailSimpleEntity) throws Exception {
        return mailHelper.sendMail(emailSimpleEntity);
    }

    /**
     * 构建邮件内容
     *
     * @param emailTempEntity 邮件对象
     * @return 构建内容结果
     */
    private String createMailContent(EmailTempEntity emailTempEntity) {
        String mailContent;
        switch (emailTempEntity.getEmailEnum()) {
            case PASSWORD_RESET: {
                mailContent = this.createPasswordReset(emailTempEntity);
                break;
            }
            default: {
                return null;
            }
        }
        return mailContent;
    }

    /**
     * 构建找回密码内容
     *
     * @param emailTempEntity 邮件对象
     * @return 构建内容结果
     */
    private String createPasswordReset(EmailTempEntity emailTempEntity) {
        String url = actionUrl + "/loginController.do?goResetPwd&key=" + emailTempEntity.getExParam().get("key");

        //配置邮件模板参数
        Map<String, Object> mailConfig = new HashMap<>(16);
        mailConfig.put("title", emailTempEntity.getEmailEnum().getName());
        mailConfig.put("content", "正在为你找回平台账号,请点击链接进行账号找回操作:");
        mailConfig.put("commentUrl", url);
        mailConfig.put("commentUrlText", "账号找回链接");

        return new FreemarkerHelper().parseTemplate(emailTempEntity.getEmailEnum().getFtlName(), mailConfig);
    }


}
