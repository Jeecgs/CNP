package com.seed.util;

import com.egzosn.pay.common.util.MatrixToImageWriter;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.io.File;
import java.io.IOException;

/**
 * @author gchiaway
 *         日期: 2019-10-08
 *         时间: 12:27
 */
public class QRCodeUtil {

    private static int width = 300;
    private static int height = 300;

    public static void generateQRCodeImage(String text, String filePath) throws WriterException, IOException {

        QRCodeWriter qrCodeWriter = new QRCodeWriter();

        BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, QRCodeUtil.width, QRCodeUtil.height);

        MatrixToImageWriter.writeToFile(bitMatrix, "PNG", new File(filePath));
    }

    public static void generateQRCodeImage(String text, int width, int height, String filePath) throws WriterException, IOException {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();

        BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);

        MatrixToImageWriter.writeToFile(bitMatrix, "PNG", new File(filePath));
    }
}
