package org.jeecgframework.jwt.service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.jeecgframework.jwt.def.JwtConstants;
import org.jeecgframework.jwt.model.TokenModel;
import org.jeecgframework.web.system.pojo.base.TSUser;
import org.jeecgframework.web.system.service.CacheServiceI;
import org.jeecgframework.web.system.service.impl.EhcacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author gchiaway
 *         日期: 2019-05-04
 *         时间: 16:16
 */
//@Component
public class EhcacheTokenManager implements TokenManager {

    @Autowired
    private EhcacheService ehcacheService;

    /**
     * 创建一个token关联上指定用户
     *
     * @param user@return 生成的token
     */
    @Override
    public String createToken(TSUser user) {
        //使用uuid作为源token
        String token = Jwts.builder().setId(user.getUserName()).setSubject(user.getUserName()).setIssuedAt(new Date()).signWith(SignatureAlgorithm.HS256, JwtConstants.JWT_SECRET).compact();
        if (ehcacheService.manager.getCache(CacheServiceI.TOKEN_CACHE) == null) {
            ehcacheService.manager.addCache(CacheServiceI.TOKEN_CACHE);
        }
        ehcacheService.put(CacheServiceI.TOKEN_CACHE,user.getUserName(),token);
        return token;
    }

    /**
     * 检查token是否有效
     *
     * @param model token
     * @return 是否有效
     */
    @Override
    public boolean checkToken(TokenModel model) {
        if (model == null) {
            return false;
        }
        String token = (String) ehcacheService.get(CacheServiceI.TOKEN_CACHE,model.getUsername());
        if (token == null || !token.equals(model.getToken())) {
            return false;
        }
        ehcacheService.put(CacheServiceI.TOKEN_CACHE,model.getUsername(),token);
        return true;
    }

    /**
     * 从字符串中解析token
     *
     * @param token
     * @param userid
     * @return
     */
    @Override
    public TokenModel getToken(String token, String userid) {
        return new TokenModel(userid, token);
    }

    /**
     * 清除token
     *
     * @param username 登录用户账号
     */
    @Override
    public void deleteToken(String username) {
        ehcacheService.remove(CacheServiceI.TOKEN_CACHE,username);
    }
}
