package org.jeecgframework.core.common.model.json;

import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * $.ajax后需要接受的JSON
 *
 * @author
 */
public class AjaxJson {

    /**
     * 是否成功
     */
    private boolean success = true;
    /**
     * 提示信息
     */
    private String msg = "操作成功";
    /**
     * 其他信息
     */
    private Object obj = null;
    /**
     * 其他参数
     */
    private Map<String, Object> attributes = new HashMap<>(16);

    /**
     * 转换为json字符串
     *
     * @return 转换后的json字符串
     */
    public String getJsonStr() {
        JSONObject obj = new JSONObject();
        obj.put("success", this.isSuccess());
        obj.put("msg", this.getMsg());
        obj.put("obj", this.obj);
        obj.put("attributes", this.attributes);
        return obj.toJSONString();
    }

    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, Object> attributes) {
        this.attributes = attributes;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public AjaxJson filed(String msg){
        this.setSuccess(false);
        this.setMsg(msg);
        return this;
    }
    public AjaxJson success(Object obj){
        this.setSuccess(true);
        this.setObj(obj);
        return this;
    }
    public AjaxJson success(String msg){
        this.setSuccess(true);
        this.setMsg(msg);
        return this;
    }
}
