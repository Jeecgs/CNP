﻿
# 云笔记拟API  V1.0



## 版本信息

| 版本号 | **操作时间** | 操作状态 | 操作内容|操作人 | 审核 |校对 |
|  --- |  ---  |  ---   |  ---  |  --- |  ---   |  ---  |
| V1.0  | 2020年4月2日  | 创建 |创建此文档|  |   |   |

## 修改信息
| 序号 |  操作时间 | 修改人 |  修改内容 |修改原因|
| --- | --- | --- | --- | --- |
|1  | 2020年4月2日    |    | 创建此文档 |  |

## 接口信息

**返回格式说明：**
> status:  状态码
> msg: 状态信息
> data: 返回数据 return
>注： 以下接口示例返回结果参数均为date内容，并包含列表

**格式说明：**

>1. url：请求url
>2. method：请求方式
>3. param：请求参数
>  |  参数名 |  类型 |  必填 |  默认 |  说明 | 
>	 | --- | --- | --- | --- | --- | --- |
> 	| a|  Integer|  N  |  10  |  |
>4. return：返回结果
>  |  参数名 |  类型 |  必填 |  默认 |  说明 | 
>	 | --- | --- | --- | --- | --- | --- |
> 	 | b |  string  |  Y  |     |   |

*实例，添加接口请复制*
 1. url:  
 2. method:  
 3. param:  
 
     |  参数名 |  类型 |  必填 |  默认 |  说明 | 
	 | --- | --- | --- | --- | --- | 
	 |   |   |     |     |  |
 4. return: 
 
	|  参数名 |  类型 |  必填 |  默认 |  说明 | 
	| --- | --- | --- | --- | --- | 
	|   |    |    |     |   |


### 笔记管理
#### 根据笔记本ID获取笔记（getPersonalNoteByNoteId）

 1. url: 
 2. method: 
 3. param: 
 
    |  参数名 |  类型 |  必填 |  默认 |  说明 | 
    | --- | --- | --- | --- | --- |
    | nodeId |  String |  Y  |    | 用户打开笔记本获取笔记 | 

 4. return: 
 
    |  参数名 |  类型 |  必填 |  默认 |  说明 | 
    | --- | --- | --- | --- | --- | 
    | id  |  String  |  Y  |     |  笔记ID  |
    | noteSerial_number  |  String  |  Y  |     | 笔记流水号  |
    | createName |  string  |  Y  |     |  笔记创建人  |
    | parentId   |  string  |  N  |     |  父本ID |
    | parentName   |  string  |  N  |     |  父本名称 |
    | noteName  |  string  |  Y  |     |  笔记名称  |
    | noteContent  |  string  |  Y  |     |  笔记内容  |
    | createTime  |  date|  N  |     |  创建时间  |
    | pdateTime   |  date|  N  |     |  更新时间  |

#### 根据用户ID获取笔记本（getNoteByUserId）

 1. url:  
 2. method:  
 3. param:  
 
    |  参数名 |  类型 |  必填 |  默认 |  说明 | 
    | --- | --- | --- | --- | --- |
    |  userId  |  String  |   Y   |     |  用户ID  |
    |  noteType |  String  |  N  |  "个人" |  笔记本类型：全部/个人/收藏/最近/共享/回收站（具体参数需在后台创建枚举）
 4. return: 
 
	|  参数名 |  类型 |  必填 |  默认 |  说明 | 
	| --- | --- | --- | --- | --- | 
	 | id  |  String  |  Y  |     |  笔记本ID  |
	 | noteName  |  String  |  Y  |     |  笔记本名称   |
	 | noteCatalog   |  String  |  Y  |     |  笔记还是目录  |
	 | parentId   |  String    |  |   N    |  父ID  |
	 | parentName  |  String  |   N  |     |  父名称  |
	 | noteId   |  String  |  N  |     |  笔记ID  |


#### 在线新建/更新笔记（creatOrUpdatePersonalNote）
 1. url:  
 2. method:  
 3. param:  
 
     |  参数名 |  类型 |  必填 |  默认 |  说明 | 
	 | --- | --- | --- | --- | --- |
	 | userId  |  String  |  Y   |     |  创建人ID  |
	 | parentId  |  String  |  Y  |     |  创建父位置ID  |
	 | noteCatalog   |  String  |  Y  |     |  笔记还是目录  |
	 | noteName  |  String   |  N  |  "无标题笔记"   |  笔记名称  |
	 | noteContent |  String   |  N  |   空  |  笔记内容 |
	 | isTop  |  String  |  N  |  no   |  是否置顶 |
	     
 4. return: 
 
	|  参数名 |  类型 |  必填 |  默认 |  说明 | 
	| --- | --- | --- | --- | --- | 
	|  /  |   / |  /   |   /   |  / |

#### 上传笔记文件（uploadNodeFile）
 1. url:  
 2. method:  
 3. param:  
 
     |  参数名 |  类型 |  必填 |  默认 |  说明 | 
	 | --- | --- | --- | --- | --- |
	 | userId  |  String  |  Y   |     |  创建人ID  |
	 |  parent_id  |  String  |  Y  |     |  上传父位置ID  |
	   |  noteFile|  file|  Y  |     |  文件 |
 4. return: 
 
	|  参数名 |  类型 |  必填 |  默认 |  说明 | 
	| --- | --- | --- | --- | --- | 
	 |  /  |   / |  /   |   /   |  / |



#### 修改笔记信息 - 公开/移动/复制/回收/永久删除（modifyNode）
**(此接口也可以用在线新建/更新笔记代替）**
 1. url:  
 2. method:  
 3. param:  
 
     |  参数名 |  类型 |  必填 |  默认 |  说明 | 
	 | --- | --- | --- | --- | --- | 
	 | noteId|  String  |  Y   |     |  笔记ID  |
	 |  modifyType  |  String  |  Y  |     |  公开/移动/复制/回收/永久删除|
	 |  noteId|  String  |  Y  |     |  笔记ID|
	 |  parentId  |  String  |  Y  |     |  目标父ID（永久删除无需目标） |
 4. return: 
 
	|  参数名 |  类型 |  必填 |  默认 |  说明 | 
	| --- | --- | --- | --- | --- | 
	|  /  |   / |  /   |   /   |  / |

#### 获取公共笔记列表（getCommonNoteList）
**（这里的返回参数根据需求而定，这里可能比较复杂）**
 1. url:  
 2. method:  
 3. param:  
 
     |  参数名 |  类型 |  必填 |  默认 |  说明 | 
	 | --- | --- | --- | --- | --- | 
	 |  page  |  Integer  |  N   |  1   |   |
	 | size  |  Integer    |   N  |   10  |  |
	 | start  |   Integer    |  N   |  0   |  |
	 
 4. return: 
 
	|  参数名 |  类型 |  必填 |  默认 |  说明 | 
	| --- | --- | --- | --- | --- | 
	| noteId |  String  |  Y   |     |  笔记ID  |
	| noteName   | String     |  Y   |     |  笔记名称  |
	|  referenceNumber  |  String  |  Y  |     | 引用数 |
	|  abstract |  String  |  Y  |     | 摘要 |
	| noteSharePeopleId   |  String    |  Y  |     |  分享人ID  |
	| noteSharePeople   |  String    |  Y  |     |  分享人名  |
	| noteShareTime  |  date  |  N  |     |  分享时间  |

#### 根据ID获取公共笔记详情（getCommonNoteById）
 1. url:  
 2. method:  
 3. param:  
 
     |  参数名 |  类型 |  必填 |  默认 |  说明 | 
	 | --- | --- | --- | --- | --- | 
	 | noteId |  String  |  Y   |     |  笔记ID  |
	 |  referenceNumber  |  String  |  Y  |     | 引用数 |
	 |  noteContent |  String  |  Y  |     |  笔记内容 |
	 | noteName   | String     |  Y   |     |  笔记名称  |
	 | parentId   |  String    |  Y  |     |  父本ID  |
	 | parentName   |  string  |  Y  |     |  父本名称 |
	 | noteShareTime  |  date  |  N  |     |  分享时间  |
	 | noteSharePeopleId   |  String    |  Y  |     |  分享人ID  |
	 | noteSharePeople   |  String    |  Y  |     |  分享人名  |
	
 4. return: 
 
	 |  参数名 |  类型 |  必填 |  默认 |  说明 | 
	 | --- | --- | --- | --- | --- | 
	 | id |  String  |  Y   |     |  笔记ID  |
	 |  referenceNumber  |  String  |  Y  |     | 引用数 |
	 |  noteContent |  String  |  Y  |     |  笔记ID|
	 | noteName   | String     |  Y   |     |  笔记名称  |
	 | parentId   |  String    |  Y  |     |  父本ID  |
	 | parentName   |  string  |  Y  |     |  父本名称 |
	 | noteShareTime  |  date  |  N  |     |  分享时间  |
	 | noteSharePeopleId   |  String    |  Y  |     |  分享人ID  |
	 | noteSharePeople   |  String    |  Y  |     |  分享人名  |



### 用户信息管理
#### 注册
 1. url:  
 2. method:  
 3. param:  
 
     |  参数名 |  类型 |  必填 |  默认 |  说明 | 
	 | --- | --- | --- | --- | --- | 
	 |  username  |  String |   Y  |     |  用户名  |
	 |  password | String  |   Y  |     |  密码  |
	 |  password2 |  String |   Y  |     |  确认密码 |
	 |  email | String  |  Y   |     |  邮箱 |
	 |  realName |  String |  N   |     |  真实姓名  |

 4. return: 
 
	|  参数名 |  类型 |  必填 |  默认 |  说明 | 
	| --- | --- | --- | --- | --- | 
	|  / | /  |  /   |  /   | / |
	

#### 登录
 1. url:  
 2. method:  
 3. param:  
 
     |  参数名 |  类型 |  必填 |  默认 |  说明 | 
	 | --- | --- | --- | --- | --- | 
	 |  username  | String  |     |     | 用户名  |
	 | password  | String   |     |     | 密码  |

 4. return: 
 
	|  参数名 |  类型 |  必填 |  默认 |  说明 | 
	| --- | --- | --- | --- | --- | 
	|  /  | /  |  /   |  /   | /  |
	

#### 找回密码
 1. url:  
 2. method:  
 3. param:  
 
     |  参数名 |  类型 |  必填 |  默认 |  说明 | 
	 | --- | --- | --- | --- | --- | 
	 | username  | String  |   Y  |     |  用户名  |
	 | email  | String  |  Y   |     |  邮箱  |
	 | code  |  String |   N  |     |  收到验证码后填入 验证码   |
	 | password |  String |   N  |     |  收到验证码后填入  新密码  |
	 | password2 |  String |   N  |     |  收到验证码后填入 确认密码  |

	
 4. return: 
 
	|  参数名 |  类型 |  必填 |  默认 |  说明 | 
	| --- | --- | --- | --- | --- | 
	|  /  |  / |  /   |   /  | / |
